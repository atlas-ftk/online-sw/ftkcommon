#!/bin/sh
if [ $# -lt 2 ]; then
    echo
    echo usage: ftk_find_SFOdata.sh RUN_NUMBER LB_KEY
    echo                 LB_KEY can contain wild cards. E.g.
    echo                 LB_KEY 0554 or 055? or 05*     
    echo
    exit -1;
fi
  
RUN_NUMBER=$1
LB_KEY=$2
for qwe in 01 02 03 04 05 06 07 08 09 10 11 12; do 
    SFO_NAME=pc-tdq-sfo-$qwe;
    echo $SFO_NAME;
    ssh $SFO_NAME ls "/raid_cntl*/data/data*${RUN_NUMBER}*_lb${LB_KEY}*.data"; 

# commands to get the data. USE WITH CARE !!!
# CONTROL NETWORK MUST NOT BE OVERUSED.... !!!
#    FILE_LIST=`ssh $SFO_NAME ls "/raid_cntl*/data/data*${RUN_NUMBER}*_lb${LB_KEY}*.data"; `
#    echo SFO_NAME $SFO_NAME;
#    echo $FILE_LIST;
#    for qwe in $FILE_LIST; do
#      rsync -avz $SFO_NAME:$qwe .;
#    done
done
