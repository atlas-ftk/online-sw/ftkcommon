#!/bin/bash

if [ -z $TDAQ_PARTITION ]; 
  then echo 'Missing $TDAQ_PARTITION; please define it'; 
  exit 77 
fi

RUNNBR=`rc_getrunnumber`
DATE=`date +"%Y%m%d.%H%M%S"`
ROOTFILE="HistoFTK.run${RUNNBR}.${DATE}.root"

echo "Saving histos in file: $ROOTFILE"

oh_cp -p $TDAQ_PARTITION -s Histogramming -n HistoFTK -f $ROOTFILE
