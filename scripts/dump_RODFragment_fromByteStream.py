#!/usr/bin/env tdaq_python
#source /afs/cern.ch/atlas/project/tdaq/cmt/bin/cmtsetup.sh tdaq-04-00-01 $CMTCONFIG

#Example script to dump one ROD fragment from a bytestream file.
#
#You might need to uncompress the file with:
# ESCompress -n -i data13_hip.00218677.physics_Standby.merge.RAW._lb0062._SFO-ALL._0001.1 -o UncompressedFile.RAW

import sys
import eformat

def main(argv=None):
    inputfile = sys.argv[1]

    printW = False

    input_stream = eformat.istream(inputfile)
    for event in input_stream:
#        print 'Evento: ', event.lvl1_id()
        for rob in event.robs()[0]:
            if rob.source_id() == 0x130005:
                print "0x1B0F00000"
                for word in rob.rod():
#                    if (word >> 16 == 0x2000) or (word >> 16 == 0x2062):
#                        printW = True
#                    if (word >> 16 == 0x4000) and printW == True:
#                        printW = False
#                        print hex(word)
#                    if printW == True:
#                        print "0x%x"%word
                        #print hex(word)3

                    #if (word >> 16 != 0x4000):
#                    print hex(word)
                    print "0x%09x"%word
                print "0x1E0F00000"
if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
