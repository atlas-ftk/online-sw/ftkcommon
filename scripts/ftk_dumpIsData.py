#!/usr/bin/env tdaq_python
import sys
import time
import re

from ispy import InfoReader, IPCPartition, ISInfoIterator, ISInfoAny

part_name = 'FTK'
if len(sys.argv) == 2:
  part_name = sys.argv[1]


p  = IPCPartition(part_name)
x  = ISInfoAny()
it = ISInfoIterator(p, 'DF')

print "Partition:", part_name
print "Time: ", int(time.time()), "[", time.asctime(), "]"
while it.next():
  #print it.name()
  if it.name().startswith('DF.ROS.ROS-TDQ-FTK-00.DataChannel0'):
    it.value(x)
    x._update(it.name(), p)
    print
    print x
  if it.name() == 'DF.ROS.ROS-TDQ-FTK-00' :
    it.value(x)
    x._update(it.name(), p)
    print
    print x


x  = ISInfoAny()
it = ISInfoIterator(p, 'Monitoring')
while it.next():
  if it.name().startswith('Monitoring.FTKInfo'):
    it.value(x)
    x._update(it.name(), p)
    print
    print x

print
print "Looking for FTK resources"
print

x  = ISInfoAny()
it = ISInfoIterator(p, 'Resources')
myreg = re.compile('Resources..*FTK.*')
while it.next():
  if myreg.match(it.name()):
    it.value(x)
    x._update(it.name(), p)
    print
    print x
