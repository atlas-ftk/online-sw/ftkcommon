
# Run it with root privilegies and will be more powerful

# nicest way to kill partition, using the standard ipc_rm command
ipc_rm -f -p ${TDAQ_PARTITION} -i ".*" -n ".*"

# often zombie processes keep running holding resources

# killing remaining is_servers
killall is_server

# killing remaining ipc_servers
killall ipc_server

# killing remaining PMG servers
killall pmgserver

# killing remaining PMG launcher
killall pmglauncher

# emptying the TDAQ ref file. This file is a sort of pointer reference for the whole tdaq.
echo "" > ${TDAQ_IPC_INIT_REF/file:/}

echo
echo "if needed delete thse directories:"
echo "/tmp/ProcessManager/ /tmp/tdaq-04-00-01/ /tmp/${TDAQ_PARTITION}/"
echo

