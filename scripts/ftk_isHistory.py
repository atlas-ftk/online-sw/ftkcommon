#!/usr/bin/env tdaq_python
import sys
import time
import re

from ispy import InfoReader, IPCPartition, ISInfoIterator, ISInfoAny

part_name = 'ATLAS'
if len(sys.argv) == 2:
  part_name = sys.argv[1]

  
DFProvider    = 'DF.ROS.ROS-TDQ-FTK-00.DataChannel0'
ResProvider = 'Resources.StoplessRecoveryDisabled:FTK_Dummy_Busy_Source'
InfoProvider = 'Monitoring.FTKInfo'


p  = IPCPartition(part_name)
x  = ISInfoAny()
x2  = ISInfoAny()
it = ISInfoIterator(p, 'DF')
itFtkInfo = ISInfoIterator(p, 'Monitoring')


while it.next():
  if it.name().startswith(DFProvider):
    break;

while itFtkInfo.next():
  if itFtkInfo.name().startswith(InfoProvider):
    break;
    
while 1:
  it.value(x)
  x._update(it.name(), p)
  print
  print "Time: ", int(time.time()), "[", time.asctime(), "]"
  print x
  
  itFtkInfo.value(x2)
  x2._update(itFtkInfo.name(), p)
  print
  print "Time: ", int(time.time()), "[", time.asctime(), "]"
  print x2
  time.sleep(5)

