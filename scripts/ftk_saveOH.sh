#!/bin/bash
RUNNBR=`rc_getrunnumber`
DATE=`date +"%Y%m%d.%H%M%S"`
ROOTFILE="HistoFTK.run${RUNNBR}.${DATE}.root"

echo "Saving histos in file: $ROOTFILE"

oh_cp -p FTK -s Histogramming -n HistoFTK -f $ROOTFILE
