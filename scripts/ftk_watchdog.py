#!/usr/bin/env tdaq_python

#NB: numpy not available :-(
#TODO: create the map link_ID <-> busy channel, via OKS! 

import sys
import signal
import time
import getopt
import os
import ers
from ispy import * 
from collections import deque


#catch the SIGTERM signal and exiting (used to fix a RunControl error)
def signal_handler(signum, stack):
  print 'Received:', signum, ', exiting..'
  sys.exit(0)

#Metdhod used to make a FTK_Solo call
def FTK_Solo_call(part_name, segment_name, Slice, date):
  os.chdir("/shared/data/")
  if part_name=='ATLAS':
    if Slice == 'FTK_SliceA':
      Solo_A= 'FTK_Solo.sh -p ATLAS -S %s -o %s/FTK_SliceA/' %(segment_name, date)
      os.system(Solo_A)
    elif Slice == 'FTK_Slice2':
      Solo_2= 'FTK_Solo.sh -p ATLAS -S %s -o %s/FTK_Slice2/' %(segment_name, date)
      os.system(Solo_2)
    else :
      Solo_A= 'FTK_Solo.sh -p ATLAS -S %s -o %s/FTK_SliceA/' %(segment_name, date)
      os.system(Solo_A)
      Solo_2= 'FTK_Solo.sh -p ATLAS -S FTK-segment_Slice2 -o %s/FTK_Slice2/' % date
      os.system(Solo_2)
  else :
    print "Going to call FTKSolo with this parameters: \t%s, \t%s, \t%s" %(part_name, segment_name, date)
    Solo='FTK_Solo.sh -p %s -S %s -o %s/%s' %(part_name, segment_name, date, part_name)
    os.system(Solo)

#Call back to IS to monitor the STABLEBEAM status
def myhandler(cb):
  ers.log("Entered the handler..")
  global value, alreadystable, part_name, segment_name, Slice
  value = ISInfoDynAny()
  cb.value(value)
  if value.value != "STABLE BEAMS" and alreadystable:
    alreadystable=False
    ers.log("STABLE BEAM ended..Going to call FTK_Solo..")
    date1=time.strftime("%Y-%m-%d")+"/"+time.strftime("%Hh%Mm%Ss")
    FTK_Solo_call(part_name, segment_name, Slice, date1)
    ers.log("Going to create an elog about the Solo call..")
    elog= 'elisa_insert -a "FTK_watchdog" -c elisaAPI:elisaAPIp -y "Default Message Type" -j "FTK_Solo end of RUN call executed" -e "FTK" -b "FTK_Watchdog call to FTK_Solo completed for partition %s.\n Provider: %s \n Slice: %s. \n\nOutput files can be found here: /data/SoloRun/moveToEOS/%s" -x closed' % (part_name, provider, Slice, date1)
  elif value.value=="STABLE BEAMS":
    alreadystable=True
    ers.log("Entered STABLE BEAM..")
  else:
    ers.log("Beam state changed to " + value.value )

#Exception
class FtkWatchDogException( ers.Issue ):
  def __init__( self, msg, params, cause ):
      ers.Issue.__init__( self, msg, params, cause )

# Class definitions
# A pair: time, IS array
class Entry:
  def __init__(self, t, v):
    self.time  = t
    self.array = v

# History of IS arrays (a deque)
class History:
  # Constructor
  def __init__(self, n, s, w):
    self.span  = s
    self.wmark = w
    self.name  = n
    self.H     = deque()
  
  # Add a new array in the deque and remove the oldest
  def add(self, v):
    t = time.time()
    self.H.append(Entry(t, v))
    while(1):
      if self.H[0].time < (t - self.span):
        self.H.popleft()
      else: break
  
  # Return time gap between 1st and last array
  def dt(self):
    return self.H[-1].time  - self.H[0].time
  
  # Return array of differences between 1st and last array
  def dv(self):
    old = self.H[ 0].array
    new = self.H[-1].array
    r = []
    for i in range(len(old)):
      r.append(new[i] - old[i])
    return r
  
  # Return the last element
  def last(self):
    return self.H[-1].array
  
  # Return array of time averages 
  def avg(self):
    if len(self.H) < 2: return []
    m = []
    for i in self.dv():
      m.append( i / self.dt() )
    return  m

  # Dump the averages
  def dump(self):
    return "Delta %s = %s, Limit = %d/%d s " % (self.name, self.dv(),  self.wmark, self.span)
  
  # Trigger: return the list of indexes of problematic links (NB: TODO get the map from OKS)
  def trigger(self):
    mean  = self.avg()
    trig  = []
    for i in range(len(mean)):
      if mean[i] > float(self.wmark)/self.span: 
        trig.append(i)
    return trig

#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


def Usage():
  print 'Watchdog for FTK stopless removal'
  print 'The $TDAQ_PARTITION variable must be set'
  print 'Usage:', sys.argv[0], " -[options]"
  print '    -h        Help'
  print '    -o  val   Name of the IS object to watch [def=ROS.ROS-TDQ_FTK-00.ReadoutModule0]'
  print '    -f  val   Name of the Segment to be informed of the removal [def=FTK-Segment]'
  print '    -u  val   Name of the User-Command to be sent to the Segment [def=Disable_BP]'
  print '    -v  val   Verbosity, log every "val" events [def=100]'
  print '    -d  val   Delay between updates [def=5]'
  print '    -s  val   Span in seconds [def=60]'
  print '    -w  val   Watermark [def=4]'
  print '    -b  val   Busy channel names, comma separated list [def=FTK_Dummy_Busy_Channel]'
  print '    -r  val   RDC application ID [def=FTK-RCD-VmeCrate-1]'
  print '    -S  val   Sleep time after the error [def=9999999]' 
  print '    -B        Disable_BP mode. if specified sends the usercommand to the segment for the BP sending removal'
  print '    -D        Dry run, do not send the message'
  sys.exit(2)


# Default parameters
segment_name = 'FTK'
provider     = 'ROS.ROS-TDQ_FTK-00.ReadoutModule0'
busychannel  = 'FTK_Dummy_Busy_Channel' 
RCD_Appl     = 'FTK-RCD-VmeCrate-1' 
USRCMD       = 'Disable_BP'
verbose      = 100 
delay        = 1
span         = 60
wmark        = 4
sleepafter   = 9999999
BP_removal   = False
DryRun       = False

#Read command line parameters
try:
  opts, args = getopt.getopt(sys.argv[1:], "hDBp:o:v:d:s:b:r:S:w:f:u:")
except getopt.GetoptError, err:
  print str(err)
  Usage()
  sys.exit(2)



try:
  if not len(opts): Usage()
  for o,a in opts:
    if    o == "-h": Usage()
    if    o == "-D": DryRun       = True
    if    o == "-B": BP_removal   = True
    if    o == "-o": provider     = a
    if    o == "-f": segment_name = a
    if    o == "-u": USRCMD       = a
    if    o == "-v": verbose      = int(a)
    if    o == "-d": delay        = int(a)
    if    o == "-s": span         = int(a)
    if    o == "-S": sleepafter   = int(a)
    if    o == "-w": wmark        = int(a)
    if    o == "-b": busychannel  = a
    if    o == "-r": RCD_Appl     = a
except(AttributeError): pass

v = []
v.append('Configuration parameters:\n')
v.append("  provider    = %s\n" % (provider))
v.append(" segment_name = %s\n" % (segment_name))
v.append("  USRCMD      = %s\n" % (USRCMD))
v.append("  verbosity   = %s\n" % (verbose))
v.append("  busychannel = %s\n" % (busychannel))
v.append("  RCD Applic  = %s\n" % (RCD_Appl))
v.append("  delay (s)   = %d\n" % (delay))
v.append("  span (s)    = %d\n" % (span))
v.append("  watermark   = %d\n" % (wmark))
v.append("  trigger     = %f\n" % (float(wmark)/span))
v.append("  BP_removal  = %s\n" % BP_removal)
v.append("  DryRun      = %s\n" % DryRun)
ers.log(''.join(v))
  


# Join the partition
part_name = os.getenv("TDAQ_PARTITION", "")
if len(part_name) == 0 :
  s = 'Missing $TDAQ_PARTITION, please set it'
  e = FtkWatchDogException(s, {}, None)
  ers.warning(e) 
  sys.exit(3)

print 'Segment =', segment_name
print 'Partition =', part_name
p = IPCPartition(part_name)
if not p.isValid():
  print 'Partition', part_name, 'is not valid, abort.'
  sys.exit(2)


# Create the histories
hNever = History("numberOfNeverToCome", span, wmark)
hMayCo = History("numberOfMayCome"    , span, wmark)

#Slice Name
Slice=""
if provider == "ROS.ROS-TDQ-FTK-00.ReadoutModule0":
  Slice="FTK_SliceA"
if provider == "ROS.ROS-TDQ-FTK-01.ReadoutModule0":
  Slice="FTK_Slice2"

#catching the SIGTERM (to fix the RC error when stopping)
ers.log("passing trough sigterm")
signal.signal(signal.SIGTERM, signal_handler)

#Making a first check on the beam status
#TODO: uncomment the following lines when the BEAMODE IS variable will be back
# alreadystable=False
init = IPCPartition("initial")
# d = ISInfoDictionary(init)
# val = ISInfoDynAny()
# d.getValue('LHC.BeamMode', val)
# ers.log("Beam status =  %s " %val.value)
# value=val.value
# if value == "STABLE BEAMS":
  # alreadystable=True

#Subscription to IS to check the STABLEBEAM state
recv = ISInfoReceiver(init)
#TODO: uncomment the following line when the BEAMMODE IS variable will be back
#recv.subscribe_info('LHC.BeamMode', myhandler)

# Working loop
count = 0
ers.info("Started")
while(1):
  time.sleep(delay)    
  count = count + 1
  it = ISInfoIterator(p, 'DF',  ISCriteria(provider)) #  ROS.ROS-TDQ_FTK-00.ReadoutModule0
  while it.next():
    # Read the IS server
    rm = ISInfoAny()
    it.value(rm)
    rm._update(it.name(), p)
    nev = rm.numberOfNeverToCome
    may = rm.numberOfMayCome
   
    ## Just for debugging purpose
    #nev[4] = count
    #if count == 7: nev[4]=100
    #if count == 5: may[2]=7777
    ##

    # Fill the histories
    hNever.add(nev)
    hMayCo.add(may)

    # Logs
    if count%verbose == 0:
      s = 'limit = %d evs / %d s; ' % (wmark, span)
      s = s + '%s: %s, ' % (hMayCo.name, str(hMayCo.dv()) ) 
      s = s + '%s: %s  ' % (hNever.name, str(hNever.dv()) ) 
      ers.log(s)

    # Check the triggers
    disable = False

    # Save the cause of the removal
    removal_cause = "Cause of the removal: \n"

    trigNever = hNever.trigger()
    if len(trigNever) > 0:
      disable = True
      map = {}
      for v in trigNever:
        map[v]=hNever.dv()[v]
      s = '%s stopless removal triggered by {ch:delta} = %s (limit = %d evs / %d s)  ' % (hNever.name, str(map), wmark, span)
      ers.log(s)
      removal_cause =  removal_cause + s + "\n"
    
    trigMayCo = hMayCo.trigger()
    if len(trigMayCo) > 0:
      disable = True
      map = {}
      for v in trigMayCo:
        map[v]=hMayCo.dv()[v]
      s = '%s stopless removal triggered by {ch:delta} = %s (limit = %d evs / %d s)  ' % (hMayCo.name, str(map), wmark, span)
      ers.log(s)
      removal_cause =  removal_cause + s + "\n"


    # Send disable request using the busychannel list 
    # TODO: map links to busychannels (via OKS) and disable only the given affected channel
    if disable:
      bChs=busychannel.split(",")
      ers.log("Disabling: %s" % bChs)
      for b in bChs:
        themsg = 'echo "6" | rc_error_generator %s %s' % (RCD_Appl, b) 
        usrcmd = 'rc_sender -p %s -n %s -c USERBROADCAST %s' % (part_name, segment_name, USRCMD)
        if DryRun:
          s = "Dry Run, I should have executed: %s" % (themsg)
          ers.log(s)
        else:
          s = "Going to execute: %s" % (themsg)
          ers.log(s)
          os.system(themsg)  
          ers.log("rc::HardwareError raised. The OnlExpertSystem is now supposed to disable the ROS")
          if BP_removal:
            os.system(usrcmd)
            ers.log("UserCommand Disable_BP raised. The DF ReadOutModules are now supposed to disable the BP")

      ers.log("Going to create make the FTK_Solo calls..")
      date=time.strftime("%Y-%m-%d")+"/"+time.strftime("%Hh%Mm%Ss")
      FTK_Solo_call(part_name, segment_name, Slice, date)
      ers.log("Going to create an elog about the removal..")
      elog= 'elisa_insert -a "FTK_watchdog" -c elisaAPI:elisaAPIp -y "Default Message Type" -j "FTK stopless-removal completed" -e "FTK" -b "FTK stopless-removal procedure completed for partition %s.\n Provider: %s \n Slice: %s. \n\n %s \n\nOutput files can be found here: /data/SoloRun/moveToEOS/%s" -x closed' % (part_name, provider, Slice, removal_cause, date)
      os.system(elog) 
      ers.log("elog crated..")

      sleepTime = sleepafter 
      ers.log("Now, sleeping %d seconds" % sleepTime)
      time.sleep(sleepTime) # Sleep a little bit to prevent multiple triggers
      
