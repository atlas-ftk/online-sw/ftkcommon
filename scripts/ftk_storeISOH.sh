#!/bin/bash

#if [ -z $TDAQ_PARTITION ];
#  then echo 'Missing $TDAQ_PARTITION; please define it';
#    exit 77
#fi


export TDAQ_PARTITION=`ftk_findPartition.py`

echo "Found FTK in partition: ${TDAQ_PARTITION}"

if [ -z $TDAQ_PARTITION ];
  then echo 'FTK not found in any partition';
  exit 77
fi

RUNNBR=`rc_getrunnumber`

DIR="/det/tdaq/ftk/isDumps/"

DEST="${DIR}/${RUNNBR}"

mkdir -p $DEST
chmod 775 $DEST

DATE=`date +"%Y%m%d.%H%M%S"`

cd $DEST
echo $PWD

/det/tdaq/ftk/bin/ftk_dumpOH.sh

echo "Histogram saved"

ISFILE="${DEST}/${RUNNBR}.${DATE}.ftk_is.txt"

/det/tdaq/ftk/bin/ftk_dumpIsData.py $TDAQ_PARTITION > ${ISFILE}

echo "IS date saved in ${ISFILE}"
