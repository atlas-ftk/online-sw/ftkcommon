#!/usr/bin/env tdaq_python
import sys
import time
import re

from ispy import InfoReader, IPCPartition, ISInfoIterator, ISInfoAny

part_name = 'ATLAS'
if len(sys.argv) == 2:
  part_name = sys.argv[1]


p  = IPCPartition(part_name)
x  = ISInfoAny()
it = ISInfoIterator(p, 'DF')

while it.next():
  if it.name().startswith('DF.ROS.ROS-TDQ-FTK-00.DataChannel0'):
    while 1:
      it.value(x)
      x._update(it.name(), p)
      print
      print "Time: ", int(time.time()), "[", time.asctime(), "]"
      print x
      time.sleep(5)

