#!/usr/bin/env tdaq_python
import sys
import time
import re

from ispy import InfoReader, IPCPartition, ISInfoIterator, ISInfoAny

part_names = ['FTK', 'ATLAS']

for part in part_names:
  partition  = IPCPartition(part)
  if not partition.isValid() :
    continue

  x  = ISInfoAny()
  it = ISInfoIterator(partition, 'Resources')
  myreg = re.compile('Resources..*FTK.*')
  while it.next():
    if myreg.match(it.name()):
      it.value(x)
      x._update(it.name(), partition)
      print >> sys.stderr 
      print >> sys.stderr, x
      print >> sys.stderr, "FTK found in partition: ", part
      print part
      sys.exit(0)

print 'FTK'
sys.exit(0)
#print >> sys.stderr, "not found"
#sys.exit(-1)
        
