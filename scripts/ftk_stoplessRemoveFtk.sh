#!/bin/sh
# Simple script to manually disable the FTK ROL linked to a Busy_Channel OKS object (default = FTK_Dummy_Busy_Channel)
# The expert system will send the acknowledge to 
# TODO: 
#  - make paramenters configurable by command line

# Source the release
source /det/tdaq/scripts/setup_TDAQ.sh

# Define environment
export TDAQ_PARTITION=ATLAS
export TDAQ_ERS_ERROR="mts"
BUSY_CHANNEL_OKS_ID=FTK_Dummy_Busy_Channel
ACK_DEST_APPLICATION=FTK-RCD-Crate1

echo "TDAQ_PARTITION  = $TDAQ_PARTITION"
echo "TDAQ_ERS_ERROR  = $TDAQ_ERS_ERROR"
echo "Busy channel id = $BUSY_CHANNEL_OKS_ID"
echo "Ack destination = $ACK_DEST_APPLICATION"

# Send the error using rc_error_generator (option 6 of the interactive menu)
echo "6" | rc_error_generator $ACK_DEST_APPLICATION $BUSY_CHANNEL_OKS_ID







