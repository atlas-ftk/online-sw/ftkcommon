#!/bin/bash

REDFG="\033[1;31m"
NOCOL="\033[0m"

echo -e "${REDFG} To start the partition: ${NOCOL}"
echo '        setup_daq $TDAQ_PARTITION'
echo

echo -e "${REDFG} To edit the partition: ${NOCOL}"
echo '        oks_data_editor $TDAQ_DB_DATA'
echo

echo -e "${REDFG} To list where the ftk applications will run: ${NOCOL}"
echo '        rc_checkapps -d oksconfig:$TDAQ_DB_DATA -p $TDAQ_PARTITION '
echo

echo -e "${REDFG} To list current running partitions at P1: ${NOCOL}"
echo '        ipc_ls -P '
echo

echo -e "${REDFG} To load vmeconfiguration in sbc-ftk-rcc-01: ${NOCOL}"
echo '        vmeconfig -a /det/tdaq/ftk/vmeconfiguration/vmeconfig.tdaq04 '
echo

echo -e "${REDFG} To feed data via the quest on pc-ftk-l1src-01 ${NOCOL}"
echo "        amb_quest (see FTK vertical slice twiki for details)"
echo

echo -e "${REDFG} To modify oks fields from command line ${NOCOL}"
echo '        echo "@ReadoutModuleFTK.streamEnable=6" | pm_set.py  oksconfig:$TDAQ_DB_DATA '
echo '              NB: it does not work in ATLAS partition '
echo

echo -e "${REDFG} To generate 100 SCT events for the quest ${NOCOL}"
echo '        ftk_generateSctDataForQuest.py -n 100 '
echo

echo -e "${REDFG} How to read spybuffers ${NOCOL}"
echo '     AMB : amb_read_IO_spy --slot 15 | less '
echo '     Edro: ftk_readspy 0x200000 IO; less dump.txt '
echo '           ftk_readspy 0x200000 IM; less dumpFtkIM.txt '
echo '     To read and store the dumps with time and run number in /det/tdaq/ftk/isDumps/RUNNBR: '
echo '            ftk_readspy_andStore.sh '          
echo

echo -e "${REDFG} To read the FTK_IM firmware version ${NOCOL}"
echo '        ftk_dumpFtkImVersion.sh'
echo

echo -e "${REDFG} How to update via command line FTK OKS files in the ATLAS partition ${NOCOL}"
echo '       - export TDAQ_DB_USER_REPOSITORY=/YOURHOME/tdaq-04-00-01/'
echo '       - oks-checkout.sh daq/xxxx/fileyyy.xml'
echo '       - Modify the file'
echo '       - oks-commit.sh -m "Bla bla" -u daq/xxxx/fileyyy.xml'
echo


echo -e "${REDFG} Stopless Removal ${NOCOL}"
echo '   - To trigger stopless removal: '
echo '         export TDAQ_ERS_ERROR="mrs"'
echo '         export TDAQ_PARTITION=ATLAS  // Or FTK in case of stand alone running'
echo '         echo "6" | rc_error_generator FTK-RCD-Crate1 FTK_Dummy_Busy_Channel'
echo '     The OnlExpertSystem will send disable messages to FTK-RCD-Crate1 and the FTK-ROS' 
echo '   - To send directly the disable command to our RCD (NB: this is the command issued by OnlExpertSystem) '
echo '     rc_sendcommand -p $TDAQ_PARTITION -n FTK-RCD-Crate1 USER disable '
echo
echo -e "${REDFG} FTK nodes ${NOCOL}"
echo "    sbc-ftk-rcc-01      SBC"
echo "    pc-ftk-tdq-01       FTK PC with quest and local disk" 
echo "    pc-tdq-ros-ftk-00   FTK ROS"
echo
