#!/bin/bash

SBCNODE="pc-tdq-ros-ftk-00.cern.ch"
H=`hostname`

if [ $H != $SBCNODE  ];
  then echo "This script works only in $SBCNODE, while this is $H"
  exit 2
fi

robinscope -m 0 -l 0 -c | egrep "Discard|Name"

VAL=`robinscope -m 0 -l 0 -c | egrep "DiscardMode" | awk 'BEGIN {FS="|"} {print $9}'`

echo 
if [ $VAL -eq 1 ]
then 
  echo "i.e.: DiscardMode activated!"
else
  echo "i.e.: DiscardMode NOT active"
fi  
