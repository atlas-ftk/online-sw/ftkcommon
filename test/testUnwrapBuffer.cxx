
#include <iomanip>
#include <iostream>
#include <vector>

#include "ftkcommon/SpyBuffer.h"

using namespace daq::ftk;

// To compile this function requires some changes to the SpyBuffer class
//    * make unwrapBuffer public
//    * add trivial implementations for the two pure virtual functions
int main( int argc, char **argv ) {
  const size_t vecsize = 128;
  uint32_t sizebits = ((0x6) << 28) & 0xf0000000;

  uint32_t last_address = 30;

  //uint32_t spyStatus = sizebits | SPY_STATUS_OVERFLOW_MASK | last_address;
  uint32_t spyStatus = sizebits | last_address;

  SpyBuffer thebuff( spyStatus );
  thebuff.getBuffer().reserve( vecsize );

//  for ( size_t i=0; i<vecsize; ++i ) {
  for ( size_t i=0; i<last_address; ++i ) {
    uint32_t val = (i > last_address) ? (i-last_address-1) : (i+vecsize-last_address-1);
    thebuff.getBuffer().push_back(val);
  }
  for ( size_t i=last_address; i < vecsize; ++i ) thebuff.getBuffer().push_back(0);

  std::vector<uint32_t> clone = thebuff.getBuffer();

  thebuff.unwrapBuffer();

  for ( size_t i=0; i<vecsize; ++i ) {
    std::cout << "i=" << std::setw(5) << i << " clone[i]=" << std::setw(5) << clone[i] << " thebuff[i] =" << std::setw(5) << thebuff.getBuffer()[i] << std::endl;
  }

  return 0;
}
