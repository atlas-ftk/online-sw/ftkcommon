#include <iostream>
#include <fstream>

#include <getopt.h>

#include "ftkcommon/core.h"
#include "ftkcommon/EventFragmentCollection.h"
#include "ftkcommon/EventFragment.h"
#include "ftkcommon/EventFragmentFTKPacket.h"
#include "ftkcommon/EventFragmentHitClusters.h"
#include "ftkcommon/ModuleHitClusters.h"

using namespace std;

#ifndef TRUE
  #define TRUE                  0x01
#endif

#ifndef FALSE
  #define FALSE                 0x00
#endif

#define BUFSIZE  (4*1024)

// Globals
int verbose = FALSE;
int readfile = FALSE;
char filename[200] = {0};
std::string inputfilename;


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// read example Spy Buffer dump from file
int read_sb_file(vector<uint32_t>  &data, std::string filename)
{
  data.clear();
  int length = 0;
  std::string line;
  std::ifstream myfile(filename.c_str());
  if (myfile.is_open()) {

    while( std::getline( myfile, line ) ) {
      //      std::cout << line <<'\n';
      uint32_t value =std::stoul(line, nullptr, 16);
      data.push_back(value);
      length++;
    }
    myfile.close();
  }
  else {
    std::cout << "Unable to open file"; 
  }

  return length;
}


/**************/
void usage(void)
/**************/
{
  std::cout << "Valid options are ..." << std::endl;
  std::cout << "-f x: Read data from file. The parameter is the path and the name of the file" << std::endl;
  std::cout << "-v  : Verbose output                               -> Default: FALSE" << std::endl;
  std::cout << std::endl;
}



/*****************************/
int main(int argc, char **argv)
/*****************************/
{

  int c;
  vector<uint32_t> data;



  static struct option long_options[] = {"DVS", no_argument, NULL, '1'}; 

  while ((c = getopt_long(argc, argv, "f:v:h", long_options, NULL)) != -1)
    switch (c) 
    {
    case 'h':
      std::cout << "Usage: " << argv[0] << " [options]: "<< std::endl;
      usage();
      exit(-1);
      break;

    case 'f':   
      readfile = TRUE;
      sscanf(optarg,"%s", filename);
      inputfilename.append(filename);
      std::cout << "read data from file " << inputfilename << std::endl;
      break;
      
    case 'v': verbose = TRUE;               break;

    default:
      std::cout << "Invalid option " << c << std::endl;
      std::cout << "Usage: " << argv[0] << " [options]: " << std::endl;
      usage();
      exit (-1);
    }
  ;


  // read one spy buffer from file
  int sbbin = read_sb_file(data, inputfilename);
  sbbin+=0; // To silent the compiler

  // print first words
  for(uint32_t ij=0; ij<25; ij++) {
    if (verbose) std::cout << "Word " << std::dec << ij << " is " << std::hex << data[ij] << std::endl;
  }
  
  //////////////////////////////////
  ERS_LOG("test new Fragment parsing:  EventFragmentHitClusters_parseFragments");
  
  std::vector<daq::ftk::EventFragmentHitClusters*> pixel_modules_new  = FTKPacket_parseFragments<daq::ftk::EventFragmentHitClusters>(data,16);
  unsigned int nfr_new = pixel_modules_new.size();
  std::cout<< "number of fragments = " << nfr_new <<std::endl;
  for(unsigned int ifr=0; ifr<nfr_new; ifr++){
    std::cout << "LVL1ID header is " << std::dec << pixel_modules_new.at(ifr)->getL1ID() << " " << std::hex <<  pixel_modules_new.at(ifr)->getL1ID() << std::endl;
    if(ifr>=1) continue;
    uint32_t nmodules =  pixel_modules_new.at(ifr)->getNModules();
    std::cout << "Number of modules " << std::dec <<  nmodules << std::endl;
   
    for(uint32_t imod=0; imod<nmodules; imod++){
      if(imod>15) continue;
      std::cout << std::endl << "Module Number = " << std::dec << pixel_modules_new.at(ifr)->getModule(imod)->getModuleNumber()  << std::endl;
      std::cout << "Module type = " << std::dec << pixel_modules_new.at(ifr)->getModule(imod)->getModuleType()  << std::endl;

      uint32_t ncluster = pixel_modules_new.at(ifr)->getModule(imod)->getNClusters();
      std::cout << "Cluster Number = " << std::dec << ncluster  << std::endl;


      for(uint32_t icl=0; icl<ncluster; icl++){
	if(icl>15) continue;
	if(pixel_modules_new.at(ifr)->getModule(imod)->getModuleType()) { // SCT cluster
	  std::cout << "hit coordinate Cluster = " << std::dec << pixel_modules_new.at(ifr)->getModule(imod)->getSctCluster(icl)->hit_coord()  << std::endl;
	  std::cout << "hit width      Cluster = " << std::dec << pixel_modules_new.at(ifr)->getModule(imod)->getSctCluster(icl)->hit_width()  << std::endl;	  
	}
	else {  // Pixel cluster
	  std::cout << "row width first Cluster = " << std::dec << pixel_modules_new.at(ifr)->getModule(imod)->getPixelCluster(icl)->row_width()  << std::endl << std::endl;
	}
      }
    };
       
  }
  

  return 0;
}



