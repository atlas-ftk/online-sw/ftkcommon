#include <iostream> 
#include "ftkcommon/SourceIDSpyBuffer.h"

/*****************************/
int main(int argc, char **argv)
/*****************************/
{
  
  if (argc!=6)
  {
    std::cerr << "Usage: " << argv[0] << "boardType boardNumber boardInternal boardPosition boardReserved" << std::endl;
    return 1;
  }


  uint32_t sourceid = 0;
  daq::ftk::BoardType boardType     = static_cast<daq::ftk::BoardType>(atoi(argv[1]));
  uint8_t             boardNumber   = atoi(argv[2]);
  uint8_t             boardInternal = atoi(argv[3]);
  daq::ftk::Position  boardPosition = static_cast<daq::ftk::Position>(atoi(argv[4]));
  uint8_t             boardRes      = atoi(argv[5]);

  sourceid = daq::ftk::encode_SourceIDSpyBuffer(boardType, boardNumber, boardInternal, boardPosition, boardRes);
  std::cout << "Coded : 0X" << std::hex << sourceid  <<  std::endl;
  std::cout << "Decode " <<  daq::ftk::dump_SourceIDSpyBuffer(sourceid)  << std::endl;
  return 0;

}



