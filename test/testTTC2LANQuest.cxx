#include "config/Configuration.h"

#include "cmdl/cmdargs.h"

#include "RunControl/Common/OnlineServices.h"
#include "RunControl/Common/Controllable.h"
#include "RunControl/Common/CmdLineParser.h"
#include "RunControl/ItemCtrl/ItemCtrl.h"

#include "asyncmsg/Server.h" 
#include "asyncmsg/Session.h" 
#include "asyncmsg/Message.h" 
#include "asyncmsg/NameService.h"
#include "asyncmsg/UDPSession.h"

#include "dal/Partition.h"
#include "dal/RunControlApplicationBase.h"
#include "DFdal/DFParameters.h"

#include <memory>
#include <thread>
#include <atomic>

#include "ers/ers.h"

#include "is/infoT.h"
#include "is/infodictionary.h"

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <getopt.h>
#include "rcc_error/rcc_error.h"
#include "cmem_rcc/cmem_rcc.h"
#include "ROSsolar/quest.h"
#include "ROSGetInput/get_input.h"
#include "DFDebug/DFDebug.h"

#include <iostream>
#include <fstream>
#include <time.h>

#include "eformat/eformat.h"
#include "eformat/write/eformat.h"
#include "EventStorage/pickDataReader.h"
#include "ftkcommon/exceptions.h"
#include "ftkcommon/Utils.h"


#define CHECK(F) F.check()

#define DEBUG 1
#ifdef DEBUG
#define debug(x) printf x
#else
#define debug(x)
#endif

//#define BUFSIZE  (1024 * 1024)       //bytes
#define BUFSIZE  (4 * 1024 * 1024)       /*bytes*/
#define NEVENTS 1000 
//#define NEVENTS 820 // 819 for max_ROD_size of 1024, but ROS can handle max 1023. why?

//Globals
u_int max_ROD_size =  ((BUFSIZE - 4 - NEVENTS * 0x400) / 4 / NEVENTS); // currently not used

namespace { 

  

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // read example Spy Buffer dump from file
  int read_from_file(std::vector<std::vector<uint32_t>>  &data, std::string filename)
  {
    int nctrlwords = 0;
    data.clear();
    std::vector<uint32_t> tmp_data;
    tmp_data.clear();
    int length = 0;
    std::string line;
    std::ifstream myfile(filename.c_str());
    if (myfile.is_open()) {

      while( std::getline( myfile, line ) ) {
	uint32_t value =std::stoul(line, nullptr, 16);
	if(value==0Xee1234ee){
	  if(nctrlwords>0) data.push_back(tmp_data);
	  tmp_data.clear();
	  nctrlwords++;
	  ERS_DEBUG(1, " n control words = " << nctrlwords );
	}
	tmp_data.push_back(value);
	length++;
	ERS_DEBUG(1, line );
      }
      myfile.close();
      if(nctrlwords==1 && tmp_data.size()>0) data.push_back(tmp_data);
    }
    else {
      FTK_WARNING( "Unable to open input file for quest: " << filename);
    }

    return length;
  }

  
  // The current state of XON/XOFF from the HLT supervisor.
  // We don't know the size of a boolean, but we can control the size of an enum now
  // in C++11

  enum class XONOFFStatus : uint32_t { OFF = 0, ON = 1 };

  /**
   * Message from HLTSV to TTC2LAN.
   *
   * Change XON/XOFF status. Used to throttle the sender.
   * Payload: a single 32bit word, 0 = OFF, 1 == ON.
   */
  class XONOFF : public daq::asyncmsg::InputMessage {
  public:

    // This must agree with what the HLTSV uses
    static const uint32_t ID = 0x00DCDF50;
        
    XONOFF()
      : m_on_off(XONOFFStatus::ON)
    {}

    // Boiler plate for messages.
        
    // The message identifier
    uint32_t typeId() const override 
    {
      return ID;
    }

    // The transaction ID, not used.
    uint32_t transactionId() const override
    {
      return 0;
    }

    // Provide receive buffers.
    void toBuffers(std::vector<boost::asio::mutable_buffer>& buffers) override
    {
      buffers.push_back(boost::asio::buffer(&m_on_off, sizeof(m_on_off)));
    }

    // Message specific
        
    // Access received data, could also make variable just public in such
    // a simple case.
    XONOFFStatus status() const
    {
      return m_on_off;
    }

  private:
    XONOFFStatus m_on_off;
  };

  /**
   * Message from TTC2LAN to HLTSV.
   *
   * Event update.
   * Payload: a single extendend L1 ID (32bit)
   */
  class Update : public daq::asyncmsg::OutputMessage {
  public:

    // Must be same in HLTSV
    static const uint32_t ID = 0x00DCDF51;

    explicit Update(uint32_t l1id)
      : m_l1_id(l1id)
    {
    }

    // Message boiler plate
    uint32_t typeId() const override 
    {
      return ID;
    }

    uint32_t transactionId() const override
    {
      return 0;
    }

    void toBuffers(std::vector<boost::asio::const_buffer>& buffers) const override
    {
      buffers.push_back(boost::asio::buffer(&m_l1_id, sizeof(m_l1_id)));
    }

  private:

    uint32_t m_l1_id;

  };


  //
  // The Session class to talk to the HLTSV.
  //
  // The constructor takes a reference to a variable that it
  // updates with the XON/XOFF status as it receives messages.
  // This is the way it interacts with the rest of the application.
  //
  class Session : public daq::asyncmsg::Session {
  public:
    Session(boost::asio::io_service& service,
	    std::atomic<XONOFFStatus>& status)
      : daq::asyncmsg::Session(service),
	m_status(status)
    {
    }

    // no copying please, just use the shared_ptr<Session>
    Session(const Session&) = delete;
    Session& operator=(const Session& ) = delete;
        
  protected:
        
    void onOpen() noexcept override 
    {
      ERS_LOG("Session is open:" << remoteEndpoint());

      // initiate first receive
      asyncReceive();
    }
        
    void onOpenError(const boost::system::error_code& error) noexcept override 
    {
      ERS_LOG("Open error..." << error);
    }
        
    void onClose() noexcept override
    {
      ERS_LOG("Session is closed");
    }
        
    void onCloseError(const boost::system::error_code& error) noexcept override
    {
      ERS_LOG("Close error..." << error);
    }
        
    //
    // This is called when the message header has been parsed. 
    // It gives us the chance to create an object specific for the message type.
    //
    // We only expect a XONOFF message, so that's all we check.
    //
    std::unique_ptr<daq::asyncmsg::InputMessage> 
    createMessage(std::uint32_t typeId,
		  std::uint32_t /* transactionId */, 
		  std::uint32_t size) noexcept override
    {
      ERS_ASSERT_MSG(typeId == XONOFF::ID, "Unexpected type ID in message: " << typeId);
      ERS_ASSERT_MSG(size == sizeof(uint32_t), "Unexpected size: " << size);
      ERS_DEBUG(3,"createMessage, size = " << size);

      // all ok, create a new XONOFF object
      return std::unique_ptr<XONOFF>(new XONOFF());
    }
        
    // 
    // This is called when the full message has been received.
    void onReceive(std::unique_ptr<daq::asyncmsg::InputMessage> message) override
    {
      ERS_DEBUG(3,"onReceive");
      std::unique_ptr<XONOFF> msg(dynamic_cast<XONOFF*>(message.release()));

      m_status = msg->status();
            
      ERS_LOG("Got XONOFF message, new status = " << (m_status == XONOFFStatus::ON ? "ON" : "OFF"));
            
      // Initiate next receive
      asyncReceive();
    }
        
    void onReceiveError(const boost::system::error_code& error,
			std::unique_ptr<daq::asyncmsg::InputMessage> /* message */) noexcept override
    {
      ERS_LOG("Receive error: " << error);
    }
        
    void onSend(std::unique_ptr<const daq::asyncmsg::OutputMessage> /* message */) noexcept override
    {
      // Let the unique_ptr<> delete the message
    }
        
    void onSendError(const boost::system::error_code& error,
		     std::unique_ptr<const daq::asyncmsg::OutputMessage> /* message */) noexcept override
    {
      ERS_LOG("Send error: " << error);
      // throwing in the towel here, for this test program...
      abort();
    }

  private:
    std::atomic<XONOFFStatus>& m_status;
    std::atomic<bool>          m_running;
  };
    
  //
  // A simple run controlled application.
  //
  // It creates the session object, connects to the HLT supervisor
  // then runs a thread that generates L1 ID updates.
  // 
  class TTC2LANApplication : public daq::rc::Controllable {
  public:
    TTC2LANApplication()
      : daq::rc::Controllable(), m_running(false), m_quest_started(false), m_disable_hlt(0)
    {
      m_status = XONOFFStatus::ON;
          
      ERS_LOG("TTC2LANApplication(): m_status = "<< ((m_status == XONOFFStatus::ON) ? 1 : 0) );

      m_inputfile     =   "";
      m_occurence     =   99;
      m_formattype    =    0;
      m_delay         =  100;// ~10Hz
      m_throttleQuest = true;
      m_robSourceID   =  0Xffffffff;
    }

    ~TTC2LANApplication() noexcept
    {
    }

    virtual void disconnect(const daq::rc::TransitionCmd& ) override
    {
      ERS_LOG("Start disconnect");
      m_session->asyncClose();

      // We should not return from this transition before the session is open;
      // this is rather clumsy but since we have only one connection...
      while(m_session->state() != daq::asyncmsg::Session::State::CLOSED) {
	usleep(10000);
      }

      m_work.reset();
      m_service.stop();
      m_io_thread.join();
      ERS_LOG("Finished disconnect");
    }
    
    virtual void connect(const daq::rc::TransitionCmd& ) override
    {
      ERS_LOG("Start connect");
      m_status = XONOFFStatus::ON;
      ERS_LOG("TTC2LANApplication(): m_status = "<< ((m_status == XONOFFStatus::ON) ? 1 : 0) );
      // Get the partition from somewhere, assume it's in the environment for this test program.
      ERS_LOG("getting IPC partition: "<< getenv("TDAQ_PARTITION"));
      IPCPartition partition(getenv("TDAQ_PARTITION"));

      // For publishing our event counter status.
      m_dict.reset(new ISInfoDictionary(partition));

      // Create the name service object to lookup the HLTSV address
      daq::asyncmsg::NameService name_service(partition, std::vector<std::string>());

          
      // for debugging: find all applications and their endpoints
      std::map<std::string,boost::asio::ip::tcp::endpoint> apps;
      apps = name_service.lookup_names("");
      std::map<std::string,boost::asio::ip::tcp::endpoint>::iterator iter;
      ERS_LOG("applications found: ");
      for (iter = apps.begin(); iter != apps.end(); iter++) {
	ERS_LOG(iter->first);
      }
      // Do the lookup, the HLTSV will publish its endpoint under 'TTC2LANReceiver'.
      // In IS you can see it as the object 'DF.MSG_TTC2LANReceiver'.
      // This will throw an exception if it can't find the name entry.
      auto addr = name_service.resolve("TTC2LANReceiver");

      m_work.reset(new boost::asio::io_service::work(m_service));

      // Start the Boost.ASIO thread
      m_io_thread = std::thread([&]() { ERS_LOG("io_service starting"); m_service.run(); ERS_LOG("io_service finished"); });
            
      // Create the session with the endpoint we just got
      m_session = std::make_shared<Session>(m_service, m_status);
      m_session->asyncOpen(daq::rc::OnlineServices::instance().applicationName(), addr);

      m_event_count = 0;
      m_next_event = 0;  //0x01000000;

      // We should not return from this transition before the session is open;
      // this is rather clumsy but since we have only one connection...

      while(m_session->state() != daq::asyncmsg::Session::State::OPEN) {
	usleep(10000);
      }
          
      ERS_LOG("TTC2LANApplication()::connect(): m_status = "<< ((m_status == XONOFFStatus::ON) ? 1 : 0 ));
      ERS_LOG("Finished connect");
    }

    virtual void stopROIB(const daq::rc::TransitionCmd& ) override
    {
      // We pretend to be an RoIBuilder, so we should stop
      // in this transition.
      ERS_LOG("TTC2LANApplication()::stopROIB(): Started m_status = "<< ((m_status == XONOFFStatus::ON) ? 1 : 0) );            
      m_running = false;
      m_quest_started = false;
      m_generator_thread.join();
      ERS_LOG("TTC2LANApplication()::stopROIB(): Finished generator_thread m_status = "<< ((m_status == XONOFFStatus::ON) ? 1 : 0) ); 
      m_quest_thread.join();    
      ERS_LOG("TTC2LANApplication()::stopROIB(): Finished m_status = "<< ((m_status == XONOFFStatus::ON) ? 1 : 0) );
    }

    virtual void prepareForRun(const daq::rc::TransitionCmd& ) override
    {
      m_running = true;
      m_quest_started = false;
      m_quest_thread = std::thread(&TTC2LANApplication::generate_events_quest, this);
      m_generator_thread = std::thread(&TTC2LANApplication::generate_events, this);
      ERS_LOG("TTC2LANApplication()::prepareForRun(): m_status = "<< ((m_status == XONOFFStatus::ON) ? 1 : 0) );
    }

    virtual void publish() override
    {
      ERS_LOG("TTC2LANApplication()::publish(): m_status = "<< ((m_status == XONOFFStatus::ON) ? 1 : 0 ));

      // write to the internal variables (which should be atomic?) 
      m_event_count = m_next_event; // FIXME is m_event_count needed ? is it just a necessary type conversion ?
	  
      // Here we publish the total event counter to IS.
      ISInfoUnsignedLong value;
      value.setValue(m_event_count);
      m_dict->checkin( "DF." + daq::rc::OnlineServices::instance().applicationName(), value);
      
    }


    // The event generator is here for this test program since it's easier
    // to directly access the xon/xoff status
    void generate_events()
    {
      ERS_LOG("TTC2LANApplication()::generateEvents(): m_status = "<< ((m_status == XONOFFStatus::ON) ? 1 : 0) );
      uint32_t m_lastID = 0;
      while(m_running) {
	while(m_running && m_quest_started){
	  // We can't really reliably sleep for less than a millisecond
	  // since this is what the Linux scheduler gives us.
	  //std::this_thread::sleep_for(std::chrono::milliseconds(500));
	  ERS_DEBUG(2, "TTC2LANApplication()::generateEvents(): m_status = "<< ((m_status == XONOFFStatus::ON) ? 1 : 0) );
	  // If XON and event really was sent by the quest 
	  if(m_status == XONOFFStatus::ON && m_all_ready) {

	    // Send an update
	    // Here we just increment the counter by the value used to in Quest card.
	    // We keep track of the overall
	    // number of events in a 64bit counter, so we can publish
	    // that to IS.

	    // make sure that the same LVL1ID isn't sent several times -> different global event ID and LVL1ID
	    if(m_lastID == m_next_event) continue;
	    else m_lastID = m_next_event;
	    
	    ERS_DEBUG(2, "TTC2LANApplication()::generateEvents(): send message for m_next_event = "<< m_next_event );
	    // Create the message.
	    std::unique_ptr<Update> msg(new Update(m_lastID));

	    // Send the message. The std::move() transfers the ownership
	    // to the message passing library. The object is passed back
	    // to us via Session::onSend() when the message can be deleted.
	    m_session->asyncSend(std::move(msg));
	  }
	}
      }
    }



    // The event generator is here for this test program since it's easier
    // to directly access the xon/xoff status
    int generate_events_quest()
    {

      ERS_LOG("TTC2LANApplication()::generateEventsQUEST(): m_status = "<< ((m_status == XONOFFStatus::ON) ? 1 : 0) );
 
      u_long virtbase, pcibase;

      bool verbose = false;
      int  ret, memhandle;
      u_int  nfree, freeref, *data;
      QUEST_config_t fconfig;
      QUEST_in_t fin[MAXQUESTROLS];
      QUEST_info_t info_quest;
      std::vector<eformat::read::FullEventFragment> vec_fevfragments;
      std::vector<eformat::read::ROBFragment> vec_robfragments;
      std::vector<std::vector<uint32_t>> vec_data;
      vec_data.clear();
      // allign fragments on large enough ~1k boundary such that Quest sending doesn't overwrite buffer
      int multali = 4; // control size of alignment, can cause errors for Quest sending if it is too small
      u_int alisize = 256; // initial value
      m_next_event=0;
      u_int event_id = 0, event_size=0;
      u_int offset_base = 0;
      u_int isactive[MAXQUESTROLS];
      uint32_t offset_base_multi[NEVENTS]; // store offset of starting position
      uint32_t event_size_multi[NEVENTS];        // Quest driver needs size of ROD fragments
      uint32_t channel = 0;                // occasionally used loop over channels

      
      // reset list of active channels
      for (channel = 0; channel < MAXQUESTROLS; channel++)
	isactive[channel] = 0;
      
      // list of enabled Quest links
      for(int ichannel=0; ichannel<MAXQUESTROLS; ichannel++) {
	if(ichannel<32) isactive[ichannel] = daq::ftk::getBit(m_occurence, ichannel);
	if(isactive[ichannel]) ERS_INFO("Quest activated  channel: " << ichannel );   
      }
            
      // contigous buffer
      ret = CMEM_Open();
      if (ret)
	{
	  ERS_LOG(rcc_error_print(stdout, ret));
	  rcc_error_print(stdout, ret);
	  exit(-1);
	}
  
      ret = CMEM_BPASegmentAllocate(BUFSIZE, "QUEST_BUFFER", &memhandle);
      if (ret)
	{
	  ERS_LOG(rcc_error_print(stdout, ret));;
	  CMEM_Close();
	  exit(-1);
	}
  
      ret = CMEM_SegmentVirtualAddress(memhandle, &virtbase);
      if (ret)
	ERS_LOG(rcc_error_print(stdout, ret));;
  
      ret = CMEM_SegmentPhysicalAddress(memhandle, &pcibase);
      if (ret)
	ERS_LOG(rcc_error_print(stdout, ret));;


      // configuation of quest card

      // This function initializes the package. It can be called several times. 
      // Only the first call performs the open action. Subsequent calls increment a counter.
      ret = QUEST_Open();
      if (ret)
	{
	  ERS_LOG(rcc_error_print(stdout, ret));;
	  return(-1);
	}

      for(channel = 0; channel < MAXQUESTROLS; channel++){
	// reset Quest cards which are used
	if(!isactive[channel]) continue; // FIXME reset all quest cards?
	  
	// This function is meant to reset a QUEST card. 
	// The action performed is a card reset as specified for bit 0 of the OPCTL register. 
	// Additionally all S/W FIFOS for this card are reset.
	ERS_DEBUG(1, "Calling QUEST_Reset " << (channel >> 2));
	ret = QUEST_Reset(channel >> 2); // reset the card the channel is on
	if (ret)
	  {
	    ERS_LOG(rcc_error_print(stdout, ret));;
	    return(-1);
	  }
      }

      for(channel = 0; channel < MAXQUESTROLS; channel++){
	// reset Quest links which are used
	if(!isactive[channel]) continue;
	// This function is meant to reset a single S-Link channel. 
	// It sets the URESET bit of the respective channel,
	// waits for the LDOWN of that channel to come up and resets the URESET. 
	// After this operation the link should be up.   
	ERS_DEBUG(1, "Calling QUEST_LinkReset");
	ret = QUEST_LinkReset(channel); // reset the link
	if (ret)
	  {
	    ERS_LOG(rcc_error_print(stdout, ret));;
	    return(-1);
	  }
      }
      
      // printing some information about hardware
      if (verbose)
	{
	  ERS_DEBUG(1, "Printing  QUEST_Info");
	  // returns information about hardware
	  ret = QUEST_Info(&info_quest); 
	  if (ret)
	    {
	      ERS_LOG(rcc_error_print(stdout, ret));;
	      return(-1);
	    }  

	  ERS_DEBUG(1, "number of installed QUEST cards " << info_quest.ncards);
	  for ( unsigned int icard=0; icard < info_quest.ncards; icard++ ){
	    ERS_DEBUG(1, "QUEST card " << icard << " is present " << info_quest.cards[icard] );
	    for ( int ichannel=0; ichannel < 4; ichannel++ ){
	      ERS_DEBUG(1,  "QUEST channel " << ichannel << " is present " << info_quest.channel[ichannel]);
	    }
	  }
	  ERS_DEBUG(1, "MAXQUESTCARDS -- MAXQUESTROLS " << MAXQUESTCARDS << " -- " << MAXQUESTROLS);
	}

      fconfig.bswap = 0;         // Enable byte swapping
      fconfig.wswap = 0;         // Enable word swapping
      fconfig.scw = 0xb0f00000;  // Start control word pattern (implemented?)
      fconfig.ecw = 0xe0f00000;  // End control word pattern (implemented?)

      // This function collectively initializes all the QUEST modules installed in the PC
      ret = QUEST_Init(&fconfig);
      if (ret)
	{
	  ERS_LOG(rcc_error_print(stdout, ret));;
	  return(-1);
	}
 
      // This function can be used to determine the number of free slots in the S/W FIFO of a given channel. 
      // This number is required to prevent sending to many PCI addresses to the QUEST via QUEST_PagesIn.

      //Just to know the number of free pages in idle state
      for (channel = 0; channel < MAXQUESTROLS; channel++)
	{
	  if (isactive[channel])
	    {
	      ret = QUEST_InFree(channel, &freeref);
	      if (ret)
		{
		  rcc_error_print(stdout, ret);
		  return(-1);
		}
	      break;  //We just need a number from one channel
	    }
	}
      
      ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
      // read full event fragments from file
      //Open the input file

      if (m_formattype == 2) {
	DataReader *pDR = pickDataReader(m_inputfile);

	if(!pDR) {
	  ERS_INFO( "Problem opening or reading this file! " << m_inputfile);
	  return -1;
	}
	vec_fevfragments.clear();
	vec_robfragments.clear();

	while (pDR->good() && !pDR->endOfFile()) { //keep reading ...     

	  unsigned int eventSize;
	  char *buf;

	  DRError ecode = pDR->getData(eventSize,&buf);

	  if(ecode==DRNOOK){
	    ERS_LOG("Problem reading a fragment!");
	    return 1;
	  }

	  uint32_t * fulleventfragment = (uint32_t *)buf;

	  eformat::read::FullEventFragment fe(fulleventfragment);
	  
	  try{
	    if (!CHECK(fe)) {
	      ERS_LOG("Found invalid event");
	      return 1;
	    }

	  } catch (eformat::Issue& ex) {
	    ERS_LOG( "Uncaught eformat issue: " << ex.what() );
	  } catch (ers::Issue& ex) {
	    ERS_LOG( "Uncaught ERS issue: " << ex.what()); 
	  } catch (std::exception& ex) {
	    ERS_LOG( "Uncaught std exception: " << ex.what());
	  } catch (...) {
	    ERS_LOG( "Uncaught unknown exception"); 
	  }

	  // loop over rob fragments in fullevent fragment
	  ERS_LOG("Event Number " << vec_fevfragments.size()+1 << " has " << fe.nchildren() << " ROB fragments"); 
	  for(uint32_t irob = 0; irob < fe.nchildren(); irob++){
	    eformat::read::ROBFragment rob( fe.child(irob) );
	    if(m_robSourceID == 0Xffffffff) {
	      if(vec_robfragments.size() < NEVENTS) vec_robfragments.push_back(rob);
	      break;
	    }
	    if(rob.rod_source_id() == m_robSourceID){
	      if(vec_robfragments.size() < NEVENTS) vec_robfragments.push_back(rob);
	      //ERS_LOG("rob.rod_source_id() = " << hex << rob.rod_source_id() );
	    }
	  }
	    
	  if(vec_fevfragments.size() < NEVENTS) vec_fevfragments.push_back(fe); // FIXME use appropriate number of events ~NEVENTS
	}
	ERS_LOG("Selected " << vec_robfragments.size() << " events from file with source ID = " << hex << m_robSourceID);
	if(vec_robfragments.size() == 0) {
	  FTK_WARNING("No ROB fragments available in input file with source ID = " << m_robSourceID);
	  daq::ftk::FTKIssue ex(ERS_HERE, "No ROB fragments available in input file with source ID = " + to_string(m_robSourceID));
 	  ers::fatal(ex);
	}
      }
      else if (m_formattype == 3) { //  RODFragment for testing
	vec_data.clear();
	read_from_file(vec_data, m_inputfile);
	ERS_LOG("Events from file  " << " = " << vec_data.size() );
	uint32_t avg_fragmentsize=0;
	for(uint32_t idv=0; idv < vec_data.size(); idv++) avg_fragmentsize += vec_data.at(idv).size();
	avg_fragmentsize /= vec_data.size();
	ERS_LOG("Average fragment size = " << avg_fragmentsize); 
      }
      // finished reading fragments from file


      // write events to buffer and store start positions to pointer

      uint32_t evcount=0;
      while(evcount<NEVENTS){

	//Align the buffers to a 1K boundary to overcome problems sending from Quest
	if(event_size>256) alisize = event_size * multali;
	else alisize = 256*multali;
	if (alisize & 0x3ff)
	  alisize = (alisize + 0x400) & ~0x3ff;	  

	// offset_base defines the position in the buffer where the ROD fragment will be written to
	offset_base += alisize; // varible boundary size depending on previous fragment size (event_size)


	if (m_formattype == 2) { // RODFragment from file
	  // get ROD Fragment from fulleventfragment
	  uint32_t ielement = evcount%vec_robfragments.size();
	  uint32_t * rodfragment;
	  if ( !vec_robfragments.at(ielement).nchildren() == 1) FTK_WARNING("Not single ROD fragment in input data ROB");
	  rodfragment = (uint32_t *) vec_robfragments.at(ielement).child(0); // FIXME several RODFragments?

	  // update event size:
	  event_size = vec_robfragments.at(ielement).rod_nstatus() + vec_robfragments.at(ielement).rod_ndata() + 12;  // 12: for header and trailer // rodfragment[1]+2;;
	  ERS_DEBUG(1, "generate_events_quest(): event_size = " << event_size);
	    
	  data = (u_int *) (virtbase+offset_base);
	  for (uint32_t word=0; word<event_size; word++) data[word]=rodfragment[word];

	  // update lvl1id; starting LVL1ID
	  data[5] = m_next_event;
	  // update BCID
	  data[6] = 1;
	  
	  for (uint32_t word=0; word<event_size; word++) {
	    if(word<20) ERS_DEBUG(2, "generate_events_quest(): word " << word << " = " << std::hex << data[word]);
	  }
	  
	}
	else if (m_formattype == 3) { // SCT RODFragment for testing
	  uint32_t ielement = evcount%vec_data.size();
	  event_size = vec_data.at(ielement).size();
	  if(evcount<vec_data.size()) ERS_LOG("Fragment " << evcount << " Event size  " << " = " << event_size );
	  // data points to current buffer position 
	  data = (u_int *) (virtbase+offset_base);
	  for (uint32_t word=0; word<event_size; word++) {
	    data[word] = vec_data.at(ielement).at(word);
	    if(evcount==0) ERS_DEBUG(1, "Word " << word << " = " << hex << data[word] );
	  }

	  data[6] = 1;    // Bunch crossing ID
	  data[event_size-2] = event_size - 12 - data[event_size-3]; // Number of data elements
	  if(evcount<vec_data.size()) ERS_LOG("Number of data elements = " << hex << data[event_size-2]); 
 
	}
	else { // RODFragment for testing
      
	  event_size = 256; // FIXME add as argument to application

	  /*
	  // calculate new alisize to reset offset_base if end of buffer is reached
	  if(event_size>256) alisize = event_size * multali ;
	  else alisize = 256*multali;
	  if (alisize & 0x3ff)
	  alisize = (alisize + 0x400) & ~0x3ff;
	  if((offset_base>BUFSIZE - alisize  -32)) offset_base =0;
	  */
	    
	  // data points to current buffer position 
	  data = (u_int *) (virtbase+offset_base);

	  int ndwords = event_size - 13;
	  int word = 0;
	  data[word++] = 0xee1234ee; //header marker 
	  data[word++] = 0x00000009; //header size 
	  data[word++] = 0x03010000; //format version 
	  data[word++] = 0x00a00000; //source ID
	  data[word++] = 0x00000001; //run number
	  //	data[word++] = event_id;   //initial L1ID
	  data[word++] = m_next_event;   //initial L1ID
	  data[word++] = 0x00000001; //BCID 
	  data[word++] = 0x00000007; //trigger type
	  data[word++] = 0x0000000a; //detector event type
	  data[word++] = 0x00000000; //status word 

	  for (int loop2 = 0; loop2 < ndwords; loop2++)  
	    {
	      if(loop2 & 1)
		data[word++] = 0;           //Data word
	      else
		data[word++] = 0xffffffff;  //Data word
	    }

	  data[word++] = 0x00000001;  //Number of status words  
	  data[word++] = ndwords;     //Number of data words
	  data[word++] = 0x00000000;  //Status word before data words
	}

	// should not occur if reset of counter works as only NEVENTS(=1000?) are written to buffer
	if (event_size + offset_base > BUFSIZE) 
	  {
	    FTK_WARNING( "Input event is too big" );
	    exit(-1);
	  }

	if (event_size > 1023) {  // FIXME enter max_ROD_size here?
	  FTK_WARNING( "Skipping event " << event_id << " because it is too big for ROS" );
	  continue; // ROS can receive ROD events of up to 1024 words
	}

	ERS_DEBUG(1, evcount << " offset_base/BUFSIZE = " << offset_base << "/" << BUFSIZE);
	offset_base_multi[evcount]  = offset_base;
	event_size_multi[evcount]   = event_size;
	evcount++;
      
      }

      // end write events to buffer

      // wait until all message sessions are established (e.g. DCM-ROS)
      std::this_thread::sleep_for(std::chrono::milliseconds(1000));
      
    
      ERS_LOG("TTC2LANApplication()::generateEventsQUEST(): m_quest_started = " << m_quest_started  );
      // loop for updating and sending lvl1id
      //ERS_LOG("TTC2LANApplication()::generateEventsQUEST(): m_status = "<< ((m_status == XONOFFStatus::ON) ? 1 : 0) );
      while(m_running) {
	
	//ERS_LOG("TTC2LANApplication()::generateEvents() event_id = "<< event_id );

	//if(!m_running) break; // FIXME

	for(int evcount2=0; evcount2<NEVENTS; evcount2++){

	  // delay qst-ros should be controlled via oks in future
	  if(m_delay>=0) std::this_thread::sleep_for(std::chrono::milliseconds(m_delay));

	  //Wait until all events are out
	  // FIXME  change such that program is not stuck forever	    
	  bool tmp_all_ready = false;
	  int loopcount = 0;
	  while (tmp_all_ready == false)
	    {
	      if(!m_running) break;
	      tmp_all_ready = true;
	      loopcount++;
	      if(loopcount==1e7) ERS_LOG("TTC2LANApplication()::generateEventsQUEST() no free pages for m_next_event = " << m_next_event);
	      for (channel = 0; channel < MAXQUESTROLS; channel++)
		{
		  if (isactive[channel])
		    {  
		      ret = QUEST_InFree(channel, &nfree);
		      if (ret)
			{
			  ERS_LOG(rcc_error_print(stdout, ret));
			  return(-1);
			}
		      //printf("channel = %d, free = %d, freeref = %d\n", channel, nfree, freeref);
		      if (nfree != freeref){
			tmp_all_ready = false;
			QUEST_Flush(channel);
		      }
		    }
		} 
	    }
	  m_all_ready = true;
	  
	  // delay 
	  while(!(m_status == XONOFFStatus::ON) && m_throttleQuest && m_running) std::this_thread::sleep_for(std::chrono::milliseconds(1));// FIXME don't get stuck forever
 
	  //update lvl1id counter
	  if(!event_id==0) m_next_event++;

	  // update LVL1IDs in ROD fragment
	  data = (u_int *) (virtbase+offset_base_multi[evcount2]);
	  data[5] = m_next_event; // Extended Level 1 ID
	  
	  // stop sending of fragments via quest 
	  if(!m_running) break;

	  // fill fin for all channels
	  for (channel = 0; channel < MAXQUESTROLS; channel++)
	    {
	      if (isactive[channel])
		{
		  // The structure QUEST_in_t
		  fin[channel].nvalid = 1;
		  // has to be equal to the number of valid requests in the pciadd, size, scw and ecw arrays
		  fin[channel].channel = channel; 
		  // channel = (# of QUEST card) * 4 + (number of channel)
		  // FIXME 
		  fin[channel].size[0] = event_size_multi[evcount2]; 
		  // defines how many 32-bit words will be taken from the respective PCI base address.
		  // determine if a start or an end control word will be added to the data block as it is transferred on S-Link.
		  fin[channel].scw[0] = 1;
		  fin[channel].ecw[0] = 1;
		  fin[channel].pciaddr[0] = pcibase + offset_base_multi[evcount2]; 
		  // has to be filled with up to MAXINFIFO PCI addresses
		}
	    }

	  m_all_ready = false;
	  
	  for (channel = 0; channel < MAXQUESTROLS; channel++)
	    {
	      if (isactive[channel])
		{
		  // This function fills the S/W FIFO of the driver with data transfer requests. 
		  // The driver automatically reads these requests from the S/W FIFO and 
		  // copies them to the QUEST H/W as required.
		  ret = QUEST_PagesIn(&fin[channel]);  
		  if (ret)
		    {
		      ERS_LOG(rcc_error_print(stdout, ret));;
		      return(-1);
		    }
		  // send TTC2LAN messages directly from Quest thread
		  //if(m_status == XONOFFStatus::ON) 
		  //  {
		  //	ERS_LOG("TTC2LANApplication()::generateEventsQUEST(): m_status = "<< ((m_status == XONOFFStatus::ON) ? 1 : 0) );
		  // Create the message.
		  //	std::unique_ptr<Update> msg(new Update(m_next_event));
		  //	m_session->asyncSend(std::move(msg));
		  //  }
		  //ERS_LOG("TTC2LANApplication()::generateEventsQUEST(): QUEST_Flush for m_next_event = "<< m_next_event );

		  // This function has to be called after QUEST_PagesIn to inform the driver about the new transfer requests.
		  QUEST_Flush(channel); 
		}
	    }    

	  event_id++;
	  m_quest_started = true; // FIXME sufficient to be called once
	  //}// end if  XON
	}// end NEVENTS loop	  
      }// end of loop updating lvl1id

      // This function closes the Quest library. It has to be called as many times as QUEST_Open. 
      // Only the last call closes the library.
      ret = QUEST_Close();
      if (ret)
	ERS_LOG(rcc_error_print(stdout, ret));

      ret = CMEM_BPASegmentFree(memhandle);
      if (ret)
	ERS_LOG(rcc_error_print(stdout, ret));
 
      ret = CMEM_Close();
      if (ret)
	ERS_LOG(rcc_error_print(stdout, ret));
      
      return(0);
    }


    std::string getInputfile()    {return m_inputfile;};
    uint32_t    getOccurence()    {return m_occurence;};
    int         getFormattype()   {return m_formattype;};
    int         getDelay()        {return m_delay;};
    bool        getThrottleQuest(){return m_throttleQuest;};
    uint32_t    getRobSourceID()  {return m_robSourceID;}
    
    void        setInputfile(std::string file)
    {
      m_inputfile = file;
      ERS_LOG("Quest input file: " << m_inputfile);
    };
    void        setOccurence(uint32_t value)
    {
      m_occurence = value;
      ERS_LOG("Quest occurence mask = 0X" << hex << m_occurence);
    };
    void        setFormattype(int value)
    {
      m_formattype = value;
      ERS_LOG("Quest format type = " << m_formattype);
    };
    void        setDelay(int value)
    {
      m_delay = value;
      ERS_LOG("Quest delay = " << m_delay); 
    };
    void        setThrottleQuest(bool value)
    {
      m_throttleQuest = value;
      ERS_LOG("Quest is throttled by HLTSV = " << m_throttleQuest); 
    };    
    void        setRobSourceID(uint32_t value)
    {
      m_robSourceID = value;
      ERS_LOG("Quest used rob source ID = 0X" << hex << m_robSourceID);
    };
    
  private:
    std::thread                  m_io_thread;
    std::thread                  m_generator_thread;
    std::thread                  m_quest_thread;
    std::atomic<bool>            m_running;
    std::atomic<bool>            m_quest_started;
    std::atomic<bool>            m_all_ready;
    boost::asio::io_service      m_service;
    std::unique_ptr<boost::asio::io_service::work> m_work;
    std::shared_ptr<Session>     m_session;
    std::unique_ptr<ISInfoDictionary> m_dict;

    std::atomic<XONOFFStatus>    m_status;
    std::atomic<uint32_t>        m_next_event;
    uint64_t                     m_event_count;
    uint32_t                     m_disable_hlt;

    // configuration variables
    std::string m_inputfile;
    uint32_t    m_occurence;
    int         m_formattype;
    int         m_delay;
    bool        m_throttleQuest;
    uint32_t    m_robSourceID;
    
  };

   
  
  //Globals
  char        quest_channelmask[200] = {0};
  char        quest_sourcemask[200] = {0};
  char        quest_filename[200] = {0};
  std::string quest_inputfile="";
  uint32_t    quest_occurence=0x1;
  int         quest_formattype=0;
  int         quest_delay=100;
  bool        quest_throttleQuest=true;
  uint32_t    quest_robSourceID=0Xffffffff;
  
  
  /**************/
  void usage(void)
  /**************/
  {
    ERS_LOG( "Valid options are ..." );
    ERS_LOG( "-m x: Eformat type (2: RODs from bytestream, 3: single ROD from txt file, else dummy ROD) -> Default: " << quest_formattype );
    ERS_LOG( "-S x: ROB source ID (= 0Xffffffff will select arbitrary rob fragment)                     -> Default: " << quest_robSourceID);
    ERS_LOG( "-o x: Occurence channel mask (= ROL number 0..N)                                          -> Default: " << quest_occurence );
    ERS_LOG( "-w x: Wait x ms after each event to reduce rate, smallest delay is 1ms                    -> Default: " << quest_delay );
    ERS_LOG( "-r x: Throttle Quest if it receives XOFF from HLTSV                                       -> Default: " << quest_throttleQuest );
    ERS_LOG( "-f x: Read data from file. The parameter is the path and the name of the file" );
  }


}



//
// Boilerplate main program
//
int main(int argc, char *argv[])
{



  daq::rc::CmdLineParser cmdline(argc, argv);  


  static struct option long_options[] = {"DVS", no_argument, NULL, '1'};
  int c;
  while ((c = getopt_long(argc, argv, "f:r:w:o:S:m:h:", long_options, NULL)) != -1)
    switch (c) 
      {
      case 'h':
	ERS_LOG( "Usage: " << argv[0] << " [options]: ");
	usage();
	break;
      case 'm': quest_formattype = atoi(optarg);   break;
      case 'S':
	sscanf(optarg,"%s", quest_sourcemask);
	quest_robSourceID = std::stoul(quest_sourcemask, nullptr, 16);
	break;
      case 'o':
	sscanf(optarg,"%s", quest_channelmask);
	quest_occurence = std::stoul(quest_channelmask, nullptr, 16);
	break;
	//	quest_occurence  = atoi(optarg);   break;
      case 'w': quest_delay = atoi(optarg);        break;
      case 'r': quest_throttleQuest = atoi(optarg);        break;	
      case 'f':   
	sscanf(optarg,"%s", quest_filename);
	quest_inputfile.append(quest_filename);
	ERS_LOG( "Quest input data from file " << quest_inputfile);
	break;
  	
      default:
	ERS_LOG( "Unknown option " << c );
	break;
      }
  std::shared_ptr<TTC2LANApplication> questApp = std::make_shared<TTC2LANApplication>();

  questApp->setInputfile(quest_inputfile);
  questApp->setOccurence(quest_occurence);
  questApp->setFormattype(quest_formattype);
  questApp->setDelay(quest_delay);
  questApp->setThrottleQuest(quest_throttleQuest);
  questApp->setRobSourceID(quest_robSourceID);
  
  try {
    //daq::rc::CmdLineParser cmdline(argc, argv);  
    //daq::rc::ItemCtrl control(cmdline, std::make_shared<TTC2LANApplication>());
    daq::rc::ItemCtrl control(cmdline, questApp);
    control.init();
    control.run();
  } catch(ers::Issue& ex) {
    ers::fatal(ex);
    exit(EXIT_SUCCESS);
  }

}


