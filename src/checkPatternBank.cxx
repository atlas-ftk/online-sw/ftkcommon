#include <ftkcommon/patternbank_lib.h>
#include <ftkcommon/FtkPatternBank.h>
#include <ftkcommon/FtkCool.hh>
#include <ftkcommon/FtkDbUtils.hpp>
#include <is/infodictionary.h>
#include <cmdl/cmdargs.h>
#include <rc/RunParams.h>


int main(int argc,char *argv[]) {
  
  CmdArgStr   partition_name ('p', "partition", "ATLAS", "partition to work in");
  CmdArgInt   runNumber('n',"runNumber","run-number","run number");
  CmdArgInt   region('r',"region","region-number","region number",CmdArg::isVALREQ );
  CmdArgStr   dbConn('d',"dbConn","database-connection","database connection string");
  CmdArgStr   dbTag('t',"dbTag","database-tag","database tag");
  CmdArgStr   patternDir('b',"patternBankDir","patternBankDir","directory bank ROOT files",CmdArg::isVALREQ);
  CmdArgStr   outDir('o',"outkDir","outDir","output diretory for  bank dumps");

  runNumber=-1;
  unsigned int runnumber;
  storage::variant v;
  CmdLine  cmd(*argv, &partition_name,&runNumber,&dbConn,&dbTag,&patternDir,&region,&outDir,NULL);
  CmdArgvIter  arg_iter(--argc, ++argv);
  cmd.parse(arg_iter);
  if(!dbConn.isNULL() && ! dbTag.isNULL()) {    
    if(runNumber==-1) {
      try {
	IPCCore::init( argc, argv );
      }
      catch( daq::ipc::Exception & ex ) {
	ers::fatal( ex );
	return 1;
      }
      IPCPartition   p( (const char*)partition_name );
      ISInfoDictionary isInfoDict=ISInfoDictionary(p);
      RunParams runParams; 
      isInfoDict.findValue("RunParams.RunParams",runParams);
      runnumber=runParams.run_number+1;
    } else
      runnumber=runNumber;
    FtkCool cool((const char*)dbConn,(const char *)dbTag,runnumber);
    cool.read(v,FtkCool::FtkConfigType::PATTERNBANK_8M_8L,region);
  } else {
    FtkDbUtils::readPatternBank((const char*)patternDir,region,v);    
  }
  std::string bankfile=FtkDbUtils::findPatternBank((const char*)patternDir,region);
  
  FtkPatternBank newbank(v);
  
  FTKPatternBank oldbank;
  oldbank.loadPatternBank(bankfile.c_str(),0,8*1024*1024,false);

  std::cout << "Check Sum(new): " << std::hex << newbank.calculateChecksum() <<std::endl;
  oldbank.calculateChecksum();
  std::cout << "Check Sum(old): " << std::hex << oldbank.getChecksum() << std::endl;
  if(!outDir.isNULL() ){
      std::string dir((const char *)outDir);
      std::string nfile=dir+"/patternbank_new_"+std::to_string(region)+".txt";
      std::string ofile=dir+"/patternbank_old_"+std::to_string(region)+".txt";
      newbank.dumpPatternBank(nfile.c_str());
      oldbank.dumpPatternBank(ofile.c_str());
    }
  return 0;

}
