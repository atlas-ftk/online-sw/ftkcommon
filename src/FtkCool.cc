#include <sstream>
#include <stdexcept>
#include <fstream>
#include <iostream>
#include <map>
#include <MsgPack.hpp>
#include "ftkcommon/FtkCool.hh"

std::map<const FtkCool::FtkConfigType,std::tuple<const std::string,const std::string,const uint8_t,bool>>  FtkCool::m_configpars={   
  {FtkConfigType::CORRGEN_RAW_12L_GCON,{"CORRGEN_RAW_12L","CorrgenRaw12L",16,false}},
  {FtkConfigType::CORRGEN_RAW_8L_GCON,{"CORRGEN_RAW_8L","CorrgenRaw8L",16,false}},
  {FtkConfigType::SECTORS_RAW_8L_CONN,{"SECTORS_RAW_8L","SectorsRaw12L",1,false}},
  {FtkConfigType::PATTERNBANK_8M_8L,{"PATTERNBANK_8M_8L","PatternBank8M8L",32,true}}
};

void FtkCoolWritable::fillRecord( cool::Record& record )const {  
  record["Data"].setValue<coral::Blob>(m_data);
}

void FtkCoolWritable::setRecordSpec(  cool::RecordSpecification& recordSpec  ) const {
  recordSpec.extend("Data", cool::StorageType::Blob16M);
}

void FtkCoolReadable::fetch(coral::Blob &pt) {
  if(object()!=0) {
    const cool::IRecord& record = object()->payload();
    pt=record["Data"].data<coral::Blob>();
  } else pt.resize(0); 
}



std::string FtkCool::_folder(FtkConfigType t) {
  return  std::get<0>(m_configpars.at(t));
}
std::string FtkCool::_tag(FtkConfigType t) {
  return  std::get<1>(m_configpars.at(t));
}

unsigned FtkCool::_nparts(FtkConfigType t) {
  return  std::get<2>(m_configpars.at(t));
}

bool FtkCool::_selfContained(FtkConfigType t) {
  return  std::get<3>(m_configpars.at(t));
}


void FtkCool::write(const storage::variant &ss,const FtkConfigType config,unsigned region) {
  std::string folder=_folder(config);
  bool selfContained=_selfContained(config);
  unsigned nparts=_nparts(config);
  daq::coolutils::CoolWriter writer(m_coolConn,std::string(FTK_FCONST_FOLDER)+"/"+folder,FTK_FCONST_PREFIX+_tag(config)+"-"+m_tag);
  if(selfContained) {
    if(  !ss.is_array() ||  nparts!=ss.size() ) throw; // storage::variant is not an array

    for( unsigned i=0;i<ss.size();i++) {
      const storage::variant &j=ss[i];
      std::vector<uint8_t> byteBuffer;
      MemoryWriteBuffer writeBuffer(byteBuffer);
      msgpack::Writer msgwriter(writeBuffer, j) ;  
      coral::Blob blob(byteBuffer.size());
      memcpy(blob.startingAddress(), byteBuffer.data(),byteBuffer.size());
      FtkCoolWritable obj(blob);
      writer.write(m_runnumber,0,obj,(region<<8)+i);
    }
  } else {
    std::vector<uint8_t> byteBuffer;
    MemoryWriteBuffer writeBuffer(byteBuffer);
    msgpack::Writer msgwriter(writeBuffer, ss) ;      
    std::size_t size = byteBuffer.size();
    unsigned n_blob=size/nparts;
    unsigned r_blob=size%nparts+n_blob;
    for(unsigned int i=0;i<nparts-1;i++) {
      coral::Blob blob(n_blob);    
      memcpy(blob.startingAddress(), &(byteBuffer.data()[n_blob*i]),n_blob);
      FtkCoolWritable obj(blob);
      writer.write(m_runnumber,0,obj,(region<<8)+i);
    }
    coral::Blob blob(r_blob);
    memcpy(blob.startingAddress(), &(byteBuffer.data()[(nparts-1)*n_blob]),r_blob);
    FtkCoolWritable obj(blob);
    writer.write(m_runnumber,0,obj,(region<<8)+(nparts-1));    
  }
}

void FtkCool::read(storage::variant &pt,FtkConfigType config,unsigned region) {
  std::string folder=_folder(config);
  unsigned nparts=_nparts(config);
  bool selfContained=_selfContained(config);
  daq::coolutils::CoolReader reader(m_coolConn,std::string(FTK_FCONST_FOLDER)+"/"+folder,FTK_FCONST_PREFIX+_tag(config)+"-"+m_tag);
  if(selfContained) {
    storage::variant v;
    for(unsigned int i=0;i<nparts;i++) {
      coral::Blob data;
      FtkCoolReadable obj; 
      reader.read(obj,m_runnumber,0,(region<<8)+i);
      obj.fetch(data);
      MemoryReadBuffer buffer((char*)data.startingAddress(),data.size());
      msgpack::Reader(buffer,v,true); // toDouble conversion for all floats
      pt.push_back(v);
    }
  } else {
    coral::Blob data;
    for(unsigned int i=0;i<nparts;i++) {
      coral::Blob blob;
      FtkCoolReadable obj; 
      reader.read(obj,m_runnumber,0,(region<<8)+i);
      obj.fetch(blob);
      data+=blob;
    }
    MemoryReadBuffer buffer((char*)data.startingAddress(),data.size());
    msgpack::Reader(buffer,pt,true);
  }
}





