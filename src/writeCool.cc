#include <sstream>
#include <ipc/partition.h>
#include <is/infoiterator.h>
#include <boost/filesystem.hpp>
#include <is/infodictionary.h>
#include <cmdl/cmdargs.h>
#include <rc/RunParams.h>
#include <ftkcommon/FtkDbUtils.hpp>
#include <ftkcommon/FtkCool.hh>
#include <memory>

int main(int argc,char *argv[])
{
 
  CmdArgStr   partition_name ('p', "partition", "ATLAS", "partition to work in");
  CmdArgInt   runNumber('n',"runNumber","run-number","run number");
  CmdArgInt   region('r',"region","region-number","region number");
  CmdArgStr   dbConn('d',"dbConn","database-connection","database connection string",CmdArg::isVALREQ );
  CmdArgStr   dbTag('t',"dbTag","database-tag","database tag",CmdArg::isVALREQ);
  CmdArgStr   constantDir('c',"constDir","","directory gcon/con files");
  CmdArgStr   patternDir('b',"patternBankDir","","directory bank ROOT files");
  

  runNumber=-1;
  unsigned int runnumber;
  CmdLine  cmd(*argv, &partition_name,&runNumber,&dbConn,&dbTag, &constantDir,&patternDir,&region,NULL);
  CmdArgvIter  arg_iter(--argc, ++argv);
  cmd.parse(arg_iter);
  if(runNumber==-1) {
    try {
      IPCCore::init( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
      ers::fatal( ex );
      return 1;
    }
    IPCPartition   p( (const char*)partition_name );
    ISInfoDictionary isInfoDict=ISInfoDictionary(p);
    RunParams runParams; 
    isInfoDict.findValue("RunParams.RunParams",runParams);
    runnumber=runParams.run_number+1;
  } else
    runnumber=runNumber;
  FtkCool cool((const char*)dbConn,(const char *)dbTag,runnumber);
  std::cout << "dbConn="<<(const char*)dbConn<< std::endl;
  std::cout << "dbTag="<<(const char*)dbTag<< std::endl;
  std::cout << "runNumber="<<runnumber<< std::endl;
  std::string constDir;
  if(!constantDir.isNULL()) constDir=(const char*)constantDir;
  std::string bankDir;
  if(!patternDir.isNULL()) bankDir=(const char*)patternDir;
  if(bankDir!="") std::cout << "patternDir="<<bankDir<< std::endl;

  if(constDir!="") {
    std::cout << "constantDir="<<constDir<< std::endl;
    storage::variant bank8;
    storage::variant bank12;
    storage::variant conn;
    FtkDbUtils::readFitConstants<8>(constDir,region,bank8);
    FtkDbUtils::readFitConstants<12>(constDir,region,bank12);
    FtkDbUtils::readSectorConn(constDir,region,conn);
    cool.write(bank8,FtkCool::FtkConfigType::CORRGEN_RAW_8L_GCON,region);
    cool.write(bank12,FtkCool::FtkConfigType::CORRGEN_RAW_12L_GCON,region);
    cool.write(conn,FtkCool::FtkConfigType::SECTORS_RAW_8L_CONN,region);
  }
  if(bankDir!="") {
    storage::variant bank;
    FtkDbUtils::readPatternBank(bankDir,region,bank);
    cool.write(bank,FtkCool::FtkConfigType::PATTERNBANK_8M_8L,region);       
  }

 return 0;
}

