
#include "ftkcommon/ReadoutModule_FTK.h"
#include <dal/Segment.h>
#include <ctime>
#include "sys/times.h"
#include "sys/vtimes.h"
#include <chrono>
#include <thread>
#include <sched.h>


using namespace daq; using namespace ftk;

std::atomic<uint32_t> ReadoutModule_FTK::m_interruptPublish       (0x0);
std::atomic<uint32_t> ReadoutModule_FTK::m_configure_performed    (0x0);
std::atomic<uint32_t> ReadoutModule_FTK::m_connect_performed      (0x0);
std::atomic<uint32_t> ReadoutModule_FTK::m_prepareForRun_performed(0x0);
std::atomic<uint32_t> ReadoutModule_FTK::m_stopROIB_performed     (0x0);
std::atomic<uint32_t> ReadoutModule_FTK::m_stopDC_performed       (0x0);
std::atomic<uint32_t> ReadoutModule_FTK::m_stopHLT_performed      (0x0);
std::atomic<uint32_t> ReadoutModule_FTK::m_stopRecording_performed(0x0);
std::atomic<uint32_t> ReadoutModule_FTK::m_stopGathering_performed(0x0);
std::atomic<uint32_t> ReadoutModule_FTK::m_stopArchiving_performed(0x0);
std::atomic<uint32_t> ReadoutModule_FTK::m_disconnect_performed   (0x0);
std::atomic<uint32_t> ReadoutModule_FTK::m_unconfigure_performed  (0x0);
std::atomic<uint32_t> ReadoutModule_FTK::m_checkConnect_performed (0x0);
std::atomic<uint32_t> ReadoutModule_FTK::m_startNoDF_performed    (0x0);
std::atomic<uint32_t> ReadoutModule_FTK::m_checkConfig_performed    (0x0);
std::atomic<uint32_t> ReadoutModule_FTK::m_thread_limits_set      (0x0);
uint32_t ReadoutModule_FTK::m_configureThreadLimit                (0x0);
uint32_t ReadoutModule_FTK::m_connectThreadLimit                  (0x0);
uint32_t ReadoutModule_FTK::m_prepareForRunThreadLimit            (0x0);
uint32_t ReadoutModule_FTK::m_stopThreadLimit                     (0x0);
uint32_t ReadoutModule_FTK::m_disconnectThreadLimit               (0x0);
uint32_t ReadoutModule_FTK::m_unconfigureThreadLimit              (0x0);

std::map<std::string, ReadoutModule_FTK*> ReadoutModule_FTK::m_enabledReadoutModules{};
std::timed_mutex ReadoutModule_FTK::m_global_mtx{};
std::timed_mutex ReadoutModule_FTK::m_global_monitoring_mtx{};

//static std::unique_ptr<std::thread> thread_ptr;
static std::thread monitoringMemoryUsageThread;



double parseLine_memory(char* line){
  int i = strlen(line);
  const char* p = line;
  while (*p <'0' || *p > '9') p++;
  line[i-3] = '\0';
  i = atoi(p);
  return i;
}

/// Read the affinity setting
/// TODO: move the function to ftkutils
uint64_t readAffinity()
{
  uint64_t affinity=0x0;
  cpu_set_t mask;
  if (sched_getaffinity(0, sizeof(cpu_set_t), &mask) == -1) 
    return 0x0;
  else
  {
    long nproc = sysconf(_SC_NPROCESSORS_ONLN);
    for (int i = 0; i < nproc; i++) 
    {
      if(CPU_ISSET(i, &mask))
        affinity |= (1<<i);
    }
  }
  return affinity;
}


bool isAPowerOf2(uint64_t x)
{
  return ((x & ~(x-1))==x)? x : 0;
}

/// Fix the affinity setting (use all the cores)
/// TODO: move the function to ftkutils
void fixAffinity()
{
  static std::atomic<bool> affinityCheck(false);
  bool expected = false;
  if (!affinityCheck.compare_exchange_strong(expected,true))
    return;

  // Read affinity
  uint64_t affinity = readAffinity();
  if(affinity==0)
  { 
    ers::warning( FTKIssue( ERS_HERE, "", "Error reading the CPU affinity" ) );
    return;
  }

  // If a core is missing, reset it
  if (not isAPowerOf2(affinity) or affinity==0x1) // Is a power of 2?
  {
    long nproc = sysconf(_SC_NPROCESSORS_ONLN);
    cpu_set_t myset;
    CPU_ZERO(&myset);
    for (int i = 0; i < nproc; i++)
      CPU_SET(i, &myset);
    if (sched_setaffinity(0, sizeof(cpu_set_t), &myset) == -1)
    { 
      ers::warning( FTKIssue( ERS_HERE, "", "Error setting the CPU affinity" ) );
      return;
    }
    uint64_t affinityOld = affinity;
    affinity = readAffinity();
    if(affinity==0)
    { 
      ers::warning( FTKIssue( ERS_HERE, "", "Error reading the CPU affinity" ) );
      return;
    }
    stringstream o;
    o << "Forced affinity change: from 0x" << std::hex << affinityOld << " to 0x" << affinity;
    ers::warning( FTKIssue( ERS_HERE, "", o.str() ) );
  }

  ERS_LOG("Affinity 0x" << std::hex << affinity);
  return; 
}


void monitoringMemoryUsage(){

  auto m_ipcpartition      = daq::rc::OnlineServices::instance().getIPCPartition();
  auto app_name      = daq::rc::OnlineServices::instance().applicationName();
  std::string isProvider =  "DF.AppResourceMonitoring." + app_name;

  auto m_FTKReadoutCommonNamed = new FTKReadoutCommonNamed( m_ipcpartition, isProvider.c_str() );

  struct tms timeSample;
  clock_t now;
 // INITIALIZE CPU
  double  m_lastCPU = times(&timeSample);
  double  m_lastSysCPU = timeSample.tms_stime;
  double m_lastUserCPU = timeSample.tms_utime;

  while(true){
    FILE* file = fopen("/proc/self/status", "r");
    double memory_used = 0;
    char line[128];
    while (fgets(line, 128, file) != NULL){
      if (strncmp(line, "VmRSS:", 6) == 0){
        memory_used = parseLine_memory(line);
        break;
        }
    }
    fclose(file);
//CPU

    double percent;

    now = times(&timeSample);
    if (now <= m_lastCPU || timeSample.tms_stime < m_lastSysCPU ||
      timeSample.tms_utime < m_lastUserCPU){
      percent = -1.0;
    }
    else{
      percent = (timeSample.tms_stime - m_lastSysCPU) +
              (timeSample.tms_utime - m_lastUserCPU);
      percent /= (now - m_lastCPU);
      percent *= 100;
    }
    m_lastCPU = now;
    m_lastSysCPU = timeSample.tms_stime;
    m_lastUserCPU = timeSample.tms_utime;
    

    m_FTKReadoutCommonNamed->CPUUsage = percent;
    m_FTKReadoutCommonNamed->MemoryUsage = memory_used; 

    // Every 5 s mean 200kB ora. Doable
    ERS_LOG("Resource monitor: cpu " << percent << "\t mem " << memory_used );
   
    m_FTKReadoutCommonNamed->checkin();
    sleep(5);
 
  }
}

ReadoutModule_FTK::~ReadoutModule_FTK() noexcept {
  if ( !m_initialized ) return;
  std::unique_lock<std::timed_mutex> lck( m_global_mtx, std::defer_lock );
  const uint32_t NMINUTES_TO_WAIT = 1;
  if ( attempt_lock_for_n_minutes( lck, NMINUTES_TO_WAIT ) ) {
    // std::map<std::string,?>::Compare is noexcept (see operator < for std::basic_string)
    m_enabledReadoutModules.erase( m_name );
  } else {
    ers::error( FTKLockFailed( ERS_HERE, m_name, "global", "destructor", NMINUTES_TO_WAIT, "Could not remove this object from the ReadoutModule map. This will cause a seg fault." ) );
  }
}

ReadoutModule_FTK::ReadoutModule_FTK()
  : m_initialized( 0x0 ) {
  fixAffinity();
}

void ReadoutModule_FTK::initialize( const std::string& name, const daq::ftk::dal::ReadoutModule_FTK* config_dal, ftk_board_t type )
{

  static std::thread monitoringMemoryUsageThread_tmp( []() { monitoringMemoryUsage(); } );

  if ( type==BOARD_UNKNOWN )
    return;

  if ( m_initialized++ ) {
    ers::error( ftkException( ERS_HERE, name, "This ReadoutModule_FTK object has already been initialized. Cannot initialize a second time!" ) );
    m_initialized = 1;
    return;
  }

  uint32_t config = getConfigFlagsFromDAL( config_dal );

  // Dump dal configuration for debugging purpose
  ERS_LOG("ReadoutModule_FTK oks configuration: \n" << *config_dal);

  // Dump RCD dal configuration only one time 
  static std::atomic<bool> doneRcdConfigDump(false);
  bool expected = false;
  if (doneRcdConfigDump.compare_exchange_strong(expected,true))
  {
    ERS_LOG("RCD oks configuration: \n" << *getReadoutConfig());
  }

  resetPublishFlag();

  m_name = name;
  m_type = type;
  m_configuration = config;

  // get the manager name for stopless recover
  getManager();

  m_enablePublish = 0x0;

  std::unique_lock<std::timed_mutex> lck ( m_global_mtx, std::defer_lock );
  const uint32_t NMINUTES_TO_WAIT = 1;
  if ( attempt_lock_for_n_minutes( lck, NMINUTES_TO_WAIT ) ) {
    if ( m_enabledReadoutModules.find( name ) != m_enabledReadoutModules.end() ) {
      ers::error( ftkException( ERS_HERE, name, "Multiple ReadoutModule objects with the same name! This will cause transitions to fail." ) );
    } else {
      m_enabledReadoutModules[ name ] = this;
    }
  } else {
    ers::error( FTKLockFailed( ERS_HERE, m_name, "global", "initialize()", NMINUTES_TO_WAIT, "Cannot add this readout module to the global map. This will cause transitions to fail.") );
  }

  if ( !(m_thread_limits_set++) ) {
    m_configureThreadLimit = configureThreadLimit();
    m_connectThreadLimit = connectThreadLimit();
    m_prepareForRunThreadLimit = prepareForRunThreadLimit();
    m_stopThreadLimit = stopThreadLimit();
    m_disconnectThreadLimit = disconnectThreadLimit();
    m_unconfigureThreadLimit = unconfigureThreadLimit();
  } else {
    m_thread_limits_set = 1;
  }

}


const daq::ftk::dal::ReadoutConfiguration_FTK* ReadoutModule_FTK::getReadoutConfig()
{
  Configuration& conf = daq::rc::OnlineServices::instance().getConfiguration();
  auto dal_rcdAppl   = daq::rc::OnlineServices::instance().getApplication().cast<daq::df::RCD>();
  return conf.cast<daq::ftk::dal::ReadoutConfiguration_FTK>( dal_rcdAppl->get_Configuration() );
}

uint32_t ReadoutModule_FTK::configureThreadLimit()
{
  int configThreadLimit = getReadoutConfig()->get_ConfigureThreadLimit();
  return ( configThreadLimit > 0 ) ? (uint32_t) configThreadLimit : globalThreadLimit();
}

uint32_t ReadoutModule_FTK::connectThreadLimit()
{
  int connectThreadLimit = getReadoutConfig()->get_ConnectThreadLimit();
  return ( connectThreadLimit > 0 ) ? (uint32_t) connectThreadLimit : globalThreadLimit();
}

uint32_t ReadoutModule_FTK::prepareForRunThreadLimit()
{
  int startThreadLimit = getReadoutConfig()->get_PrepareForRunThreadLimit();
  return ( startThreadLimit > 0 ) ? (uint32_t) startThreadLimit : globalThreadLimit();
}

uint32_t ReadoutModule_FTK::stopThreadLimit()
{
  int stopThreadLimit = getReadoutConfig()->get_StopThreadLimit();
  return ( stopThreadLimit > 0 ) ? (uint32_t) stopThreadLimit : globalThreadLimit();
}

uint32_t ReadoutModule_FTK::disconnectThreadLimit()
{
  int disconnectThreadLimit = getReadoutConfig()->get_DisconnectThreadLimit();
  return ( disconnectThreadLimit > 0 ) ? (uint32_t) disconnectThreadLimit : globalThreadLimit();
}

uint32_t ReadoutModule_FTK::unconfigureThreadLimit()
{
  int unconfigureThreadLimit = getReadoutConfig()->get_UnconfigureThreadLimit();
  return ( unconfigureThreadLimit > 0 ) ? (uint32_t) unconfigureThreadLimit : globalThreadLimit();
}

// Enable monitoring of the board
// If parallelization is desired, create one or two thread(s) here for publish/publishFullStats
void ReadoutModule_FTK::enablePublish()
{
  if ( !m_initialized )
    return;

  m_enablePublish = 0x0;
  m_interruptPublish = 0x0;

  // lock the mutex prior to starting new publish threads to ensure that any existing ones have exited
  std::unique_lock<std::timed_mutex> lck ( m_local_mtx, std::defer_lock );
  const uint32_t NMINUTES_TO_WAIT = 4;
  if ( !attempt_lock_for_n_minutes( lck, NMINUTES_TO_WAIT ) ) {
    ers::warning( FTKLockFailed( ERS_HERE, m_name, "board", "enablePublish()", NMINUTES_TO_WAIT, "Not starting publish threads." ) );
    m_enablePublish = 0x1;
    return;
  }

  // now that we're sure any old publish threads have exited, enable publish and start new ones
  m_enablePublish = 0x1;

  if ( m_configuration & READOUTMODULE_FTK_PARALLEL_MONITORING ) {
    if ( m_configuration & READOUTMODULE_FTK_SEPARATE_PUBLISH_THREADS ) {
      std::thread publishFullStatsThread( [&]() { publishFullStatsLauncher(); } );
      publishFullStatsThread.detach();
      std::thread publishThread( [&]() { publishLauncher(); } );
      publishThread.detach();
    } else {
      std::thread monitoringThread( [&]() { monitoringLauncher(); } );
      monitoringThread.detach();
    }
  }

}

// invoke the publishFullStats callback function when appropriate
// should run in a separate thread
void ReadoutModule_FTK::publishFullStatsLauncher()
{
  ERS_LOG( "PublishFullStats thread starting." );
  while ( m_enablePublish and !m_interruptPublish ) {
    if ( m_publishFullStatsQueue ) {
      m_publishFullStatsQueue--;
      uint32_t flag = nextPublishFullStatsFlag() | READOUTMODULE_FTK_PUBLISHFULLSTATS_BIT;
      std::unique_lock<std::timed_mutex> lck ( m_local_mtx, std::defer_lock );
      const uint32_t NMINUTES_TO_WAIT = 1;
      if ( attempt_lock_for_n_minutes( lck, NMINUTES_TO_WAIT ) && m_enablePublish && !m_interruptPublish ) {
        std::unique_lock<std::timed_mutex> lck2 ( m_global_monitoring_mtx, std::defer_lock );
        if ( ( !(m_configuration & READOUTMODULE_FTK_SERIALIZE_RCD_MONITORING)
                  || attempt_lock_for_n_minutes( lck2, NMINUTES_TO_WAIT ) ) 
                              && m_enablePublish && !m_interruptPublish ) {
          doPublishFullStats( flag );
        }
      } else {
        // could not acquire lock
      }
    } else { // no pending requests
      sleep(1);
    }
  }
  ERS_LOG( "PublishFullStats thread exiting." );
}

void ReadoutModule_FTK::publishLauncher()
{
  ERS_LOG( "Publish thread starting." );
  while ( m_enablePublish and !m_interruptPublish ) {
    if ( m_publishQueue ) {
      m_publishQueue--;
      uint32_t flag = nextPublishFlag();
      std::unique_lock<std::timed_mutex> lck ( m_local_mtx, std::defer_lock );
      const uint32_t NMINUTES_TO_WAIT = 2;
      if ( attempt_lock_for_n_minutes( lck, NMINUTES_TO_WAIT ) && m_enablePublish && !m_interruptPublish ) {
        std::unique_lock<std::timed_mutex> lck2 ( m_global_monitoring_mtx, std::defer_lock );
        if ( ( !(m_configuration & READOUTMODULE_FTK_SERIALIZE_RCD_MONITORING)
                  || attempt_lock_for_n_minutes( lck2, NMINUTES_TO_WAIT ) ) 
                              && m_enablePublish && !m_interruptPublish ) {
          doPublish( flag, false );
        }
      } else {
        // could not acquire lock
      }
    } else { // no pending requests
      sleep(1);
    }
  }
  ERS_LOG( "Publish thread exiting." );
}

// this one is exclusive with the previous two, serializes publish/publishFullStats
void ReadoutModule_FTK::monitoringLauncher()
{
  ERS_LOG( "Monitoring thread starting." );
  while ( m_enablePublish and !m_interruptPublish ) {
    bool isPublishFullStats;
    if ( m_monitoringQueue.try_pop( isPublishFullStats ) ) {
      std::unique_lock<std::timed_mutex> lck ( m_local_mtx, std::defer_lock );
      const uint32_t NMINUTES_TO_WAIT = 2;
      if ( attempt_lock_for_n_minutes( lck, NMINUTES_TO_WAIT ) && m_enablePublish && !m_interruptPublish ) {
        std::unique_lock<std::timed_mutex> lck2 ( m_global_monitoring_mtx, std::defer_lock );
        if ( ( !(m_configuration & READOUTMODULE_FTK_SERIALIZE_RCD_MONITORING)
                  || attempt_lock_for_n_minutes( lck2, NMINUTES_TO_WAIT ) ) 
                              && m_enablePublish && !m_interruptPublish ) {
          if ( isPublishFullStats ) {
            doPublishFullStats( nextPublishFullStatsFlag() | READOUTMODULE_FTK_PUBLISHFULLSTATS_BIT );
          } else {
              doPublish( nextPublishFlag(), false );
          }
        }
      } else {
        // could not acquire lock
      }
    } else { // no pending requests
      sleep(1);
    }
  }
  ERS_LOG( "Monitoring thread exiting." );
}

void ReadoutModule_FTK::configure( const daq::rc::TransitionCmd& cmd ) {
  if ( !m_initialized )
  {
    ERS_LOG("Board not initialized! Transition " << cmd.toString() << " skipped..");
    return;
  }

  m_unconfigure_performed = 0;

  if ( m_configuration & READOUTMODULE_FTK_PARALLEL_CONFIGURE ) {
    if ( !(m_configure_performed++) ) {
      ftkTimeLog tLog(ERS_HERE, name_ftk(), "Parallel configure transition");
      {
      ThreadPool theTP( m_name, m_configureThreadLimit );
      for ( auto& rm : m_enabledReadoutModules ) {
        // check that the other RM has parallel configure enabled
        if ( rm.second->m_configuration & READOUTMODULE_FTK_PARALLEL_CONFIGURE ) {
          ERS_LOG( "Enqueuing configure for ReadoutModule named " << rm.first );
          theTP.enqueue( [&]() { 
            try{
              rm.second->doConfigure( cmd ); 
            }
            catch( ers::Issue &ex)
            {
              ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during Configure transition", ex);
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }
            catch( std::exception &ex)
            {
              ftkException err(ERS_HERE, name_ftk(), "Caught a major standard c++ issue during Configure transition", ex);
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }
            catch(...)
            {
              ftkException err(ERS_HERE, name_ftk(), "Caught a major standard c++ issue during Configure transition");
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }
          } );
        }
      }
      ERS_LOG( "Finished launching parallel configure." );
      } // the ThreadPool dtor hangs until all enqueued tasks are completed
      tLog.end(ERS_HERE); // Close ftkTimeLog 
    } else {
      ERS_LOG( "Parallel configure transition has already occurred." );
    }
  } 
  else 
  {
    try{
      doConfigure( cmd );
    }
    catch( ers::Issue &ex)
    {
      ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during Configure transition", ex);
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }
    catch( std::exception &ex)
    {
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during Configure transition", ex);
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }
    catch(...)
    {
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during Configure transition");
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }
  }
}

void ReadoutModule_FTK::connect( const daq::rc::TransitionCmd& cmd ) {
  if ( !m_initialized )
  { 
    ERS_LOG("Board not initialized! Transition " << cmd.toString() << " skipped..");
    return;
  }

  m_disconnect_performed = 0;
  m_checkConnect_performed = 0;

  if ( m_configuration & READOUTMODULE_FTK_PARALLEL_CONNECT ) {
    if ( !(m_connect_performed++) ) {
      ftkTimeLog tLog(ERS_HERE, name_ftk(), "Parallel connect transition");
      {
      ThreadPool theTP( m_name, m_connectThreadLimit );
      for ( auto& rm : m_enabledReadoutModules ) {
        if ( rm.second->m_configuration & READOUTMODULE_FTK_PARALLEL_CONNECT ) {
          ERS_LOG( "Enqueuing connect for ReadoutModule named " << rm.first );
          theTP.enqueue( [&]() { 
            try{
              rm.second->doConnect( cmd ); 
            }
            catch( ers::Issue &ex)
            {
              ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during Connect transition", ex);
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }
            catch( std::exception &ex)
            {
              ftkException err(ERS_HERE, name_ftk(), "Caught a major standard c++ issue during Connect transition", ex);
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }
            catch(...)
            {
              ftkException err(ERS_HERE, name_ftk(), "Caught a major standard c++ issue during Connect transition");
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }
          } );
        }
      }
      ERS_LOG( "Finished launching parallel connect." );
      }
      tLog.end(ERS_HERE); // Close ftkTimeLog
    } else {
      ERS_LOG( "Parallel connect transition has already occurred." );
    }
  } else {
    try{
     doConnect( cmd );
    }
    catch( ers::Issue &ex)
    {
      ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during Connect transition", ex);
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }   
    catch( std::exception &ex)
    {
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during Connect transition", ex);
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }
    catch(...)
    {
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during Connect transition");
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }
  }
}

void ReadoutModule_FTK::prepareForRun( const daq::rc::TransitionCmd& cmd ) {
  if ( !m_initialized )
  { 
    ERS_LOG("Board not initialized! Transition " << cmd.toString() << " skipped..");
    return;
  }

  m_stopROIB_performed = 0;
  m_stopDC_performed = 0;
  m_stopHLT_performed = 0;
  m_stopRecording_performed = 0;
  m_stopGathering_performed = 0;
  m_stopArchiving_performed = 0;

  if ( m_configuration & READOUTMODULE_FTK_PARALLEL_PREPAREFORRUN ) {
    if ( !(m_prepareForRun_performed++) ) {
      ftkTimeLog tLog(ERS_HERE, name_ftk(), "Parallel prepareForRun transition");
      {
      ThreadPool theTP( m_name, m_prepareForRunThreadLimit );
      for ( auto& rm : m_enabledReadoutModules ) {
        if ( rm.second->m_configuration & READOUTMODULE_FTK_PARALLEL_PREPAREFORRUN ) {
          ERS_LOG( "Enqueuing prepareForRun for ReadoutModule named " << rm.first );
          theTP.enqueue( [&]() { 
            try{
              rm.second->prepareForRun_atomic( cmd ); 
            }
            catch( ers::Issue &ex)
            { 
              ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during prepareForRun transition", ex);
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }
            catch( std::exception &ex)
            {
              ftkException err(ERS_HERE, name_ftk(), "Caught a major standard c++ issue during prepareForRun transition", ex);
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }
            catch(...)
            {
              ftkException err(ERS_HERE, name_ftk(), "Caught a major standard c++ issue during prepareForRun transition");
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }
          } );
        }
      }
      ERS_LOG( "Finished launching parallel prepareForRun." );
      }
      tLog.end(ERS_HERE); // Close ftkTimeLog
    } else {
      ERS_LOG( "Parallel prepareForRun transition has already occurred." );
    }
  } else {
    try{
     prepareForRun_atomic( cmd ); 
    }
    catch( ers::Issue &ex)
    {
      ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during prepareForRun transition", ex);
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }
    catch( std::exception &ex)
    {
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during prepareForRun transition", ex);
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }
    catch(...)
    {
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during prepareForRun transition");
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }
  }
}

void ReadoutModule_FTK::stopROIB( const daq::rc::TransitionCmd& cmd ) {
  if ( !m_initialized )
  { 
    ERS_LOG("Board not initialized! Transition " << cmd.toString() << " skipped..");
    return;
  }

  if ( m_configuration & READOUTMODULE_FTK_PARALLEL_STOPROIB ) {
    if ( !(m_stopROIB_performed++) ) {
      ftkTimeLog tLog(ERS_HERE, name_ftk(), "Parallel stopROIB transition");
      {
      ThreadPool theTP( m_name, m_stopThreadLimit );
      for ( auto& rm : m_enabledReadoutModules ) {
        if ( rm.second->m_configuration & READOUTMODULE_FTK_PARALLEL_STOPROIB ) {
          ERS_LOG( "Enqueuing stopROIB for ReadoutModule named " << rm.first );
          theTP.enqueue( [&]() { 
            try{
              rm.second->stopROIB_atomic( cmd ); 
            }
            catch( ers::Issue &ex)
            {
              ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during stopROIB transition", ex);
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }
            catch( std::exception &ex)
            {
              ftkException err(ERS_HERE, name_ftk(), "Caught a major standard c++ issue during stopROIB transition", ex);
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }
            catch(...)
            {
              ftkException err(ERS_HERE, name_ftk(), "Caught a major standard c++ issue during stopROIB transition");
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }

          } );
        }
      }
      ERS_LOG( "Finished launching parallel prepareForRun." );
      }
      tLog.end(ERS_HERE); // Close ftkTimeLog
    } else {
      ERS_LOG( "Parallel stopROIB transition has already occurred." );
    }
  } else {
    try{
     stopROIB_atomic( cmd );
    }
    catch( ers::Issue &ex)
    {
      ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during stopROIB transition", ex);
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }
    catch( std::exception &ex)
    {
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during stopROIB transition", ex);
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }
    catch(...)
    {
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during stopROIB transition");
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }
  }
}

void ReadoutModule_FTK::stopDC( const daq::rc::TransitionCmd& cmd ) {
  if ( !m_initialized )
  { 
    ERS_LOG("Board not initialized! Transition " << cmd.toString() << " skipped..");
    return;
  }

  if ( m_configuration & READOUTMODULE_FTK_PARALLEL_STOPDC ) {
    if ( !(m_stopDC_performed++) ) {
      ftkTimeLog tLog(ERS_HERE, name_ftk(), "Parallel stopDC transition");
      {
      ThreadPool theTP( m_name, m_stopThreadLimit );
      for ( auto& rm : m_enabledReadoutModules ) {
        // check that the other RM has parallel configure enabled
        if ( rm.second->m_configuration & READOUTMODULE_FTK_PARALLEL_STOPDC ) {
          ERS_LOG( "Enqueuing stopDC for ReadoutModule named " << rm.first );
          theTP.enqueue( [&]() { 
            try{
              rm.second->doStopDC( cmd ); 
            }
            catch( ers::Issue &ex)
            {
              ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during stopDC transition", ex);
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }
            catch( std::exception &ex)
            {
              ftkException err(ERS_HERE, name_ftk(), "Caught a major standard c++ issue during stopDC transition", ex);
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }
            catch(...)
            {
              ftkException err(ERS_HERE, name_ftk(), "Caught a major standard c++ issue during stopDC transition");
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }
          } );
        }
      }
      ERS_LOG( "Finished launching parallel stopDC." );
      }
      tLog.end(ERS_HERE); // Close ftkTimeLog
    } else {
      ERS_LOG( "Parallel stopDC transition has already occurred." );
    }
  } else {
    try{
     doStopDC( cmd );
    }
    catch( ers::Issue &ex)
    { 
      ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during stopDC transition", ex);
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }
    catch( std::exception &ex)
    {
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during stopDC transition", ex);
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }
    catch(...)
    {
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during stopDC transition");
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }
  }
}

void ReadoutModule_FTK::stopHLT( const daq::rc::TransitionCmd& cmd ) {
  if ( !m_initialized )
  { 
    ERS_LOG("Board not initialized! Transition " << cmd.toString() << " skipped..");
    return;
  }

  if ( m_configuration & READOUTMODULE_FTK_PARALLEL_STOPHLT ) {
    if ( !(m_stopHLT_performed++) ) {
      ftkTimeLog tLog(ERS_HERE, name_ftk(), "Parallel stopHLT transition");
      {
      ThreadPool theTP( m_name, m_stopThreadLimit );
      for ( auto& rm : m_enabledReadoutModules ) {
        if ( rm.second->m_configuration & READOUTMODULE_FTK_PARALLEL_STOPHLT ) {
          ERS_LOG( "Enqueuing stopHLT for ReadoutModule named " << rm.first );
          theTP.enqueue( [&]() { 
            try{
              rm.second->doStopHLT( cmd ); 
            }
            catch( ers::Issue &ex)
            { 
              ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during stopHLT transition", ex);
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }
            catch( std::exception &ex)
            {
              ftkException err(ERS_HERE, name_ftk(), "Caught a major standard c++ issue during stopHLT transition", ex);
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }
            catch(...)
            {
              ftkException err(ERS_HERE, name_ftk(), "Caught a major standard c++ issue during stopHLT transition");
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }
          } );
        }
      }
      ERS_LOG( "Finished launching parallel stopHLT." );
      }
      tLog.end(ERS_HERE); // Close ftkTimeLog
    } else {
      ERS_LOG( "Parallel stopHLT transition has already occurred." );
    }
  } else {
    try{
      doStopHLT( cmd );
    }
    catch( ers::Issue &ex)
    {
      ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during stopHLT transition", ex);
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }
    catch( std::exception &ex)
    {
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during stopHLT transition", ex);
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }
    catch(...)
    {
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during stopHLT transition");
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    } 
  }
}

void ReadoutModule_FTK::stopRecording( const daq::rc::TransitionCmd& cmd ) {
  if ( !m_initialized )
  { 
    ERS_LOG("Board not initialized! Transition " << cmd.toString() << " skipped..");
    return;
  }

  if ( m_configuration & READOUTMODULE_FTK_PARALLEL_STOPRECORDING ) {
    if ( !(m_stopRecording_performed++) ) {
      ftkTimeLog tLog(ERS_HERE, name_ftk(), "Parallel stopRecording transition");
      {
      ThreadPool theTP( m_name, m_stopThreadLimit );
      for ( auto& rm : m_enabledReadoutModules ) {
        if ( rm.second->m_configuration & READOUTMODULE_FTK_PARALLEL_STOPRECORDING ) {
          ERS_LOG( "Enqueuing stopRecording for ReadoutModule named " << rm.first );
          theTP.enqueue( [&]() { 
            try{
              rm.second->doStopRecording( cmd ); 
            }
            catch( ers::Issue &ex)
            { 
              ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during stopRecording transition", ex);
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }
            catch( std::exception &ex)
            {
              ftkException err(ERS_HERE, name_ftk(), "Caught a major standard c++ issue during stopRecording transition", ex);
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }
            catch(...)
            {
              ftkException err(ERS_HERE, name_ftk(), "Caught a major standard c++ issue during stopRecording transition");
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }
          } );
        }
      }
      ERS_LOG( "Finished launching parallel stopRecording." );
      }
      tLog.end(ERS_HERE); // Close ftkTimeLog
    } else {
      ERS_LOG( "Parallel stopRecording transition has already occurred." );
    }
  } else {
    try{
      doStopRecording( cmd );
    }
    catch( ers::Issue &ex)
    { 
      ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during stopRecording transition", ex);
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }
    catch( std::exception &ex)
    {
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during stopRecording transition", ex);
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }
    catch(...)
    {
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during stopRecording transition");
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }
  }
}

void ReadoutModule_FTK::stopGathering( const daq::rc::TransitionCmd& cmd ) {
  if ( !m_initialized )
  { 
    ERS_LOG("Board not initialized! Transition " << cmd.toString() << " skipped..");
    return;
  }

  if ( m_configuration & READOUTMODULE_FTK_PARALLEL_STOPGATHERING ) {
    if ( !(m_stopGathering_performed++) ) {
      ftkTimeLog tLog(ERS_HERE, name_ftk(), "Parallel stopGathering transition");
      {
      ThreadPool theTP( m_name, m_stopThreadLimit );
      for ( auto& rm : m_enabledReadoutModules ) {
        if ( rm.second->m_configuration & READOUTMODULE_FTK_PARALLEL_STOPGATHERING ) {
          ERS_LOG( "Enqueuing stopGathering for ReadoutModule named " << rm.first );
          theTP.enqueue( [&]() { 
            try{
              rm.second->doStopGathering( cmd ); 
            }
            catch( ers::Issue &ex)
            { 
              ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during stopGathering transition", ex);
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }
            catch( std::exception &ex)
            {
              ftkException err(ERS_HERE, name_ftk(), "Caught a major standard c++ issue during stopGathering transition", ex);
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }
            catch(...)
            {
              ftkException err(ERS_HERE, name_ftk(), "Caught a major standard c++ issue during stopGathering transition");
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }
          } );
        }
      }
      tLog.end(ERS_HERE); // Close ftkTimeLog
      }
      ERS_LOG( "Parallel stopGathering transition completed." );
    } else {
      ERS_LOG( "Parallel stopGathering transition has already occurred." );
    }
  } else {
    try{
      doStopGathering( cmd );
    }
    catch( ers::Issue &ex)
    { 
      ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during stopGathering transition", ex);
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }
    catch( std::exception &ex)
    {
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during stopGathering transition", ex);
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }
    catch(...)
    {
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during stopGathering transition");
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }
  }
}

void ReadoutModule_FTK::stopArchiving( const daq::rc::TransitionCmd& cmd ) {
  if ( !m_initialized )
  { 
    ERS_LOG("Board not initialized! Transition " << cmd.toString() << " skipped..");
    return;
  }

  m_prepareForRun_performed = 0;

  if ( m_configuration & READOUTMODULE_FTK_PARALLEL_STOPARCHIVING ) {
    if ( !(m_stopArchiving_performed++) ) {
      ftkTimeLog tLog(ERS_HERE, name_ftk(), "Parallel stopArchiving transition");
      {
      ThreadPool theTP( m_name, m_stopThreadLimit );
      for ( auto& rm : m_enabledReadoutModules ) {
        if ( rm.second->m_configuration & READOUTMODULE_FTK_PARALLEL_STOPARCHIVING ) {
          ERS_LOG( "Enqueuing stopArchiving for ReadoutModule named " << rm.first );
          theTP.enqueue( [&]() { 
            try{
              rm.second->doStopArchiving( cmd ); 
            }
            catch( ers::Issue &ex)
            { 
              ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during stopArchiving transition", ex);
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }
            catch( std::exception &ex)
            {
              ftkException err(ERS_HERE, name_ftk(), "Caught a major standard c++ issue during stopArchiving transition", ex);
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }
            catch(...)
            {
              ftkException err(ERS_HERE, name_ftk(), "Caught a major standard c++ issue during stopArchiving transition");
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }
          } );
        }
      }
      ERS_LOG( "Finished launching parallel stopArchiving." );
      }
      tLog.end(ERS_HERE); // Close ftkTimeLog
    } else {
      ERS_LOG( "Parallel stopArchiving transition has already occurred." );
    }
  } else {
    try{
      doStopArchiving( cmd );
    }
    catch( ers::Issue &ex)
    { 
      ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during stopArchiving transition", ex);
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }
    catch( std::exception &ex)
    {
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during stopArchiving transition", ex);
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }
    catch(...)
    {
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during stopArchiving transition");
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }
  }
}

void ReadoutModule_FTK::disconnect( const daq::rc::TransitionCmd& cmd ) {
  if ( !m_initialized )
  { 
    ERS_LOG("Board not initialized! Transition " << cmd.toString() << " skipped..");
    return;
  }

  m_connect_performed = 0;

  if ( m_configuration & READOUTMODULE_FTK_PARALLEL_DISCONNECT ) {
    if ( !(m_disconnect_performed++) ) {
      ftkTimeLog tLog(ERS_HERE, name_ftk(), "Parallel disconnect transition");
      {
      ThreadPool theTP( m_name, m_disconnectThreadLimit );
      for ( auto& rm : m_enabledReadoutModules ) {
        if ( rm.second->m_configuration & READOUTMODULE_FTK_PARALLEL_DISCONNECT ) {
          ERS_LOG( "Enqueuing disconnect for ReadoutModule named " << rm.first );
          theTP.enqueue( [&]() { 
            try{
              rm.second->doDisconnect( cmd ); 
            }
            catch( ers::Issue &ex)
            { 
              ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during Disconnect transition", ex);
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }
            catch( std::exception &ex)
            {
              ftkException err(ERS_HERE, name_ftk(), "Caught a major standard c++ issue during Disconnect transition", ex);
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }
            catch(...)
            {
              ftkException err(ERS_HERE, name_ftk(), "Caught a major standard c++ issue during Disconnect transition");
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }
          } );
        }
      }
      ERS_LOG( "Finished launching parallel disconnect." );
      }
      tLog.end(ERS_HERE); // Close ftkTimeLog
    } else {
      ERS_LOG( "Parallel disconnect transition already done." );
    }
  } else {
    try{
      doDisconnect( cmd );
    }
    catch( ers::Issue &ex)
    { 
      ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during Disconnect transition", ex);
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }
    catch( std::exception &ex)
    {
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during Disconnect transition", ex);
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }
    catch(...)
    {
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during Disconnect transition");
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }
  }
}

void ReadoutModule_FTK::unconfigure( const daq::rc::TransitionCmd& cmd ) {
  if ( !m_initialized )
  { 
    ERS_LOG("Board not initialized! Transition " << cmd.toString() << " skipped..");
    return;
  }

  m_configure_performed = 0;
 // m_thread_mon_close = 1;

  if ( m_configuration & READOUTMODULE_FTK_PARALLEL_UNCONFIGURE ) {
    if ( !(m_unconfigure_performed++) ) {
      ftkTimeLog tLog(ERS_HERE, name_ftk(), "Parallel unconfigure transition");
      {
      ThreadPool theTP( m_name, m_unconfigureThreadLimit );
      for ( auto& rm : m_enabledReadoutModules ) {
        if ( rm.second->m_configuration & READOUTMODULE_FTK_PARALLEL_UNCONFIGURE ) {
          ERS_LOG( "Enqueuing unconfigure for ReadoutModule named " << rm.first );
          theTP.enqueue( [&]() { 
            try{
              rm.second->doUnconfigure( cmd ); 
            }
            catch( ers::Issue &ex)
            { 
              ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during Unconfigure transition", ex);
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }
            catch( std::exception &ex)
            {
              ftkException err(ERS_HERE, name_ftk(), "Caught a major standard c++ issue during Unconfigure transition", ex);
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }
            catch(...)
            {
              ftkException err(ERS_HERE, name_ftk(), "Caught a major standard c++ issue during Unconfigure transition");
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }
          } );
        }
      }
      ERS_LOG( "Finished launching parallel unconfigure." );
      }
      tLog.end(ERS_HERE); // Close ftkTimeLog
    } else {
      ERS_LOG( "Parallel unconfigure transition already performed." );
    }
  } else {
    try{
      doUnconfigure( cmd );
    }
    catch( ers::Issue &ex)
    { 
      ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during Unconfigure transition", ex);
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }
    catch( std::exception &ex)
    {
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during Unconfigure transition", ex);
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }
    catch(...)
    {
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during Unconfigure transition");
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }
  }

  //thread_ptr->join();

}

void ReadoutModule_FTK::callCheckConfig( const daq::rc::SubTransitionCmd& cmd ) {
  if ( !m_initialized )
  { 
    ERS_LOG("Board not initialized! Transition " << cmd.toString() << " skipped..");
    return;
  }

  if ( m_configuration & READOUTMODULE_FTK_PARALLEL_CONFIGURE ) {
    if ( !(m_checkConfig_performed++) ) {
      ftkTimeLog tLog(ERS_HERE, name_ftk(), "Parallel checkConfig transition");
      {
      ThreadPool theTP( m_name, m_prepareForRunThreadLimit );
      for ( auto& rm : m_enabledReadoutModules ) {
        if ( rm.second->m_configuration & READOUTMODULE_FTK_PARALLEL_CONFIGURE ) {
          ERS_LOG( "Enqueuing checkConfig for ReadoutModule named " << rm.first );
          theTP.enqueue( [&]() { 
            try{
              rm.second->checkConfig( cmd ); 
            }
            catch( ers::Issue &ex)
            { 
              ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during checkConfig sub-transition", ex);
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }
            catch( std::exception &ex)
            {
              ftkException err(ERS_HERE, name_ftk(), "Caught a major standard c++ issue during checkConfig sub-transition", ex);
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }
            catch(...)
            {
              ftkException err(ERS_HERE, name_ftk(), "Caught a major standard c++ issue during checkConfig sub-transition");
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }          } );
        }
      }
      ERS_LOG( "Finished launching parallel checkConfig." );
      }
      tLog.end(ERS_HERE); // Close ftkTimeLog
    } else {
      ERS_LOG( "Parallel checkConfig subtransition already performed." );
    }
  } else {
    try{
      checkConfig( cmd );
    }
    catch( ers::Issue &ex)
    {
      ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during checkConfig sub-transition", ex);
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }
    catch( std::exception &ex)
    {
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during checkConfig sub-transition", ex);
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }
    catch(...)
    {
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during checkConfig sub-transition");
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }
  }
}

void ReadoutModule_FTK::callCheckConnect( const daq::rc::SubTransitionCmd& cmd ) {
  if ( !m_initialized )
  { 
    ERS_LOG("Board not initialized! Transition " << cmd.toString() << " skipped..");
    return;
  }

  if ( m_configuration & READOUTMODULE_FTK_PARALLEL_CONNECT ) {
    if ( !(m_checkConnect_performed++) ) {
      ftkTimeLog tLog(ERS_HERE, name_ftk(), "Parallel checkConnect transition");
      {
      ThreadPool theTP( m_name, m_connectThreadLimit );
      for ( auto& rm : m_enabledReadoutModules ) {
        if ( rm.second->m_configuration & READOUTMODULE_FTK_PARALLEL_CONNECT ) {
          ERS_LOG( "Enqueuing checkConnect for ReadoutModule named " << rm.first );
          theTP.enqueue( [&]() { 
            try{
              rm.second->checkConnect( cmd ); 
            }
            catch( ers::Issue &ex)
            { 
              ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during checkConnect sub-transition", ex);
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }
            catch( std::exception &ex)
            {
              ftkException err(ERS_HERE, name_ftk(), "Caught a major standard c++ issue during checkConnect sub-transition", ex);
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }
            catch(...)
            {
              ftkException err(ERS_HERE, name_ftk(), "Caught a major standard c++ issue during checkConnect sub-transition");
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }
          } );
        }
      }
      ERS_LOG( "Finished launching parallel checkConnect." );
      }
      tLog.end(ERS_HERE); // Close ftkTimeLog
    } else {
      ERS_LOG( "Parallel checkConnect subtransition already performed." );
    }
  } else {
    try{
      checkConnect( cmd );
    }
    catch( ers::Issue &ex)
    {
      ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during checkConnect sub-transition", ex);
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }
    catch( std::exception &ex)
    {
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during checkConnect sub-transition", ex);
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }
    catch(...)
    {
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during checkConnect sub-transition");
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }
  }
}

void ReadoutModule_FTK::callStartNoDF( const daq::rc::SubTransitionCmd& cmd ) {
  if ( !m_initialized )
  { 
    ERS_LOG("Board not initialized! Transition " << cmd.toString() << " skipped..");
    return;
  }

  if ( m_configuration & READOUTMODULE_FTK_PARALLEL_PREPAREFORRUN ) {
    if ( !(m_startNoDF_performed++) ) {
      ftkTimeLog tLog(ERS_HERE, name_ftk(), "Parallel startNoDF transition");
      {
      ThreadPool theTP( m_name, m_prepareForRunThreadLimit );
      for ( auto& rm : m_enabledReadoutModules ) {
        if ( rm.second->m_configuration & READOUTMODULE_FTK_PARALLEL_PREPAREFORRUN ) {
          ERS_LOG( "Enqueuing startNoDF for ReadoutModule named " << rm.first );
          theTP.enqueue( [&]() { 
            try{
              rm.second->startNoDF( cmd ); 
            }
            catch( ers::Issue &ex)
            { 
              ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during startNoDF sub-transition", ex);
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }
            catch( std::exception &ex)
            {
              ftkException err(ERS_HERE, name_ftk(), "Caught a major standard c++ issue during startNoDF sub-transition", ex);
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }
            catch(...)
            {
              ftkException err(ERS_HERE, name_ftk(), "Caught a major standard c++ issue during startNoDF sub-transition");
              if(isFatalMode())
                throw(err);
              else
                ers::error(err);
            }          } );
        }
      }
      ERS_LOG( "Finished launching parallel startNoDF." );
      }
      tLog.end(ERS_HERE); // Close ftkTimeLog
    } else {
      ERS_LOG( "Parallel startNoDF subtransition already performed." );
    }
  } else {
    try{
      startNoDF( cmd );
    }
    catch( ers::Issue &ex)
    {
      ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during startNoDF sub-transition", ex);
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }
    catch( std::exception &ex)
    {
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during startNoDF sub-transition", ex);
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }
    catch(...)
    {
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during startNoDF sub-transition");
      if(isFatalMode())
        throw(err);
      else
        ers::error(err);
    }
  }
}


void ReadoutModule_FTK::publish() {
  if ( !m_initialized )
    return;

  if ( (!m_enablePublish) || m_interruptPublish ) return;

  ERS_LOG( "Received request for publish." );

  if ( m_configuration & READOUTMODULE_FTK_PARALLEL_MONITORING ) {
    if ( m_configuration & READOUTMODULE_FTK_SEPARATE_PUBLISH_THREADS ) {
      // separate publish and publishFullStats queues
      m_publishQueue++;
    } else {
      // same queue for both
      m_monitoringQueue.try_push( false );
    }
  } else {
    std::unique_lock<std::timed_mutex> lck( m_local_mtx, std::defer_lock );
    const uint32_t NMINUTES_TO_WAIT = 2;
    if ( attempt_lock_for_n_minutes( lck, NMINUTES_TO_WAIT ) && m_enablePublish && !m_interruptPublish ) {
      std::unique_lock<std::timed_mutex> lck2( m_global_monitoring_mtx, std::defer_lock );
      if ( ( !( m_configuration & READOUTMODULE_FTK_SERIALIZE_RCD_MONITORING )
                || attempt_lock_for_n_minutes( lck2, NMINUTES_TO_WAIT ) )
            && m_enablePublish && !m_interruptPublish ) {
        doPublish( nextPublishFlag(), false );
      }
    }
  }
}

void ReadoutModule_FTK::publishFullStats() {
  if ( !m_initialized )
  { 
    ERS_LOG("Board not initialized! publishFullStats skipped..");
    return;
  }

  if ( (!m_enablePublish) || m_interruptPublish ) return;

  ERS_LOG( "Received request for publishFullStats." );

  if ( m_configuration & READOUTMODULE_FTK_PARALLEL_MONITORING ) {
    if ( m_configuration & READOUTMODULE_FTK_SEPARATE_PUBLISH_THREADS ) {
      // separate publish and publishFullStats queues
      m_publishFullStatsQueue++;
    } else {
      // same queue for both
      m_monitoringQueue.try_push( true );
    }
  } else {
    std::unique_lock<std::timed_mutex> lck( m_local_mtx, std::defer_lock );
    const uint32_t NMINUTES_TO_WAIT = 2;
    if ( attempt_lock_for_n_minutes( lck, NMINUTES_TO_WAIT ) && m_enablePublish && !m_interruptPublish ) {
      std::unique_lock<std::timed_mutex> lck2( m_global_monitoring_mtx, std::defer_lock );
      if ( ( !( m_configuration & READOUTMODULE_FTK_SERIALIZE_RCD_MONITORING )
                || attempt_lock_for_n_minutes( lck2, NMINUTES_TO_WAIT ) )
            && m_enablePublish && !m_interruptPublish ) {
        doPublishFullStats( nextPublishFullStatsFlag() | READOUTMODULE_FTK_PUBLISHFULLSTATS_BIT );
      }
    }
  }
}

void ReadoutModule_FTK::subTransition( const daq::rc::SubTransitionCmd& cmd ) 
{
  ftkTimeLog tLog(ERS_HERE, name_ftk(), cmd.toString()); // Create log counter

  std::vector<std::string> args = cmd.arguments();
  if ( args.size() >= 2 ) {
    if ( args.at(0) == "SUB_TRANSITION" ) {
      ERS_LOG( "SUB_TRANSITION: " << args.at(1) );
      if ( args.at(1) == "checkConnect" ) {
        callCheckConnect( cmd  );
      }
			else if (args.at(1) == "startNoDF" ){
				callStartNoDF( cmd );
			}
			else if (args.at(1) == "checkConfig" ){
				callCheckConfig( cmd );
			}
    }
  }
  tLog.end(ERS_HERE);  // End log conter
}

void ReadoutModule_FTK::resetPublishQueues() {
  // these operations are not thread safe
  std::unique_lock<std::timed_mutex> lck( m_local_mtx, std::defer_lock );
  const uint32_t NMINUTES_TO_WAIT = 2;
  if ( attempt_lock_for_n_minutes( lck, NMINUTES_TO_WAIT ) ) {
    resetPublishFlag();
    resetPublishFullStatsFlag();
    m_monitoringQueue.clear();
    m_publishQueue = 0x0;
    m_publishFullStatsQueue = 0x0;
  } else {
    ers::warning( FTKLockFailed( ERS_HERE, m_name, "local", "resetPublishQueues", NMINUTES_TO_WAIT, "Cannot reset monitoring queues." ) );
  }
}

void ReadoutModule_FTK::user( const daq::rc::UserCmd& cmd ) {
  const std::string& cmdName = cmd.commandName();
  ERS_LOG( "Received user command \"" << cmdName << "\" while in state \"" << cmd.currentFSMState() << "\"" );

  if ( cmdName == "Recovery" ) {
    ERS_LOG( "Stopless recovery procedure." );
    std::vector<std::string> param = cmd.commandParameters();
    if ( param.size() < 1 ) return;
    if ( param[0] == "stopDC" ) {
      ERS_LOG( "stopDC" );
      SRstopDC();
    } else if ( param[0] == "stopROIB" ) {
      ERS_LOG( "stopROIB" );
      SRstopROIB();
    } else if ( param[0] == "stopHLT" ) {
      ERS_LOG( "stopHLT" );
      SRstopHLT();
    } else if ( param[0] == "stopRecording" ) {
      ERS_LOG( "stopRecording" );
      SRstopRecording();
    } else if ( param[0] == "stopGathering" ) {
      ERS_LOG( "stopGathering" );
      SRstopGathering();
    } else if ( param[0] == "stopArchiving" ) {
      ERS_LOG( "stopArchiving" );
      SRstopArchiving();
    } else if ( param[0] == "disconnect" ) {
      ERS_LOG( "disconnect" );
      SRdisconnect();
    } else if ( param[0] == "unconfigure" ) {
      ERS_LOG( "unconfigure" );
      SRunconfigure();
    } else if ( param[0] == "configure" ) {
      ERS_LOG( "configure" );
      SRconfigure();
    } else if ( param[0] == "connect" ) {
      ERS_LOG( "connect" );
      SRconnect();
    } else if ( param[0] == "checkConnect" ) {
      ERS_LOG( "checkConnect" );
      SRcheckConnect();
    } else if ( param[0] == "checkConfig" ) {
      ERS_LOG( "checkConfig" );
      SRcheckConfig();
		} else if ( param[0] == "startNoDF" ) {
			ERS_LOG( "startNoDF" );
      SRstartNoDF();
    }else if ( param[0] == "prepareForRun" ) {
      ERS_LOG( "prepareForRun" );
      SRprepareForRun();
    } else {
      ERS_LOG( "Unrecognized transition!" );
    }
  } else { // for unrecognized commands, forward them to the doUser function that subclasses may override
      ERS_LOG( "Forwarding command \"" << cmdName << "\" to the doUser function." );
      std::unique_lock<std::timed_mutex> lck ( m_local_mtx, std::defer_lock );
      const uint32_t NMINUTES_TO_WAIT = 1;
      if ( attempt_lock_for_n_minutes( lck, NMINUTES_TO_WAIT ) ) {
        doUser( cmd );
      } else {
        ers::error( FTKLockFailed( ERS_HERE, m_name, "global", "destructor", NMINUTES_TO_WAIT, "Could not execute the user command" ) );
      }
  }
}

void ReadoutModule_FTK::user_sender( const std::string receiver, const std::string cmd_name, const std::vector<std::string>& cmd_params )
{
  ERS_LOG( "Sending the command " << cmd_name );
  rc::UserCmd command( cmd_name, cmd_params );
  rc::UserBroadcastCmd usrbc(command);
  rc::CommandSender sendr( m_ipcpartition.name(), m_appName );
  sendr.makeTransition( receiver, usrbc );
  ERS_LOG( "usercommand " << cmd_name << " send to " << receiver );
}

void ReadoutModule_FTK::ACKsender( const std::string state )
{
  const std::string cmd_name = "ACK";
  std::vector<std::string> cmd_params;
  cmd_params.push_back( m_name );
  cmd_params.push_back( state );
  ERS_LOG( "Sending the ACK: Transition " << state << " completed!" );
  user_sender( m_manager, cmd_name, cmd_params );
}

void ReadoutModule_FTK::getManager()
{

  m_ipcpartition = daq::rc::OnlineServices::instance().getIPCPartition();
  m_appName      = daq::rc::OnlineServices::instance().applicationName();

	//Reading the Configuration..Trying to identify the Manager Application from the segment
  Configuration& confi=daq::rc::OnlineServices::instance().getConfiguration();
  const daq::core::Partition & partition = daq::rc::OnlineServices::instance().getPartition();
  const daq::core::Segment & segment = daq::rc::OnlineServices::instance().getSegment();
  std::vector<const daq::core::ResourceBase*> rb = segment.get_Resources();
  std::vector<const daq::core::Component*> disabledComponents = partition.get_Disabled();
  //Performing the check on all found applications what are the RM and what are the active ones
  for (std::vector<const daq::core::ResourceBase*>::iterator it = rb.begin(); it != rb.end(); it++)
  {
    const daq::df::RCD* rcdAppl = confi.cast<daq::df::RCD>(*it);
		if ( rcdAppl == 0 ) continue;
    std::vector<const daq::core::ResourceBase*> RMs = rcdAppl->get_Contains();
    for (std::vector<const daq::core::ResourceBase*>::iterator cit = RMs.begin(); cit != RMs.end(); cit++)
    {
      const daq::df::ReadoutModule* roModule = confi.cast<daq::df::ReadoutModule, daq::core::ResourceBase> (*cit);
      if (roModule==0) //check if the object is a RM
      {
        continue;
      }
			else if ((*cit)->disabled(partition)) //check if the RM is enabled
      {
        continue;
      } 
      else if (roModule->class_name()== "ReadoutModule_Manager")  //check if the RM is the manager Application
      {                                   
        ERS_LOG( "ReadoutModule " << roModule->UID()<< " is the Manager application!\n The Recovery ACKs will be sent to RCD " << rcdAppl->UID());
				m_manager=rcdAppl->UID();
        break;
      }
		}
	}
	if(m_manager == "")
	{
		std::stringstream message;
    message << "FTK WARNING: the RM " << m_name << "was not able to find the FTK Manager application. The Recovery procedure will fail!!";
    daq::is::Exception ex(ERS_HERE, message.str());
    ers::warning(ex);
	}
}

void ReadoutModule_FTK::SRstopDC()
{
  daq::rc::TransitionCmd cmd( daq::rc::FSM_COMMAND::STOPDC );

  if ( m_configuration & READOUTMODULE_FTK_PARALLEL_STOPDC ) {
    if ( !(m_stopDC_performed++) ) {
      ERS_LOG( "Launching parallel SRstopDC transition." );
      {
        ThreadPool theTP( m_name, m_stopThreadLimit );
        for ( auto& rm : m_enabledReadoutModules ) {
          if ( rm.second->m_configuration & READOUTMODULE_FTK_PARALLEL_STOPDC ) {
            ERS_LOG( "Enqueuing SRstopDC for ReadoutModule named " << rm.first );
            theTP.enqueue( [&]() { 
              try{
                rm.second->doSRStopDC( cmd ); 
              }
              catch( ers::Issue &ex)
              {
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during SRStopDC transition", ex);
                ers::error(err);
              }
              catch( std::exception &ex)
              {
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue", ex);
                ers::error(err);
              }
              catch(...)
              {
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue");
                ers::error(err);
              }
            } );
          }
        }
      }
      ERS_LOG( "Parallel SRstopDC transition is complete." );
    } else {
      ERS_LOG( "Parallel SRstopDC transition already performed." );
    }
  } else {
    try{
      doSRStopDC( cmd );
    }
    catch( ers::Issue &ex)
    {
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during SRstopDC transition", ex);
      ers::error(err);
    }
    catch( std::exception &ex)
    {
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during recovery transition", ex);
      ers::error(err);
    }
    catch(...)
    {
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during recovery transition");
      ers::error(err);
    }
  }
  // now have to send ACK
  ACKsender( "stopDC" );
}

void ReadoutModule_FTK::SRstopROIB()
{
  rc::TransitionCmd cmd( daq::rc::FSM_COMMAND::STOPROIB );

  if ( m_configuration & READOUTMODULE_FTK_PARALLEL_STOPROIB ) {
    if ( !(m_stopROIB_performed++) ) {
      ERS_LOG( "Launching parallel SRstopROIB transition." );
      {
        ThreadPool theTP( m_name, m_stopThreadLimit );
        for ( auto& rm : m_enabledReadoutModules ) {
          if ( rm.second->m_configuration & READOUTMODULE_FTK_PARALLEL_STOPROIB ) {
            ERS_LOG( "Enqueuing SRstopROIB for ReadoutModule named " << rm.first );
            theTP.enqueue( [&]() { 
              try{
                rm.second->SRstopROIB_atomic( cmd ); 
              } 
              catch( ers::Issue &ex)
              {
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during SRStopROIB transition", ex);
                ers::error(err);
              }
              catch( std::exception &ex)
              {
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue", ex);
                ers::error(err);
              }
              catch(...)
              {
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue");
                ers::error(err);
              }
            } );
          }
        }
      }
      ERS_LOG( "Parallel SRstopROIB transition is complete." );
    } else {
      ERS_LOG( "Parallel SRstopROIB transition already performed." );
    }
  } else {
    try{
      SRstopROIB_atomic( cmd );
    }
    catch( ers::Issue &ex)
    {
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during SRstopROIB transition", ex);
      ers::error(err);
    }
    catch( std::exception &ex)
    {
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during recovery transition", ex);
      ers::error(err);
    }
    catch(...)
    {
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during recovery transition");
      ers::error(err);
    }
  }
  // now have to send ACK
  ACKsender( "stopROIB" );
}

void ReadoutModule_FTK::SRstopHLT()
{
  daq::rc::TransitionCmd cmd( daq::rc::FSM_COMMAND::STOPHLT );

  if ( m_configuration & READOUTMODULE_FTK_PARALLEL_STOPHLT ) {
    if ( !(m_stopHLT_performed++) ) {
      ERS_LOG( "Launching parallel SRstopHLT transition." );
      {
        ThreadPool theTP( m_name, m_stopThreadLimit );
        for ( auto& rm : m_enabledReadoutModules ) {
          if ( rm.second->m_configuration & READOUTMODULE_FTK_PARALLEL_STOPHLT ) {
            ERS_LOG( "Enqueuing SRstopHLT for ReadoutModule named " << rm.first );
            theTP.enqueue( [&]() { 
              try{
                rm.second->doSRStopHLT( cmd ); 
              } 
              catch( ers::Issue &ex)
              {
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during SRStopHLT transition", ex);
                ers::error(err);
              }
              catch( std::exception &ex)
              {
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue", ex);
                ers::error(err);
              }
              catch(...)
              {
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue");
                ers::error(err);
              }            
            } );
          }
        }
      }
      ERS_LOG( "Parallel SRstopHLT transition is complete." );
    } else {
      ERS_LOG( "Parallel SRstopHLT transition already performed." );
    }
  } else {
    try{
      doSRStopHLT( cmd );
    }
    catch( ers::Issue &ex)
    {
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during SRstopHLT transition", ex);
      ers::error(err);
    }
    catch( std::exception &ex)
    {
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during recovery transition", ex);
      ers::error(err);
    }
    catch(...)
    {
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during recovery transition");
      ers::error(err);
    }
  }
  // now have to send ACK
  ACKsender( "stopHLT" );
}

void ReadoutModule_FTK::SRstopRecording()
{
  rc::TransitionCmd cmd( daq::rc::FSM_COMMAND::STOPRECORDING );

  if ( m_configuration & READOUTMODULE_FTK_PARALLEL_STOPRECORDING ) {
    if ( !(m_stopRecording_performed++) ) {
      ERS_LOG( "Launching parallel SRstopRecording transition." );
      {
        ThreadPool theTP( m_name, m_stopThreadLimit );
        for ( auto& rm : m_enabledReadoutModules ) {
          if ( rm.second->m_configuration & READOUTMODULE_FTK_PARALLEL_STOPRECORDING ) {
            ERS_LOG( "Enqueuing SRstopRecording for ReadoutModule named " << rm.first );
            theTP.enqueue( [&]() { 
              try{
                rm.second->doSRStopRecording( cmd );
              } 
              catch( ers::Issue &ex)
              {
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during SRStopRecording transition", ex);
                ers::error(err);
              }
              catch( std::exception &ex)
              {
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue", ex);
                ers::error(err);
              }
              catch(...)
              {
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue");
                ers::error(err);
              }
            });
          }
        }
      }
      ERS_LOG( "Parallel SRstopRecording transition is complete." );
    } else {
      ERS_LOG( "Parallel SRstopRecording transition already performed." );
    }
  } else {
    try{
      doSRStopRecording( cmd );
    }
    catch( ers::Issue &ex)
    {
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during SRstopRecording transition", ex);
      ers::error(err);
    }
    catch( std::exception &ex)
    {
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during recovery transition", ex);
      ers::error(err);
    }
    catch(...)
    {
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during recovery transition");
      ers::error(err);
    }
  }
  // now have to send ACK
  ACKsender( "stopRecording" );
}

void ReadoutModule_FTK::SRstopGathering()
{
  daq::rc::TransitionCmd cmd( daq::rc::FSM_COMMAND::STOPGATHERING );

  if ( m_configuration & READOUTMODULE_FTK_PARALLEL_STOPGATHERING ) {
    if ( !(m_stopGathering_performed++) ) {
      ERS_LOG( "Launching parallel SRstopGathering transition." );
      {
        ThreadPool theTP( m_name, m_stopThreadLimit );
        for ( auto& rm : m_enabledReadoutModules ) {
          if ( rm.second->m_configuration & READOUTMODULE_FTK_PARALLEL_STOPGATHERING ) {
            ERS_LOG( "Enqueuing SRstopGathering for ReadoutModule named " << rm.first );
            theTP.enqueue( [&]() { 
              try{
                rm.second->doSRStopGathering( cmd ); 
              } 
              catch( ers::Issue &ex)
              {
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during SRStopGathering transition", ex);
                ers::error(err);
              } 
              catch( std::exception &ex)
              {
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue", ex);
                ers::error(err);
              }
              catch(...)
              {
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue");
                ers::error(err);
              }
            } );
          }
        }
      }
      ERS_LOG( "Parallel SRstopGathering transition is complete." );
    } else {
      ERS_LOG( "Parallel SRstopGathering transition already performed." );
    }
  } else {
    try{
      doSRStopGathering( cmd );
    }
    catch( ers::Issue &ex)
    {
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during SRstopGathering transition", ex);
      ers::error(err);
    } 
    catch( std::exception &ex)
    {
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during recovery transition", ex);
      ers::error(err);
    }
    catch(...)
    {
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during recovery transition");
      ers::error(err);
    }
  }
  // now have to send ACK
  ACKsender( "stopGathering" );
}

void ReadoutModule_FTK::SRstopArchiving()
{
  rc::TransitionCmd cmd( daq::rc::FSM_COMMAND::STOPARCHIVING );

  m_prepareForRun_performed = 0;

  if ( m_configuration & READOUTMODULE_FTK_PARALLEL_STOPARCHIVING ) {
    if ( !(m_stopArchiving_performed++) ) {
      ERS_LOG( "Launching parallel SRstopArchiving transition." );
      {
        ThreadPool theTP( m_name, m_stopThreadLimit );
        for ( auto& rm : m_enabledReadoutModules ) {
          if ( rm.second->m_configuration & READOUTMODULE_FTK_PARALLEL_STOPARCHIVING ) {
            ERS_LOG( "Enqueuing SRstopArchiving for ReadoutModule named " << rm.first );
            theTP.enqueue( [&]() { 
              try{
                rm.second->doSRStopArchiving( cmd ); 
              } 
              catch( ers::Issue &ex)
              {
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during SRStopArchiving transition", ex);
                ers::error(err);
              } 
              catch( std::exception &ex)
              {
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue", ex);
                ers::error(err);
              }
              catch(...)
              {
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue");
                ers::error(err);
              }
            } );
          }
        }
      }
      ERS_LOG( "Parallel SRstopArchiving transition is complete." );
    } else {
      ERS_LOG( "Parallel SRstopArchiving transition already performed." );
    }
  } else {
    try{
      doSRStopArchiving( cmd );
    }
    catch( ers::Issue &ex)
    {
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during SRstopArchiving transition", ex);
      ers::error(err);
    }
    catch( std::exception &ex)
    {
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during recovery transition", ex);
      ers::error(err);
    }
    catch(...)
    {
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during recovery transition");
      ers::error(err);
    } 
  }
  // now have to send ACK
  ACKsender( "stopArchiving" );
}

void ReadoutModule_FTK::SRdisconnect()
{
  daq::rc::TransitionCmd cmd( daq::rc::FSM_COMMAND::DISCONNECT );

  m_connect_performed = 0;

  if ( m_configuration & READOUTMODULE_FTK_PARALLEL_DISCONNECT ) {
    if ( !(m_disconnect_performed++) ) {
      ERS_LOG( "Launching parallel SRdisconnect transition." );
      {
        ThreadPool theTP( m_name, m_disconnectThreadLimit );
        for ( auto& rm : m_enabledReadoutModules ) {
          if ( rm.second->m_configuration & READOUTMODULE_FTK_PARALLEL_DISCONNECT ) {
            ERS_LOG( "Enqueuing SRdisconnect for ReadoutModule named " << rm.first );
            theTP.enqueue( [&]() { 
              try{
                rm.second->doSRDisconnect( cmd ); 
              } 
              catch( ers::Issue &ex)
              {
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during SRDisconnect transition", ex);
                ers::error(err);
              }
              catch( std::exception &ex)
              {
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue", ex);
                ers::error(err);
              }
              catch(...)
              {
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue");
                ers::error(err);
              }
            } );
          }
        }
      }
      ERS_LOG( "Parallel SRdisconnect transition is complete." );
    } else {
      ERS_LOG( "Parallel SRdisconnect transition already performed." );
    }
  } else {
    try{
      doSRDisconnect( cmd );
    }
    catch( ers::Issue &ex)
    {
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during SRDisconnect transition", ex);
      ers::error(err);
    } 
    catch( std::exception &ex)
    {
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during recovery transition", ex);
      ers::error(err);
    }
    catch(...)
    {
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during recovery transition");
      ers::error(err);
    }
  }
  // now have to send ACK
  ACKsender( "disconnect" );
}

void ReadoutModule_FTK::SRunconfigure()
{
  daq::rc::TransitionCmd cmd( daq::rc::FSM_COMMAND::UNCONFIGURE );

  m_configure_performed = 0;

  if ( m_configuration & READOUTMODULE_FTK_PARALLEL_UNCONFIGURE ) {
    if ( !(m_unconfigure_performed++) ) {
      ERS_LOG( "Launching parallel SRunconfigure transition." );
      {
        ThreadPool theTP( m_name, m_unconfigureThreadLimit );
        for ( auto& rm : m_enabledReadoutModules ) {
          if ( rm.second->m_configuration & READOUTMODULE_FTK_PARALLEL_UNCONFIGURE ) {
            ERS_LOG( "Enqueuing SRunconfigure for ReadoutModule named " << rm.first );
            theTP.enqueue( [&]() { 
              try{
                rm.second->doSRUnconfigure( cmd ); 
              } 
              catch( ers::Issue &ex)
              {
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during SRUnconfigure transition", ex);
                ers::error(err);
              }
              catch( std::exception &ex)
              {
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue", ex);
                ers::error(err);
              }
              catch(...)
              {
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue");
                ers::error(err);
              }
            } );
          }
        }
      }
      ERS_LOG( "Parallel SRunconfigure transition is complete." );
    } else {
      ERS_LOG( "Parallel SRunconfigure transition already performed." );
    }
  } else {
    try{
      doSRUnconfigure( cmd );
    }
    catch( ers::Issue &ex)
    {
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during SRUnconfigure transition", ex);
      ers::error(err);
    }
    catch( std::exception &ex)
    {
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during recovery transition", ex);
      ers::error(err);
    }
    catch(...)
    {
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during recovery transition");
      ers::error(err);
    }
  }
  // now have to send ACK
  ACKsender( "unconfigure" );
}

void ReadoutModule_FTK::SRconfigure()
{
  daq::rc::TransitionCmd cmd( daq::rc::FSM_COMMAND::CONFIGURE );

  m_unconfigure_performed = 0;

  if ( m_configuration & READOUTMODULE_FTK_PARALLEL_CONFIGURE ) {
    if ( !(m_configure_performed++) ) {
      ERS_LOG( "Launching parallel SRconfigure transition." );
      {
        ThreadPool theTP( m_name, m_configureThreadLimit );
        for ( auto& rm : m_enabledReadoutModules ) {
          if ( rm.second->m_configuration & READOUTMODULE_FTK_PARALLEL_CONFIGURE ) {
            ERS_LOG( "Enqueuing SRconfigure for ReadoutModule named " << rm.first );
            theTP.enqueue( [&]() { 
              try{
                rm.second->doSRConfigure( cmd ); 
              } 
              catch( ers::Issue &ex)
              {
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during SRConfigure transition", ex);
                ers::error(err);
              }
              catch( std::exception &ex)
              {
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue", ex);
                ers::error(err);
              }
              catch(...)
              {
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue");
                ers::error(err);
              }
            } );
          }
        }
      }
      ERS_LOG( "Parallel SRconfigure transition is complete." );
    } else {
      ERS_LOG( "Parallel SRconfigure transition already performed." );
    }
  } else {
    try{
      doSRConfigure( cmd );
    }
    catch( ers::Issue &ex)
    {
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during SRConfigure transition", ex);
      ers::error(err);
    }
    catch( std::exception &ex)
    {
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during recovery transition", ex);
      ers::error(err);
    }
    catch(...)
    {
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during recovery transition");
      ers::error(err);
    }
  }
  // now have to send ACK
  ACKsender( "configure" );
}

void ReadoutModule_FTK::SRconnect()
{
  daq::rc::TransitionCmd cmd( daq::rc::FSM_COMMAND::CONNECT );

  m_disconnect_performed = 0;
  m_checkConnect_performed = 0;

  if ( m_configuration & READOUTMODULE_FTK_PARALLEL_CONNECT ) {
    if ( !(m_connect_performed++) ) {
      ERS_LOG( "Launching parallel SRconnect transition." );
      {
        ThreadPool theTP( m_name, m_connectThreadLimit );
        for ( auto& rm : m_enabledReadoutModules ) {
          if ( rm.second->m_configuration & READOUTMODULE_FTK_PARALLEL_CONNECT ) {
            ERS_LOG( "Enqueuing SRconnect for ReadoutModule named " << rm.first );
            theTP.enqueue( [&]() { 
              try{
                rm.second->doSRConnect( cmd ); 
              } 
              catch( ers::Issue &ex)
              {
                ACKsender( "ERROR" ); 
                ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during SRConnect transition", ex);
                ers::error(err);
              }
              catch( std::exception &ex)
              {
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue", ex);
                ers::error(err);
              }
              catch(...)
              {
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue");
                ers::error(err);
              }
            } );
          }
        }
      }
      ERS_LOG( "Parallel SRconnect transition is complete." );
    } else {
      ERS_LOG( "Parallel SRconnect transition already performed." );
    }
  } else {
    try{
      doSRConnect( cmd );
    }
    catch( ers::Issue &ex)
    {
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during SRConnect transition", ex);
      ers::error(err);
    }
    catch( std::exception &ex)
    {
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during recovery transition", ex);
      ers::error(err);
    }
    catch(...)
    {
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during recovery transition");
      ers::error(err);
    }
  }
  // now have to send ACK
  ACKsender( "connect" );
}

void ReadoutModule_FTK::SRprepareForRun()
{
  daq::rc::TransitionCmd cmd( daq::rc::FSM_COMMAND::START );

  m_stopROIB_performed = 0;
  m_stopDC_performed = 0;
  m_stopHLT_performed = 0;
  m_stopRecording_performed = 0;
  m_stopGathering_performed = 0;
  m_stopArchiving_performed = 0;

  if ( m_configuration & READOUTMODULE_FTK_PARALLEL_PREPAREFORRUN ) {
    if ( !(m_prepareForRun_performed++) ) {
      ERS_LOG( "Launching parallel SRprepareForRun transition." );
      {
        ThreadPool theTP( m_name, m_prepareForRunThreadLimit );
        for ( auto& rm : m_enabledReadoutModules ) {
          if ( rm.second->m_configuration & READOUTMODULE_FTK_PARALLEL_PREPAREFORRUN ) {
            ERS_LOG( "Enqueuing SRprepareForRun for ReadoutModule named " << rm.first );
            theTP.enqueue( [&]() { 
              try{
                rm.second->SRprepareForRun_atomic( cmd ); 
              } 
              catch( ers::Issue &ex)
              { 
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during SRprepareForRun transition", ex);
                ers::error(err);
              }
              catch( std::exception &ex)
              {
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue", ex);
                ers::error(err);
              }
              catch(...)
              {
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue");
                ers::error(err);
              }
            } );
          }
        }
      }
      ERS_LOG( "Parallel SRprepareForRun transition is complete." );
    } else {
      ERS_LOG( "Parallel SRprepareForRun transition already performed." );
    }
  } else {
    try{
      SRprepareForRun_atomic( cmd );
    }
    catch( ers::Issue &ex)
    { 
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during SRprepareForRun transition", ex);
      ers::error(err);
    }
    catch( std::exception &ex)
    {
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during recovery transition", ex);
      ers::error(err);
    }
    catch(...)
    {
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during recovery transition");
      ers::error(err);
    }
  }
  // now have to send ACK
  ACKsender( "prepareForRun" );
}

void ReadoutModule_FTK::SRcheckConfig()
{
  daq::rc::SubTransitionCmd cmd( "checkConfig", daq::rc::FSM_COMMAND::CONFIGURE );

  if ( m_configuration & READOUTMODULE_FTK_PARALLEL_CONFIGURE ) {
    if ( !(m_checkConfig_performed++) ) {
      ERS_LOG( "Launching parallel SRcheckConfig transition." );
      {
        ThreadPool theTP( m_name, m_connectThreadLimit );
        for ( auto& rm : m_enabledReadoutModules ) {
          if ( rm.second->m_configuration & READOUTMODULE_FTK_PARALLEL_CONFIGURE ) {
            ERS_LOG( "Enqueuing SRcheckConnect for ReadoutModule named " << rm.first );
            theTP.enqueue( [&]() { 
              try{
                rm.second->checkSRConfig( cmd ); 
              } 
              catch( ers::Issue &ex)
              { 
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during SRcheckConfig transition", ex);
                ers::error(err);
              }
              catch( std::exception &ex)
              {
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue", ex);
                ers::error(err);
              }
              catch(...)
              {
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue");
                ers::error(err);
              }
            } );
          }
        }
      }
      ERS_LOG( "Parallel SRcheckConfig transition is complete." );
    } else {
      ERS_LOG( "Parallel SRcheckConfig transition already performed." );
    }
  } else {
    try{
      checkSRConnect( cmd );
    }
    catch( ers::Issue &ex)
    { 
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during checkSRConnect transition", ex);
      ers::error(err);
    }
    catch( std::exception &ex)
    {
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during recovery transition", ex);
      ers::error(err);
    }
    catch(...)
    {
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during recovery transition");
      ers::error(err);
    }
  }
  // now have to send ACK
  ACKsender( "checkConfig" );
}

void ReadoutModule_FTK::SRcheckConnect()
{
  daq::rc::SubTransitionCmd cmd( "checkConnect", daq::rc::FSM_COMMAND::CONNECT );

  if ( m_configuration & READOUTMODULE_FTK_PARALLEL_CONNECT ) {
    if ( !(m_checkConnect_performed++) ) {
      ERS_LOG( "Launching parallel SRcheckConnect transition." );
      {
        ThreadPool theTP( m_name, m_connectThreadLimit );
        for ( auto& rm : m_enabledReadoutModules ) {
          if ( rm.second->m_configuration & READOUTMODULE_FTK_PARALLEL_CONNECT ) {
            ERS_LOG( "Enqueuing SRcheckConnect for ReadoutModule named " << rm.first );
            theTP.enqueue( [&]() { 
              try{
                rm.second->checkSRConnect( cmd ); 
              } 
              catch( ers::Issue &ex)
              { 
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during checkSRConnect transition", ex);
                ers::error(err);
              }
              catch( std::exception &ex)
              {
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue", ex);
                ers::error(err);
              }
              catch(...)
              {
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue");
                ers::error(err);
              }
            } );
          }
        }
      }
      ERS_LOG( "Parallel SRcheckConnect transition is complete." );
    } else {
      ERS_LOG( "Parallel SRcheckConnect transition already performed." );
    }
  } else {
    try{
      checkSRConnect( cmd );
    }
    catch( ers::Issue &ex)
    { 
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during checkSRConnect transition", ex);
      ers::error(err);
    }
    catch( std::exception &ex)
    {
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during recovery transition", ex);
      ers::error(err);
    }
    catch(...)
    {
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during recovery transition");
      ers::error(err);
    }
  }
  // now have to send ACK
  ACKsender( "checkConnect" );
}

void ReadoutModule_FTK::SRstartNoDF()
{
  daq::rc::SubTransitionCmd cmd( "startNoDF", daq::rc::FSM_COMMAND::START );

  if ( m_configuration & READOUTMODULE_FTK_PARALLEL_PREPAREFORRUN ) {
    if ( !(m_startNoDF_performed++) ) {
      ERS_LOG( "Launching parallel SRstartNoDF transition." );
      {
        ThreadPool theTP( m_name, m_prepareForRunThreadLimit );
        for ( auto& rm : m_enabledReadoutModules ) {
          if ( rm.second->m_configuration & READOUTMODULE_FTK_PARALLEL_PREPAREFORRUN ) {
            ERS_LOG( "Enqueuing SRstartNoDF for ReadoutModule named " << rm.first );
            theTP.enqueue( [&]() { 
              try{
                rm.second->startSRNoDF( cmd ); 
              } 
              catch( ers::Issue &ex)
              { 
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during startSRNoDF transition", ex);
                ers::error(err);
              }
              catch( std::exception &ex)
              {
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue", ex);
                ers::error(err);
              }
              catch(...)
              {
                ACKsender( "ERROR" );
                ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue");
                ers::error(err);
              }
            } );
          }
        }
      }
      ERS_LOG( "Parallel SRcheckConnect transition is complete." );
    } else {
      ERS_LOG( "Parallel SRcheckConnect transition already performed." );
    }
  } else {
    try{ 
      startSRNoDF( cmd );
    } 
    catch( ers::Issue &ex)
    { 
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a major issue during startSRNoDF transition", ex);
      ers::error(err);
    }
    catch( std::exception &ex)
    {
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during recovery transition", ex);
      ers::error(err);
    }
    catch(...)
    {
      ACKsender( "ERROR" );
      ftkException err(ERS_HERE, name_ftk(), "Caught a standard c++ issue during recovery transition");
      ers::error(err);
    }  
  }
  // now have to send ACK
  ACKsender( "startNoDF" );
}


bool daq::ftk::attempt_lock_for_n_minutes( std::unique_lock<std::timed_mutex>& lck, const uint32_t nMinutesToWait ) {
  uint32_t nMinutesWaited = 0;
  bool gotlock = false;

  while ( !( gotlock = lck.try_lock_for(std::chrono::seconds(60)) ) ) {
    ++nMinutesWaited;
    if ( nMinutesWaited >= nMinutesToWait ) {
      break;
    }
  }

  return gotlock;
}

