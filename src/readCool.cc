#include <sstream>
#include <ipc/partition.h>
#include <is/infoiterator.h>
#include <boost/filesystem.hpp>
#include <is/infodictionary.h>
#include <cmdl/cmdargs.h>
#include <rc/RunParams.h>
#include "ftkcommon/FtkCool.hh"
#include <variant.hpp>

int main(int argc,char *argv[])
{
 
  CmdArgStr   partition_name ('p', "partition", "ATLAS", "partition to work in");
  CmdArgInt   runNumber('n',"runNumber","run-number","run number");
  CmdArgInt   region('r',"region","region-number","region number");
  CmdArgStr   dbConn('d',"dbConn","database-connection","database connection string",CmdArg::isVALREQ );
  CmdArgStr   dbTag('t',"dbTag","database-tag","database tag",CmdArg::isVALREQ);
  try {
    IPCCore::init( argc, argv );
  }
  catch( daq::ipc::Exception & ex ) {
    ers::fatal( ex );
    return 1;
  }
  runNumber=-1;
  region=-1;
  unsigned int runnumber;
  CmdLine  cmd(*argv, &partition_name,&runNumber,&dbConn,&dbTag,&region,NULL);
  CmdArgvIter  arg_iter(--argc, ++argv);
  cmd.set(CmdLine::NO_ABORT|CmdLine::QUIET);
  cmd.parse(arg_iter);
  if(runNumber==-1) {
    try {
      IPCPartition   p( (const char*)partition_name );
      ISInfoDictionary isInfoDict=ISInfoDictionary(p);
      RunParams runParams; 
      isInfoDict.findValue("RunParams.RunParams",runParams);
      runnumber=runParams.run_number+1;
    } catch(...) {
      std::cerr << "Failed to connect to partition: " << partition_name << std::endl;
      return -1;
    }
  } else
    runnumber=runNumber;
  storage::variant bank8;
  storage::variant bank12;
  storage::variant conn;
  storage::variant bank;
  if(runnumber<0) {std::cerr << "Run number not set\n"; return -1;}
  if(region<0 || region>=64) {std::cerr << "Region not valid\n"; return -1;}
  FtkCool cool((const char*)dbConn,(const char *)dbTag,runnumber);
  try {    
  //   cool.read(bank8,FtkCool::FtkConfigType::CORRGEN_RAW_8L_GCON,region);
  //  cool.read(bank12,FtkCool::FtkConfigType::CORRGEN_RAW_12L_GCON,region);
     cool.read(conn,FtkCool::FtkConfigType::SECTORS_RAW_8L_CONN,region);
  //  cool.read(bank,FtkCool::FtkConfigType::PATTERNBANK_8M_8L,region);
  } catch(...) {
    std::cerr << "Cool read failed\n"; return -1;
  }
  
  storage::variant t=conn["conn"];
  for(auto vv:t) {
    const std::vector<uint16_t> &u=vv;
    for(auto const &vvv:u) std::cout << int(vvv) << " ";
    std::cout <<std::endl;
  }

  return 0;
}

