#ifndef FTK_SECTOR_MAP_HPP
#define FTK_SECTOR_MAP_HPP

#include <string>
#include <vector>
#include <string>
class FtkSectorMap {
 public:
  FtkSectorMap(unsigned extralayers, const storage::variant &v):m_nextralayers(extralayers)  {
    const storage::variant vvmap=v["conn"];
    unsigned nsectors=vvmap.size();
    for (int isec=0;isec!=nsectors;++isec) { // sectors loop
      unsigned main_secid;
      unsigned nsimilar;
      const std::vector<uint16_t> &vv=vvmap[isec];
      
      size_t index=0;
      main_secid=vv[index++];
      nsimilar=vv[index++];
      m_nrelations.push_back(nsimilar);
      std::vector<unsigned> v_subID;
      std::vector<unsigned> v_secID;
      std::vector<std::vector<uint32_t>> v_stereoIDs;
      
      for (unsigned isim=0;isim!=nsimilar;++isim) { // loop over similar
	unsigned subid;
	unsigned secid;
	subid=vv[index++];
	secid=vv[index++];
	// set the relation
	v_subID.push_back(subid);
	v_secID.push_back(secid);
	// store the ID of the stereo modules
	std::vector<unsigned> v_extralayers;
	for (unsigned ip=0;ip!=m_nextralayers;++ip) {
        unsigned val=vv[index++];
        v_extralayers.push_back(val);
	}
	v_stereoIDs.push_back( v_extralayers);
      } // end loop over similar
      m_stereoIDs.push_back(v_stereoIDs);
      m_subID.push_back(v_subID);
      m_secID.push_back(v_secID);
    } // end sectors loop  
    m_nsectors=nsectors;    
  }
  
  const unsigned getNSectors() const { return m_nsectors; }
  const unsigned getNExtraLayers() const { return m_nextralayers; }
  
  const unsigned getNSimilarSectors(const unsigned secid) const
  { return m_nrelations[secid]; }
  const unsigned getSimilarSubID(const unsigned secid, const unsigned irel) const 
    { return m_subID[secid][irel]; }
  const unsigned getSimilarSecID(const unsigned secid, const unsigned irel) const 
    { return m_secID[secid][irel]; }
  const std::vector<unsigned>& getSimilarStereoIDs(const unsigned secid, const unsigned irel) const 
    { return m_stereoIDs[secid][irel]; }

 private:

  unsigned m_nsectors; // number of 7L sectors
  unsigned m_nextralayers; // number of extra layers described in the table

  std::vector<unsigned> m_nrelations;
  std::vector<std::vector<unsigned>> m_subID;
  std::vector<std::vector<unsigned>> m_secID;
  std::vector<std::vector<std::vector<uint32_t>>> m_stereoIDs; 


};
#endif
