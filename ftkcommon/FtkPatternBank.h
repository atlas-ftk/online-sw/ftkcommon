#ifndef _FTk_DB_PATTERNBANK_LIB_H_
#define _FTK_DB_PATTERNBANK_LIB_H_

#include <fstream>
#include <iomanip>
#include <string>
#include <vector>
#include <variant.hpp>


/** \brief The FTKPatternBank class represents a pattern bank for the AM board.
 */
template <unsigned nlayers=8,unsigned npart=32,unsigned npatt=8*1024*1024> class FtkPatternBankImpl {
 private:
 //useful short cuts
 constexpr static unsigned np=npatt/npart;
 
 const storage::variant *m_part[npart]; // pointer to subsegmets
 const std::vector<uint32_t> *m_dcmask[npart];
 const std::vector<uint32_t> *m_hbmask[npart];
 const std::vector<int16_t> *m_ssid[npart];
 const std::vector<int16_t> *m_sectorsID[npart];
 /** array with the number of DC bits in the pattern bank,
  * the size is set when the number of layers is set, default 0 everywhere */
 uint32_t m_bankdcbits[nlayers];
 /** array with the number of bits in the chip, the size is set
  * when the number of layers is set, default is 2 everywhere */
 uint32_t m_chipdcbits[nlayers];
 /** it is possible to assign an offset to all the SS to prevent the SS=0 in the pattern */
 uint32_t m_ssoffset[nlayers];

  /** check variable that records the maximum SS */
  unsigned int m_maxSS;

 

  /** internal inline method to return the position of the SS of a given pattern in
   * a speicific layer
   */


 
 void getPattern(unsigned ipatt,uint32_t (&ssid)[nlayers],uint32_t &sectorID) {
   uint16_t shift=0;
   const unsigned bank=ipatt/np;
   const unsigned offset=ipatt%np;
   const uint32_t dcmask=m_dcmask[bank]->operator[](offset);
   const uint32_t hbmask=m_hbmask[bank]->operator[](offset);
   sectorID=m_sectorsID[bank]->operator[](offset);   
   for (unsigned il=0; il<nlayers; ++il) {
     const uint32_t patt=m_ssid[bank]->operator[](offset*nlayers+il);
     uint32_t curss = patt<<m_bankdcbits[il]*2;
     uint32_t mask = (1<<m_bankdcbits[il])-1;
     uint32_t hb = (hbmask>>shift) & mask;
     uint32_t dc = (dcmask>>shift) & mask;
     for (unsigned ibit=0; ibit<m_bankdcbits[il]; ++ibit) { 
       uint32_t val=0;
       if (dc&(1<<ibit)) val = 0;
       else val = (hb&(1<<ibit)) ? 2 : 1; 
       curss |= val << (ibit*2);
       ssid[il]=curss;
     } 
     shift += m_bankdcbits[il];
   }
 }
 public: 

 void dumpPatternBank(const std::string &path) {
   std::ofstream outfile(path);
   
   uint32_t curpatt[nlayers];
   uint32_t sectorID;
   for (unsigned ipatt=0; ipatt<npatt; ++ipatt) { 
     getPattern(ipatt,curpatt,sectorID);
     outfile << std::hex << std::setw(5) << ipatt ;
     for (unsigned ilayer=0; ilayer<nlayers; ++ilayer) {
       outfile << "\t" << std::setw(5) << curpatt[ilayer];
     } 
     outfile << std::dec << std::endl;	
   } 
   outfile.close();
}


 FtkPatternBankImpl(const storage::variant &v) {
   for(unsigned i=0;i<npart;i++) {
     m_part[i]=&v[i];
     m_dcmask[i]=&(const std::vector<uint32_t>&) v[i]["dcmask"];
     m_hbmask[i]=&(const std::vector<uint32_t>&) v[i]["hbmask"];
     m_ssid[i]=&(const std::vector<int16_t>&)  v[i]["ssid"];
     m_sectorsID[i]=&(const std::vector<int16_t>&)  v[i]["sectorID"];
   }
   // init ssmap
   const std::vector<uint32_t> &ndcx=v[0]["ndcx"];
   const std::vector<uint32_t> &ndcy=v[0]["ndcy"];
   for(unsigned i=0;i<nlayers;i++) {
     m_bankdcbits[i] = ndcx[i]+ndcy[i];
     m_chipdcbits[i]=m_bankdcbits[i];
     m_ssoffset[i]=0;
   }
   

 }

 void printConfiguration() const {};

 unsigned int getNLayers() const { return nlayers; }
 //  const unsigned& getDCConfig(unsigned int layer) const { return m_bankdcbits[layer]; }

  /** return the number of pattern that exist in the bank */
 unsigned int getNPatterns() const { return npatt; }


 uint32_t calculateChecksum() {
   uint32_t checksum=1;
   const uint32_t ADLER_NUM = 65521;

   uint32_t sum1 = checksum & 0xffff; // this represents the LSB
   uint32_t sum2 = checksum>>16; // represents the MSB
   for (unsigned ipatt=0; ipatt<npatt; ++ipatt) { 
     uint32_t curpatt[nlayers];
     uint32_t sectorID;
     getPattern(ipatt,curpatt,sectorID);
     for (unsigned int i=0; i<nlayers; i++) {
       const uint32_t &word = curpatt[i];
       sum1 = ((sum1 + word) % ADLER_NUM);
       sum2 = ((sum2 + sum1) & ADLER_NUM);
     }
   }
   checksum = (sum2 << 16) | sum1;
   return checksum;
 }

 //  int getPattern(unsigned int, std::vector<uint32_t> &) const;
 //    int getPatternAndDC(unsigned int, std::vector<uint32_t>&,std::vector<uint32_t> &) const;
 // int getPatternAUX(unsigned int, std::vector<uint32_t>&,std::vector<uint32_t>&, std::vector<uint32_t>&, unsigned int&, bool loadAllSectors = false, unsigned int lamb = 4) const;

 //unsigned int getChipDCBits(unsigned il) { return m_chipdcbits[il]; }

 //void setSSOffset(unsigned il, unsigned val) { m_ssoffset[il] = val;}

 //  void calculateChecksum();
 // void updateChecksum(const unsigned int , const std::vector<uint32_t> &);

  /** rencode the SS in a representation with a different number of DC bits */
 //  static unsigned int convertSSAdjustTernaryBits(unsigned int, unsigned int, unsigned int);
  /** Extract a mask showing where the DC bits are used in a SS */
 // static unsigned int extractDCBits(unsigned int, unsigned int);

  /** Helper function that converts a pattern number into a road ID */
 //  static unsigned int getHWRoadID(unsigned int id, unsigned int nPatternsPerChip, unsigned int offset=1);
};
using FtkPatternBank=FtkPatternBankImpl<>;
#endif // PATTERNBANK_LIB_H
