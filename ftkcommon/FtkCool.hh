#ifndef _FTK_COOL_HH_
#define _FTK_COOL_HH_
#include <CoolUtils/CoolWritable.h>
#include <CoolUtils/CoolReadable.h>
#include <CoolUtils/CoolWriter.h>
#include <CoolUtils/CoolReader.h>
#include <CoolKernel/IObject.h>
#include <tuple>
#include <map>

#include <variant.hpp>

#define FTK_FCONST_FOLDER "/FTK/CONST"
#define FTK_FCONST_PREFIX "FtkConst"

class FtkCoolWritable : public daq::coolutils::CoolWritable {
public:
  FtkCoolWritable(const coral::Blob &pt):m_data(pt) {};
protected:
  virtual void fillRecord( cool::Record& record ) const;
  virtual void setRecordSpec( cool::RecordSpecification& recordSpec ) const;
private:
  const coral::Blob &m_data;
};

class FtkCoolReadable : public daq::coolutils::CoolReadable { \
public:
  FtkCoolReadable():CoolReadable(){};
  void fetch(coral::Blob &pt);
};


class FtkCool {
public:
  FtkCool() {};
  FtkCool(const std::string& coolConnString,const std::string &tag,unsigned int runnumber):
    m_coolConn(coolConnString),m_tag(tag),m_runnumber(runnumber){}; 
  enum class FtkConfigType  : uint8_t {
    CORRGEN_RAW_12L_GCON,
      CORRGEN_RAW_8L_GCON,  
      SECTORS_RAW_8L_CONN,
      PATTERNBANK_8M_8L
      } ;
  void write(const storage::variant &ss,FtkConfigType config,unsigned region);
  void read(storage::variant &pt,FtkConfigType config,unsigned region);
private:
  std::string m_coolConn;
  std::string m_tag;
  unsigned int m_runnumber;
  std::string _folder(FtkConfigType t);
  std::string _tag(FtkConfigType t);
  unsigned _nparts(FtkConfigType t);
  bool _selfContained(FtkConfigType t);
  static std::map<const FtkConfigType,
		  std::tuple<const std::string,const std::string,const uint8_t,bool>
		  > m_configpars;

};


#endif
