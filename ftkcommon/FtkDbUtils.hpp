#ifndef __FTK_DB_UTILS_HPP__
#define __FTK_DB_UTILS_HPP__

#include <iostream>
#include <fstream>
#include <string>
#include <TFile.h>
#include <TTree.h>
#include <boost/filesystem.hpp>
#include <boost/range.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/filter/bzip2.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <variant.hpp>

template <unsigned NPLANES=8,unsigned NPATTERNS=1024*1024*8,unsigned NPARTS=32> 
class _FtkDbUtils {
public:
  static std::string findPatternBank(const std::string &patternDir,unsigned region) {
    boost::filesystem::path path(patternDir);
    std::string patternFile;
    for(auto & p : boost::make_iterator_range(boost::filesystem::directory_iterator(path), {})) {
      std::string entry=p.path().string();
      std::string match="reg"+std::to_string(region);
      if(entry.find("pbank.root") != std::string::npos && entry.find(match) != std::string::npos)  {
	patternFile=entry; 
	break;
      } 
    }
    return patternFile;
  }

  static bool readPatternBank(const std::string &patternDir,unsigned region,storage::variant &var) {
    std::string patternFile=findPatternBank(patternDir,region);
    if(patternFile=="") return false;
    TFile rootcache(patternFile.c_str());
    TTree *ssmap = dynamic_cast<TTree*>(rootcache.Get("SSMap")); 
    int ndcx;
    int ndcy;
    ssmap->SetBranchAddress("ndcx", &ndcx);
    ssmap->SetBranchAddress("ndcy", &ndcy);
    std::vector<uint32_t> v_ndcy;
    std::vector<uint32_t> v_ndcx;
    unsigned n_ssmap=ssmap->GetEntries();
  if(n_ssmap!=NPLANES) throw;
  for(unsigned i=0;i<n_ssmap;i++) {
      ssmap->GetEntry(i);
      v_ndcx.push_back(ndcx);
      v_ndcy.push_back(ndcy);
    }
	TTree *amtree = dynamic_cast<TTree *>(rootcache.Get("Bank"));
  unsigned npatterns=amtree->GetEntries();
  if(NPATTERNS!=npatterns) throw;
  int nplanes; // number of planes
  int ssid[NPLANES]; //[m_nplanes] SS id on each plane
  int sectorID; // sector containing this pattern
  int coverage; // number of events that created this pattern
  int dcmask; // DC mask for this pattern
  int hbmask; // half-bin mask
  amtree->SetBranchAddress("nplanes", &nplanes);
  amtree->SetBranchAddress("ssid", &ssid);
  amtree->SetBranchAddress("sectorID", &sectorID);
  amtree->SetBranchAddress("coverage", &coverage);
  amtree->SetBranchAddress("dcmask", &dcmask);
  amtree->SetBranchAddress("hbmask", &hbmask);
  amtree->GetEntry(0);
  if (nplanes!=NPLANES) throw;
  
  const unsigned npart=NPARTS;
  for(unsigned i=0;i<npart;i++) var.push_back(storage::variant());
  for (unsigned part=0;part<npart;part++) {
    std::vector<uint32_t> v_dcmask;
    std::vector<uint32_t> v_hbmask;
    std::vector<int16_t> v_sectorID;
    std::vector<int16_t> v_ssid;
    for(unsigned i=0;i<npatterns/npart;i++) {
      unsigned ipatt=(part*(npatterns/npart)+i);
      amtree->GetEntry(ipatt);
      v_sectorID.push_back(sectorID);
      v_dcmask.push_back(dcmask);
      v_hbmask.push_back(hbmask);
      
      for(unsigned j=0;j<NPLANES;j++) {
	v_ssid.push_back(ssid[j]);
      }       
    }
    var[part]["ndcx"]=v_ndcx;
    var[part]["ndcy"]=v_ndcy;
    var[part]["dcmask"]=v_dcmask;
    var[part]["hbmask"]=v_hbmask;
    var[part]["sectorID"]=v_sectorID;
    var[part]["ssid"]=v_ssid;
    var[part]["blob_start"]=part*npart;
    var[part]["blob_len"]=npatterns/npart;
    var[part]["blob_nr"]=part;    
  }
  return true;
}

  
template <unsigned nlayer>
static bool readFitConstants(const std::string &constDir,unsigned region,storage::variant &var) {
  static_assert(nlayer==8 || nlayer==12,"readFitConstants: number of layers must be 8 or 12");
  storage::variant sectors;
  std::vector<std::string> keywords={"Vc","Vd","Vf","Vz0","Vo","kernel","Cc","Cd","Cf","Cz0","Co","kaverages"};
  
  std::string corrgen;
  if(nlayer==8) corrgen="/corrgen_raw_8L_reg";
  else if(nlayer==12) corrgen="/corrgen_raw_12L_reg";
  else throw; // should never happen
  std::string fileName=constDir+corrgen+std::to_string(region)+".gcon.bz2";
  std::cout << fileName << std::endl;
  std::vector<float> float_array;
  std::string currentKey="";
  
  int nplanes;
  int ndim;
  int nsectors;
  int sector=-1;
  storage::variant sec;
  std::ifstream is(fileName, std::ios::binary);
  boost::iostreams::filtering_istream in;
  in.push(boost::iostreams::bzip2_decompressor());  
  in.push(is);
  
  for(std::string str; std::getline(in, str); )
    {
      //	          std::cerr << str << std::endl;
      if(str.find("NPLANES")!=std::string::npos) {
	std::getline(in, str);
	nplanes=std::stoi(str);
	if(nplanes!=nlayer) throw;
	  
      } else if (str.find("SECTORS")!=std::string::npos) {
	std::getline(in, str);
	nsectors=std::stoi(str);      
      } else if (str.find("NDIM")!=std::string::npos) {
	std::getline(in, str);
	ndim=std::stoi(str); 
	
	}  
      else if (str.find("sector")!=std::string::npos) {
	if(currentKey!="")
	  if(float_array.size()==1) 
	    sec[currentKey]=float_array[0]; 
	  else  sec[currentKey]=float_array;
	
	std::getline(in, str);
	if(sector>=0) {
	  sectors.push_back(sec);
	  sec=storage::variant();
	  //      j_sectors[std::to_string(sector)]=j_sector;
          //if(sector==10) break;
	}
	sector=std::stoi(str);
	//            std::cerr << sector << std::endl;
	
      }  
      else { 
	str.erase(std::remove_if(str.begin(), str.end(), isspace),str.end());
	if (std::find(std::begin(keywords), std::end(keywords), str) != std::end(keywords)) {
	  
	  if(currentKey!="")   
	    if(float_array.size()==1)
	      sec[currentKey]=float_array[0]; 
	    else
	      sec[currentKey]=float_array;
	  float_array.clear();    
	  currentKey=str;
	}
	if(isFloat(str)) {
	  double f=std::stof(str);
	  float_array.push_back(f);	    
	}
      }
	
    }
  if(float_array.size()==1)
    sec[currentKey]=float_array[0]; 
  else
    sec[currentKey]=float_array;
  sectors.push_back(sec);
  var["sectors"]=sectors;
    var["ndim"]=uint32_t(ndim);
    var["nplanes"]=uint32_t(nplanes);
    
    return true;
}
static bool readSectorConn(const std::string &constDir,unsigned region,storage::variant &var) {
  storage::variant vvmap;
  std::string line;
  std::string fileName=constDir+"/sectors_raw_8L_reg"+std::to_string(region)+".conn";
  std::ifstream infile(fileName);
  unsigned index=0;
  while (std::getline(infile, line)) {
    std::istringstream iss(line);
    std::vector<uint16_t> output;
    while (iss) {
      uint16_t el;
      iss >> el;
      output.push_back(el);
    }
    vvmap.push_back(storage::variant());
    vvmap[index]=output;
    index++;
  }
  var["conn"]=vvmap;   
  return true;
}


private:
 static  bool isFloat(std::string s) {
  std::istringstream iss(s);
  float dummy;
  iss >> std::noskipws >> dummy;
  return iss && iss.eof();  
}

  };

using FtkDbUtils=_FtkDbUtils<>;
#endif
