/**
 *  @author Francesco Cervigni
 *  @date Dec 2010
 *
 *  @file Declares FTK exceptions
 *
 */

#ifndef FTK_EXCEPTIONS_H
#define FTK_EXCEPTIONS_H

/////////////////////
//	
//	In order to catch one of these errors:
//
//	try
//	{
//	...yourcode...
//	}
//	catch ( daq::ftk::PartitionProblem& e)
//  {
//    std::cerr << "- Error during your code :" << e.what() << std::endl;
//  }
//
//////////////////////

#include <ers/ers.h>
#include <ipc/exceptions.h>
#include <is/exceptions.h>
#include <oh/exceptions.h>
#include <iomanip>

using namespace std;

inline std::string name_ftk(){ return "";}

namespace daq
{
       			
  ERS_DECLARE_ISSUE( ftk, 
    PartitionProblem,
    "FTK:"<< name << ": The parameter '" << parameter << "' was not found in the configuration file",
    ((std::string)name) ((std::string)parameter )
  )

  ERS_DECLARE_ISSUE( ftk,
    IOError,
    "FTK:"<< name << ":Found I/O error : " << parameter ,
    ((std::string)name) ((std::string)parameter )
  )

  ERS_DECLARE_ISSUE( ftk, 
    EmptyStringParameter,
    "FTK:"<< name << ":The insterted parameter is empty: " << parameter ,
    ((std::string)name) ((std::string)parameter )
  )
	
  ERS_DECLARE_ISSUE( ftk, 
    VmeError,
    "Found error when accessing the vme library: " << parameter ,
    ((std::string)parameter )
  )

  ERS_DECLARE_ISSUE( ftk, 
    CMEMError,
    "Found error when accessing the cmem library: " << parameter ,
    ((std::string)parameter )
  )

  ERS_DECLARE_ISSUE( ftk,
    ConversionError,
    "Found error when converting string to integer: " << parameter ,
    ((std::string)parameter )
  )
	
  ERS_DECLARE_ISSUE( ftk, 
    BadConfigFileException,
    "FTK:"<< name << ":'" << parameter << "' was not found in the configuration file",
    ((std::string)name) ((std::string)parameter )
  )
    			
  ERS_DECLARE_ISSUE( ftk, 
    TypeException,
    "FTK:"<< name << ":Error: Type exception" << parameter ,
    ((std::string)name) ((std::string)parameter )
  )
    			
  ERS_DECLARE_ISSUE( ftk, 
    WrongParameters,
    "FTK:"<< name << ":Error: Wrong Parameters"  << parameter ,
    ((std::string)name) ((std::string)parameter )
  )
    			
  ERS_DECLARE_ISSUE( ftk, 
    CoralException,
    "FTK:"<< name << ":Error: Interacting with coral"  << parameter ,
    ((std::string)name) ((std::string)parameter )
  )
    
  ERS_DECLARE_ISSUE( ftk,
    ISException,
    "Error: Publishing on IS: " << parameter ,
    ((std::string)parameter )
  )
  
  ERS_DECLARE_ISSUE( ftk,
    OHException,
    "Error: Publishing on OH: " << parameter ,
    ((std::string)parameter )
  )

  ERS_DECLARE_ISSUE( ftk,
    EnvironmentValueException,
    "FTK:"<< name << ":Error: "  << parameter ,
    ((std::string)name) ((std::string)parameter )
  )

  ERS_DECLARE_ISSUE( ftk,
    Information,
    parameter ,
    ((std::string)parameter )
  )
	
  ERS_DECLARE_ISSUE( ftk, 
    StatusRegisterIssue,
    "Error: operation with an FTK status register: " << parameter ,
    ((std::string)parameter )
  )

  ERS_DECLARE_ISSUE( ftk,
    IPBusIssue,
    "FTK:"<< name << ": Error: IPBus access: " << parameter ,
    ((std::string)name) ((std::string)parameter )
  )

  ERS_DECLARE_ISSUE( ftk,
    IPBusRead,
    "Error: IPBus read: " << parameter ,
    ((std::string)parameter )
  )

  ERS_DECLARE_ISSUE( ftk,
    i2cIPBusIssue,
    "FTK:"<< name <<": Error: i2c+IPBus access: " << parameter ,
    ((std::string)name) ((std::string)parameter )
  )

  ERS_DECLARE_ISSUE( ftk,
    i2cIPBusRead,
    "Error: i2c+IPBus read: " << parameter ,
    ((std::string)parameter )
  )

  ERS_DECLARE_ISSUE( ftk,
    i2cIPBusWrite,
    "Error: i2c+IPBus write: " << parameter ,
    ((std::string)parameter )
  )

  ERS_DECLARE_ISSUE( ftk,
    IMLUTWarning,
    "FTK:"<< name << ":Warning: " << par0 << " Failed IM LUT upload: " 
                <<        hex<<setw(4)<<setfill('0')<<par1
	        <<" != "<<hex<<setw(4)<<setfill('0')<<par2
	        <<" or "<<hex<<setw(4)<<setfill('0')<<par3
	        <<" != "<<hex<<setw(4)<<setfill('0')<<par4
                <<". Will be retried...",
    ((std::string)name) ((std::string)par0)((uint32_t)par1)((uint32_t)par2)((uint32_t)par3)((uint32_t)par4)
  )

  ERS_DECLARE_ISSUE( ftk,
    WrongFwVersion,
    "FTK:"<< name << ":Wrong FW version of "<<par1<<" FPGA "<<par2
	        <<": "<<hex<<par3
	        <<", while expected "
                <<hex<<par4,
    ((std::string)name) ((std::string)par1)((uint32_t)par2)((uint32_t)par3)((uint32_t)par4)
  )

  ERS_DECLARE_ISSUE( ftk,
		     EventFragmentCollectionAddWrongL1ID,
		     "Adding event fragment with L1ID " << std::hex << badL1ID
		     << " to a collection containing L1IDs " << std::hex << collL1ID,
		     ((uint32_t)collL1ID)((uint32_t)badL1ID)
		     )

  ERS_DECLARE_ISSUE( ftk,
		     EventFragmentCollectionMissingIdx,
		     "Requesting subset with " << idx << " when set only has "  << size << " items",
		     ((uint32_t)idx)((uint32_t)size)
		     )

  ERS_DECLARE_ISSUE( ftk,
                     FTKInfoNotAcquired,
                     "FTK SpyBuffer sampling: parameter " << parameter << " not found. Setting it to zero.",
                     ((std::string)parameter)
                     )

  ERS_DECLARE_ISSUE( ftk,
                     FTKFwVersion,
                     "FTK:"<< name << ":FTK FW Version: Board " << name << " FPGA " <<fpga_name << ":\t " <<std::hex<<fw_version,
                     ((std::string)name) ((std::string)fpga_name) ((uint32_t)fw_version)
                     )

	ERS_DECLARE_ISSUE( ftk,
                     ftkException,
                     "FTK:"<< name  << ": " << message, 
                     ((std::string)name) ((std::string)message)
										)

  ERS_DECLARE_ISSUE( ftk,
                     FTKIssue,
                     "FTK:"<< name << ":Generic FTK Issue: " << parameter ,
                     ((std::string)name) ((std::string)parameter )
	  							 )

  ERS_DECLARE_ISSUE( ftk,
                     FTKLockFailed,
                     "FTK:"<< name << ":Could not lock " << mutex_desc 
                     << " mutex in " << context_str << " after waiting " << nminutes_waited << " minutes. " << action_taken,
                     ((std::string)name) ((std::string)mutex_desc) ((std::string)context_str) ((uint32_t)nminutes_waited) ((std::string)action_taken)
                   )

  ERS_DECLARE_ISSUE( ftk,
                     FTKLinkDown,
                     "FTK:"<< name  << ": Link " << link << "down! " << message,
                     ((std::string)name) ((std::string)link) ((std::string)message)
                    )

  ERS_DECLARE_ISSUE( ftk,
                     FTKWrongChecksum,
                     "FTK:"<< name  << ": Wrong checksum for FPGA " << fpga_name << "! Expected checksum: " << std::hex << 
                              exp_checksum  << ", computed checksum: " << comp_checksum << ".\n File path: " << data_path << ".",
                     ((std::string)name) ((int)exp_checksum) ((int)comp_checksum) ((std::string)fpga_name) ((std::string)data_path)
                    )

  ERS_DECLARE_ISSUE( ftk,
                     FTKOperationTimedOut,
                     "FTK:"<< name  << ": " << message,
                     ((std::string)name) ((std::string)message)
                    )

  ERS_DECLARE_ISSUE( ftk,
                     FTKMonitoringError,
                     "FTK:"<< name  << ": " << message,
                     ((std::string)name) ((std::string)message)
                    )

  ERS_DECLARE_ISSUE( ftk,
                     FTKRecoveryIssue,
                     "FTK:"<< name  << ":Recovery procedure Issue. " << message,
                     ((std::string)name) ((std::string)message)
                    )

} // namespace daq

#endif
