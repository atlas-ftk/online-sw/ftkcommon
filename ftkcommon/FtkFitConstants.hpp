#ifndef FTK_FIT_CONSTANTS_HPP
#define FTK_FIT_CONSTANTS_HPP

#include <vector>
#include <variant.hpp>


class FtkFitConstants {
 public:
  FtkFitConstants(const storage::variant &v):m_var(v) {
    m_sec=&v["sectors"];
  }
  inline double getFitConst( unsigned isec , const std::string &ipar ){
    return (*m_sec)[isec][ipar];
 }
  inline const std::vector<double>  &getFitPar( unsigned isec , const std::string ipar) { const std::vector<double> &res= (*m_sec)[isec][ipar]; return res; }
  inline const std::vector<double> &getKaverage( int isec) { const std::vector<double> &res=(*m_sec)[isec]["kaverages"]; return res; }
  inline const std::vector<double> &getKernel( int isec ) {  
    const storage::variant &sec=(*m_sec)[isec];      
    const std::vector<double> &res=sec["kernel"]; 
    return res; }
  int getNSectors()  { return (*m_sec).size(); }
 private:
  storage::variant empty;
  const storage::variant &m_var;
  const storage::variant *m_sec;
};
#endif 
