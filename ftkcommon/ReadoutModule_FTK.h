#ifndef READOUT_MODULE_FTK_H
#define READOUT_MODULE_FTK_H

#include <queue>
#include <condition_variable>
#include <future>
#include <functional>
#include <mutex>
#include <memory>
#include <functional>
#include <atomic>
#include <thread>
#include <vector>

//TO BE DELETED
#include "ftkcommon/dal/FTKReadoutCommonNamed.h"

#include "tbb/concurrent_queue.h"

#include "ROSCore/ReadoutModule.h"
#include "RunControl/Common/OnlineServices.h"
#include "RunControl/Common/CommandSender.h"
#include "RunControl/FSM/FSMCommands.h"
#include "DFdal/RCD.h"

#include "ftkcommon/dal/ReadoutModule_FTK.h"
#include "ftkcommon/core.h"
#include "ftkcommon/exceptions.h"
#include "ftkcommon/dal/ReadoutConfiguration_FTK.h"

namespace daq {
  namespace ftk {

    bool attempt_lock_for_n_minutes( std::unique_lock<std::timed_mutex>&, const uint32_t );

    typedef enum { BOARD_UNKNOWN, BOARD_IMDF, BOARD_PU, BOARD_AUX, BOARD_AMB, BOARD_SSB, BOARD_FLIC, BOARD_FLICBLADE, BOARD_DUMMY }
                    ftk_board_t;

    // bit masks to parallelize RC transitions (one thread per ReadoutModule object)
    const uint32_t READOUTMODULE_FTK_PARALLEL_CONFIGURE       = 0x0001;
    const uint32_t READOUTMODULE_FTK_PARALLEL_CONNECT         = 0x0002;
    const uint32_t READOUTMODULE_FTK_PARALLEL_PREPAREFORRUN   = 0x0004;
    const uint32_t READOUTMODULE_FTK_PARALLEL_STOPROIB        = 0x0008;
    const uint32_t READOUTMODULE_FTK_PARALLEL_STOPDC          = 0x0010;
    const uint32_t READOUTMODULE_FTK_PARALLEL_STOPHLT         = 0x0020;
    const uint32_t READOUTMODULE_FTK_PARALLEL_STOPRECORDING   = 0x0040;
    const uint32_t READOUTMODULE_FTK_PARALLEL_STOPGATHERING   = 0x0080;
    const uint32_t READOUTMODULE_FTK_PARALLEL_STOPARCHIVING   = 0x0100;
    const uint32_t READOUTMODULE_FTK_PARALLEL_DISCONNECT      = 0x0200;
    const uint32_t READOUTMODULE_FTK_PARALLEL_UNCONFIGURE     = 0x0400;
    //const uint32_t READOUTMODULE_FTK_PARALLEL_CLEARINFO       = 0x0800;

    // bit masks to configure publish(FullStats) parallelization
    // use MT publish (not in RCD-controlled thread)
    const uint32_t READOUTMODULE_FTK_PARALLEL_MONITORING      = 0x1000;
    // different threads for publish and publishFullStats (default same thread for both)
    const uint32_t READOUTMODULE_FTK_SEPARATE_PUBLISH_THREADS = 0x2000;
    // Protect prepareForRun from running at the same time as publish for each board
    // This causes interruptPublish to go high and for the mutex to be locked
    const uint32_t READOUTMODULE_FTK_PROTECT_START            = 0x4000;
    // Protect stop from running at the same time as publish for each board
    const uint32_t READOUTMODULE_FTK_PROTECT_STOP             = 0x8000;

    // All RM's with this flag set will serialize publish(FullStats) between them
    const uint32_t READOUTMODULE_FTK_SERIALIZE_RCD_MONITORING = 0x10000;

    const uint32_t READOUTMODULE_FTK_CONFIG_DEFAULT         = READOUTMODULE_FTK_PROTECT_START | READOUTMODULE_FTK_PROTECT_STOP; // default don't parallelize

    // in case we serialize publish and publishFullStats, need to be able to differentiate
    // if this bit in the flag is high, call publishFullStats. Else call publish
    const uint32_t READOUTMODULE_FTK_PUBLISHFULLSTATS_BIT = 0x80000000;

    uint32_t getConfigFlagsFromDAL( const dal::ReadoutModule_FTK* module_dal ) {
      uint32_t retval = 0x0;

      if ( module_dal->get_UseDefaultConfig() ) return READOUTMODULE_FTK_CONFIG_DEFAULT;

      if ( module_dal->get_ParallelConfigure() )
        retval |= READOUTMODULE_FTK_PARALLEL_CONFIGURE;
      if ( module_dal->get_ParallelConnect() )
        retval |= READOUTMODULE_FTK_PARALLEL_CONNECT;
      if ( module_dal->get_ParallelPrepareForRun() )
        retval |= READOUTMODULE_FTK_PARALLEL_PREPAREFORRUN;
      if ( module_dal->get_ParallelStopROIB() )
        retval |= READOUTMODULE_FTK_PARALLEL_STOPROIB;
      if ( module_dal->get_ParallelStopDC() )
        retval |= READOUTMODULE_FTK_PARALLEL_STOPDC;
      if ( module_dal->get_ParallelStopHLT() )
        retval |= READOUTMODULE_FTK_PARALLEL_STOPHLT;
      if ( module_dal->get_ParallelStopRecording() )
        retval |= READOUTMODULE_FTK_PARALLEL_STOPRECORDING;
      if ( module_dal->get_ParallelStopGathering() )
        retval |= READOUTMODULE_FTK_PARALLEL_STOPGATHERING;
      if ( module_dal->get_ParallelStopArchiving() )
        retval |= READOUTMODULE_FTK_PARALLEL_STOPARCHIVING;
      if ( module_dal->get_ParallelDisconnect() )
        retval |= READOUTMODULE_FTK_PARALLEL_DISCONNECT;
      if ( module_dal->get_ParallelUnconfigure() )
        retval |= READOUTMODULE_FTK_PARALLEL_UNCONFIGURE;
      if ( module_dal->get_ParallelMonitoring() )
        retval |= READOUTMODULE_FTK_PARALLEL_MONITORING;
      if ( module_dal->get_SeparatePublishThreads() )
        retval |= READOUTMODULE_FTK_SEPARATE_PUBLISH_THREADS;
      if ( module_dal->get_ProtectStart() )
        retval |= READOUTMODULE_FTK_PROTECT_START;
      if ( module_dal->get_ProtectStop() )
        retval |= READOUTMODULE_FTK_PROTECT_STOP;
      if ( module_dal->get_SerializeRCDMonitoring() )
        retval |= READOUTMODULE_FTK_SERIALIZE_RCD_MONITORING;

      return retval;
    }

    class ReadoutModule_FTK : public ROS::ReadoutModule
    {
    public:

      // These methods are final; subclasses cannot override them.
      // The functionality should be implemented by subclasses in the pure virtual do* functions
      virtual void configure    ( const daq::rc::TransitionCmd& ) override final;
      virtual void connect      ( const daq::rc::TransitionCmd& ) override final;
      virtual void prepareForRun( const daq::rc::TransitionCmd& ) override final;
      virtual void stopROIB     ( const daq::rc::TransitionCmd& ) override final;
      virtual void stopDC       ( const daq::rc::TransitionCmd& ) override final;
      virtual void stopHLT      ( const daq::rc::TransitionCmd& ) override final;
      virtual void stopRecording( const daq::rc::TransitionCmd& ) override final;
      virtual void stopGathering( const daq::rc::TransitionCmd& ) override final;
      virtual void stopArchiving( const daq::rc::TransitionCmd& ) override final;
      virtual void disconnect   ( const daq::rc::TransitionCmd& ) override final;
      virtual void unconfigure  ( const daq::rc::TransitionCmd& ) override final;

      virtual void publish()          override final;
      virtual void publishFullStats() override final;

      // These methods are required; subclasses must implement them.
      virtual void doConfigure    ( const daq::rc::TransitionCmd& ) = 0;
      virtual void doConnect      ( const daq::rc::TransitionCmd& ) = 0;
      virtual void doPrepareForRun( const daq::rc::TransitionCmd& ) = 0;
      // (maybe we decide to rename these functions)
      virtual void doStopROIB     ( const daq::rc::TransitionCmd& ) = 0;
      virtual void doStopDC       ( const daq::rc::TransitionCmd& ) = 0;
      virtual void doStopHLT      ( const daq::rc::TransitionCmd& ) = 0;
      virtual void doStopRecording( const daq::rc::TransitionCmd& ) = 0;
      virtual void doStopGathering( const daq::rc::TransitionCmd& ) = 0;
      virtual void doStopArchiving( const daq::rc::TransitionCmd& ) = 0;
      virtual void doDisconnect   ( const daq::rc::TransitionCmd& ) = 0;
      virtual void doUnconfigure  ( const daq::rc::TransitionCmd& ) = 0;

      // subtransitions are not required
      virtual void checkConnect( const daq::rc::SubTransitionCmd& ) {}
      void callCheckConnect( const daq::rc::SubTransitionCmd& );
      virtual void startNoDF( const daq::rc::SubTransitionCmd& ) {}
      void callStartNoDF( const daq::rc::SubTransitionCmd& );
      virtual void checkConfig( const daq::rc::SubTransitionCmd& ) {}
      void callCheckConfig( const daq::rc::SubTransitionCmd& );

      // Special transition methods for stopless recovery (optionally override in subclass)
      virtual void doSRConfigure    ( const daq::rc::TransitionCmd& cmd ) { doConfigure     ( cmd ); }
      virtual void doSRConnect      ( const daq::rc::TransitionCmd& cmd ) { doConnect       ( cmd ); }
      virtual void doSRPrepareForRun( const daq::rc::TransitionCmd& cmd ) { doPrepareForRun ( cmd ); }
      virtual void doSRStopROIB     ( const daq::rc::TransitionCmd& cmd ) { doStopROIB      ( cmd ); }
      virtual void doSRStopDC       ( const daq::rc::TransitionCmd& cmd ) { doStopDC        ( cmd ); }
      virtual void doSRStopHLT      ( const daq::rc::TransitionCmd& cmd ) { doStopHLT       ( cmd ); }
      virtual void doSRStopRecording( const daq::rc::TransitionCmd& cmd ) { doStopRecording ( cmd ); }
      virtual void doSRStopGathering( const daq::rc::TransitionCmd& cmd ) { doStopGathering ( cmd ); }
      virtual void doSRStopArchiving( const daq::rc::TransitionCmd& cmd ) { doStopArchiving ( cmd ); }
      virtual void doSRDisconnect   ( const daq::rc::TransitionCmd& cmd ) { doDisconnect    ( cmd ); }
      virtual void doSRUnconfigure  ( const daq::rc::TransitionCmd& cmd ) { doUnconfigure   ( cmd ); }
      virtual void checkSRConnect   ( const daq::rc::SubTransitionCmd& cmd ) { checkConnect ( cmd ); }
      virtual void checkSRConfig    ( const daq::rc::SubTransitionCmd& cmd ) { checkConfig ( cmd ); }
			virtual void startSRNoDF 			( const daq::rc::SubTransitionCmd& cmd ) { startNoDF ( cmd ); }

      // These methods are required; subclasses must implement them.
      virtual void doPublish( uint32_t publishFlag, bool finalPublish ) = 0;
      virtual void doPublishFullStats( uint32_t publishFlag ) = 0;

      virtual void subTransition( const daq::rc::SubTransitionCmd& cmd );

      virtual ~ReadoutModule_FTK() noexcept;

      // disallow copy semantics
      ReadoutModule_FTK( const ReadoutModule_FTK& ) = delete;
      ReadoutModule_FTK& operator =( const ReadoutModule_FTK& ) = delete;

      std::string name_ftk() { return m_name; }

      virtual void user( const daq::rc::UserCmd& cmd ) final;

    protected:

      // The only constructor is protected, can't instantiate this class.
      ReadoutModule_FTK();

      // user commands other than stopless recovery will be forwarded from user
      // to this function, so each derived class can override as desired
      virtual void doUser( const daq::rc::UserCmd& cmd ) {}

      // Called in the "setup" method of the subclass, which is not implemented here.
      void initialize( const std::string& name, 
                       const dal::ReadoutModule_FTK* config,
                       ftk_board_t type=BOARD_UNKNOWN );

      // in enablePublish, launch thread(s) as necessary to execute the monitoring functions
      void enablePublish();
      void disablePublish() { m_enablePublish = 0x0; }

      //CPU and Memory Usage
      //void monitoringMemoryUsage();
      //double parseLine_memory(char* line);

      std::atomic<uint32_t> m_lastPublishFlag;
      virtual uint32_t nextPublishFlag() // virtual; allow override
       { return ( m_lastPublishFlag = (m_lastPublishFlag+1)&(~READOUTMODULE_FTK_PUBLISHFULLSTATS_BIT) ); }
      virtual void resetPublishFlag() { m_lastPublishFlag = 0xFFFFFFFF; }
      std::atomic<uint32_t> m_lastPublishFullStatsFlag;
      virtual uint32_t nextPublishFullStatsFlag()
       { return ( m_lastPublishFullStatsFlag = (m_lastPublishFullStatsFlag+1)&(~READOUTMODULE_FTK_PUBLISHFULLSTATS_BIT) ); }
      virtual void resetPublishFullStatsFlag() { m_lastPublishFullStatsFlag = 0xFFFFFFFF; }

      void resetPublishQueues(); // reset the publish request queues

      uint32_t interruptPublish() { return m_interruptPublish.load(); }

    private:

       FTKReadoutCommonNamed                          *m_FTKReadoutCommonNamed;      // Access IS via schema 


      std::atomic<uint32_t> m_initialized;
      std::atomic<uint32_t> m_thread_mon_close;

      std::string m_name;
      std::string m_appName;
      std::string m_manager;
      IPCPartition m_ipcpartition;
      ftk_board_t m_type;
      std::atomic<uint32_t> m_configuration;
      // we have both enablePublish for only this RM, and interruptPublish which is static (shared by all RMs)
      std::atomic<uint32_t> m_enablePublish;

      // this atomic will be set high when a stop sub-transition is reached
      // derived classes should check it's value periodically and return from publish or publishFullStats when nonzero
      static std::atomic<uint32_t> m_interruptPublish;

      // stopless recovery implementation
      void user_sender( const std::string receiver, const std::string cmd_name, const std::vector<std::string>& cmd_params );
      void ACKsender( const std::string state );
      void getManager();
      void SRstopDC();
      void SRstopROIB();
      void SRstopHLT();
      void SRstopRecording();
      void SRstopGathering();
      void SRstopArchiving();
      void SRdisconnect();
      void SRunconfigure();
      void SRconfigure();
      void SRcheckConfig();
      void SRconnect();
      void SRcheckConnect();
			void SRstartNoDF();
      void SRprepareForRun();

      // static atomic flags controlling MT transitions
      static std::atomic<uint32_t> m_configure_performed;
      static std::atomic<uint32_t> m_connect_performed;
      static std::atomic<uint32_t> m_prepareForRun_performed;
      static std::atomic<uint32_t> m_stopROIB_performed;
      static std::atomic<uint32_t> m_stopDC_performed;
      static std::atomic<uint32_t> m_stopHLT_performed;
      static std::atomic<uint32_t> m_stopRecording_performed;
      static std::atomic<uint32_t> m_stopGathering_performed;
      static std::atomic<uint32_t> m_stopArchiving_performed;
      static std::atomic<uint32_t> m_disconnect_performed;
      static std::atomic<uint32_t> m_checkConfig_performed;
      static std::atomic<uint32_t> m_unconfigure_performed;
      static std::atomic<uint32_t> m_checkConnect_performed;
      static std::atomic<uint32_t> m_startNoDF_performed;

      // an "atomic" version of the prepareForRun method with protection against concurrent access
      // this is needed in case publish is being run when the start transition happens
      void prepareForRun_atomic( const daq::rc::TransitionCmd& cmd ) {
        if ( m_configuration & READOUTMODULE_FTK_PROTECT_START ) {
          disablePublish(); // stop publishing temporarily, just for this board

          std::unique_lock<std::timed_mutex> lock( m_local_mtx, std::defer_lock );
          const uint32_t NMINUTES_TO_WAIT = 4;
          if ( attempt_lock_for_n_minutes( lock, NMINUTES_TO_WAIT ) ) { // succesfully locked
            doPrepareForRun( cmd );
          } else { // there was a problem locking the mutex

          }
        } else {
          doPrepareForRun( cmd );
        }

        // this is the last possible moment to start publishing before we go to Running
        resetPublishFlag();
        resetPublishQueues();
        if ( !m_enablePublish ) enablePublish();
      }
      void SRprepareForRun_atomic( const daq::rc::TransitionCmd& cmd ) {
        if ( m_configuration & READOUTMODULE_FTK_PROTECT_START ) {
          disablePublish(); // stop publishing temporarily, just for this board

          std::unique_lock<std::timed_mutex> lock( m_local_mtx, std::defer_lock );
          const uint32_t NMINUTES_TO_WAIT = 4;
          if ( attempt_lock_for_n_minutes( lock, NMINUTES_TO_WAIT ) ) { // succesfully locked
            doSRPrepareForRun( cmd );
          } else { // there was a problem locking the mutex

          }
        } else {
          doSRPrepareForRun( cmd );
        }

        // this is the last possible moment to start publishing before we go to Running
        resetPublishFlag();
        resetPublishQueues();
        if ( !m_enablePublish ) enablePublish();
      }

      // an "atomic" version of the stopROIB method with protection against concurrent access
      // Since this is the first stop subtransition, it is sufficient to disable publishing here
      void stopROIB_atomic( const daq::rc::TransitionCmd& cmd ) {
        if ( m_configuration & READOUTMODULE_FTK_PROTECT_STOP ) {
          disablePublish();
          m_interruptPublish = 0x1; // stop publishing for all ReadoutModules in the RCD

          std::unique_lock<std::timed_mutex> lock( m_local_mtx, std::defer_lock );
          const uint32_t NMINUTES_TO_WAIT = 4;
          if ( attempt_lock_for_n_minutes( lock, NMINUTES_TO_WAIT ) ) {
            doStopROIB( cmd );
          } else {

          }
        } else doStopROIB( cmd );
      }
      void SRstopROIB_atomic( const daq::rc::TransitionCmd& cmd ) {
        if ( m_configuration & READOUTMODULE_FTK_PROTECT_STOP ) {
          disablePublish();
          m_interruptPublish = 0x1; // stop publishing for all ReadoutModules in the RCD

          std::unique_lock<std::timed_mutex> lock( m_local_mtx, std::defer_lock );
          const uint32_t NMINUTES_TO_WAIT = 4;
          if ( attempt_lock_for_n_minutes( lock, NMINUTES_TO_WAIT ) ) {
            doSRStopROIB( cmd );
          } else {

          }
        } else doSRStopROIB( cmd );
      }

      // launcher functions for monitoring threads
      void publishLauncher();
      void publishFullStatsLauncher();
      void monitoringLauncher();
      // queues to hold monitoring requests
      tbb::concurrent_bounded_queue<bool> m_monitoringQueue;
      std::atomic<uint32_t> m_publishQueue;
      std::atomic<uint32_t> m_publishFullStatsQueue;

      // Need to keep a reference to each existing ReadoutModule to parallelize transitions.
      // require m_name is unique in the process (should already be required by OKS)
      static std::map<std::string, ReadoutModule_FTK*> m_enabledReadoutModules;
      // protection for the static resources
      static std::timed_mutex m_global_mtx;
      // serialize publish(FullStats) and state transitions
      std::timed_mutex m_local_mtx;
      // serialize publish(FullStats) between RM's in the same RCD
      static std::timed_mutex m_global_monitoring_mtx;

      // Thread limits from the RCD configuration
      const dal::ReadoutConfiguration_FTK* getReadoutConfig();
      uint32_t globalThreadLimit() { return getReadoutConfig()->get_GlobalThreadLimit(); }
      uint32_t configureThreadLimit();
      uint32_t connectThreadLimit();
      uint32_t prepareForRunThreadLimit();
      uint32_t stopThreadLimit();
      uint32_t disconnectThreadLimit();
      uint32_t unconfigureThreadLimit();
      static std::atomic<uint32_t> m_thread_limits_set;
      static uint32_t m_configureThreadLimit;
      static uint32_t m_connectThreadLimit;
      static uint32_t m_prepareForRunThreadLimit;
      static uint32_t m_stopThreadLimit;
      static uint32_t m_disconnectThreadLimit;
      static uint32_t m_unconfigureThreadLimit;

      // Utility class to maintain a thread pool
      // taken from https://github.com/progschj/ThreadPool
      // License file reproduced from that repository (fcc9141):
      // Copyright (c) 2012 Jakob Progsch, Václav Zeman
      //
      // This software is provided 'as-is', without any express or implied
      // warranty. In no event will the authors be held liable for any damages
      // arising from the use of this software.
      //
      // Permission is granted to anyone to use this software for any purpose,
      // including commercial applications, and to alter it and redistribute it
      // freely, subject to the following restrictions:
      //
      //    1. The origin of this software must not be misrepresented; you must not
      //       claim that you wrote the original software. If you use this software
      //       in a product, an acknowledgment in the product documentation would be
      //       appreciated but is not required.
      //
      //    2. Altered source versions must be plainly marked as such, and must not be
      //       misrepresented as being the original software.
      //
      //    3. This notice may not be removed or altered from any source
      //       distribution.
      class ThreadPool {
      public:
        ThreadPool( std::string, size_t );
        template<class F, class... Args>
        auto enqueue(F&& f, Args&&... args)
          -> std::future<typename std::result_of<F(Args...)>::type>;
        ~ThreadPool();
        std::string name_ftk() { return m_name; }
      private:
        std::vector< std::thread > workers;
        std::queue< std::function<void()> > tasks;
        std::mutex queue_mutex;
        std::condition_variable condition;
        std::string m_name;
        bool stop;
      }; // ReadoutModule_FTK::ThreadPool

    }; // end class ReadoutModule_FTK

    inline ReadoutModule_FTK::ThreadPool::ThreadPool(std::string name, size_t threads)
      : m_name(name), stop(false)
    {
      for(size_t i = 0;i<threads;++i)
        workers.emplace_back(
          [this]
          {
            for(;;)
            {
              std::function<void()> task;
              {
                std::unique_lock<std::mutex> lock(this->queue_mutex);
                this->condition.wait(lock,
                  [this]{ return this->stop || !this->tasks.empty(); });
                if(this->stop && this->tasks.empty())
                  return;
                task = std::move(this->tasks.front());
                this->tasks.pop();
              }
              task();
            }
          }
        );
    }
    
    template<class F, class... Args>
    auto ReadoutModule_FTK::ThreadPool::enqueue(F&& f, Args&&... args)
      -> std::future<typename std::result_of<F(Args...)>::type>
    {
      using return_type = typename std::result_of<F(Args...)>::type;
    
      auto task = std::make_shared< std::packaged_task<return_type()> >(
          std::bind(std::forward<F>(f), std::forward<Args>(args)...)
        );
    
      std::future<return_type> res = task->get_future();
      {
        std::unique_lock<std::mutex> lock(queue_mutex);
    
        if(stop) // replacing throw std::runtime_error with ers::fatal
          ers::fatal( daq::ftk::ftkException( ERS_HERE, name_ftk(), "enqueue on stopped ThreadPool") );
    
        tasks.emplace([task](){ (*task)(); });
      }
      condition.notify_one();
      return res;
    }
    
    inline ReadoutModule_FTK::ThreadPool::~ThreadPool()
    {
      {
        std::unique_lock<std::mutex> lock(queue_mutex);
        stop = true;
      }
    
      condition.notify_all();
      for(std::thread &worker: workers)
        worker.join();
    }
 
 }
}

#endif
