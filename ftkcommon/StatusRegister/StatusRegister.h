#ifndef __STATUSREGISTER_H__
#define __STATUSREGISTER_H__

#include <string>
#include <vector>
#include "ftkcommon/core.h"

enum class srType { fifoEmtpy, fifoFull, fifoOther, counterXOFF, counterWords, counterCtrlWords, counterOther, gtOther, srOther, dummy };

enum class srAccess { i2cIPBus, IPBus_direct, IPBus_selector, VME, VME_direct, VME_selector, AMB_selector, IS, dummy };

namespace daq {
namespace ftk {

   /*! \brief The base class to define a status register for monitoring.
    * All inherited class should implement the readout() function.
    * That implementation should take care of the particular access 
    * protocol and any access specialities
    */
  const uint32_t srNoneValue=0xFFFFFFFF;

  class StatusRegister
  {
  public:
    StatusRegister() {};
    virtual ~StatusRegister() {};

    /*! \brief Pure virtual function that should define access procedure for
     * a particular implementation.
     */
    virtual void readout() = 0;

    /*! \brief Virtual function that should define block-transfer access procedure for
     * a particular implementation. Default implementation reads 1 value with 1-by-1 access.
     */
    virtual std::vector<uint32_t> readoutBlockTransfer(uint32_t blockSize = 1) 
     {
       if (blockSize != 1)
	PRINT_LOG("Write a proper implementation of block transfer readout! The base interface can handle readout of a single register only!");
       readout();
       std::vector<uint32_t> vov = {m_value};
       return vov;
     };


    std::string	getName() const					{ return m_name; };
    std::string	getNameShort() const				{ return m_nameShort; };
    uint32_t	getAddress() const				{ return m_address; };
    std::string getNode() const					{ return m_node; };
    uint32_t	getValue() const				{ return m_value; };
    void 	setValue(uint32_t value)			{ m_value = value; };
    srType	getType() const					{ return m_type; };
    srAccess	getAccess() const				{ return m_access; };

  protected:

    uint32_t m_value			= srNoneValue;
    std::string m_name			= "Foo";
    std::string m_nameShort		= "foo";

    uint32_t m_address			= 0x0;
    std::string m_node			= "dummy";

    srType m_type			= srType::dummy;
    srAccess m_access			= srAccess::dummy;

  private:
  };

} // namespace ftk
} // namespace daq

#endif // __STATUSREGISTER_H__
