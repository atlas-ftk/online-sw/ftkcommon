#ifndef STATUSREGISTERFACTORY_H
#define STATUSREGISTERFACTORY_H

#include "ftkcommon/StatusRegister/StatusRegisterCollection.h"
#include "ftkcommon/StatusRegister/StatusRegister.h"


// IS publishing
#include "RunControl/Common/OnlineServices.h"
#include <is/namedinfo.h>

#include <iostream>
#include <atomic>
#include <vector>
#include <string>
#include <cctype>
#include <mutex>
#include <memory>


// Namespaces
namespace daq {
namespace ftk {

/*! \brief Reads out Registers from any board using StatusRegisterCollection objects and publishes the data to IS
 */
class StatusRegisterFactory {
public:
	/*! \brief StatusRegisterFactory class constructor
	 *   
	 * \param boardName The name of the board being monitored
	 * \param registersToRead A string telling the class which collections to create
	 * \param isServerName The name of the IS server to publish to
	 */
	StatusRegisterFactory(
		std::string boardName,
		std::string registersToRead,
		std::string isServerName
	);

	virtual ~StatusRegisterFactory();

	/*! \brief Call the readout functions of all the StatusRegisterCollection objects.
	 */
	virtual void readout();

	/*! \brief Clear IS objects	 */
	void clear();

	/*! \brief Set the default prescale value (1 by default)
	 *  It assumes that all collections have already been setup */
	void setDefPrescale(uint p);

	/*! \brief Retrieve all register contents */
	const std::vector<std::vector<uint>*> getRegisterContents() {return m_isObjectVectors;}

	/*! \brief Retrieve all meta-data about register collections */
	const std::vector<std::vector<uint>*> getMetaData() {return m_isObjectInfoVectors;}


protected:
	/*! \brief Creates a collection if the corresponding character was passed to the Factory constructor. Pure virtual!
	 *
	 * \param IDChar The character which must be passed to the constructor in order for this group of registers to be read out
	 * \param fpgaNum The fpga number the registers are to be read from
	 * \param firstAddress The address of the first register to be read
	 * \param finalAddress The address of the final register to be read
	 * \param addrIncremenet The increment between neighbouring addresses to be read
	 * \param collectionName A descriptive name for the collection of registers
	 * \param collectionShortName A brief name for the collection of registers
	 * \param ISObjectVector The vector in the IS interface object that the register values will be stored in
	 * \param ISObjectInfoVector If IS interface object is provided, a vector containing firstAddress, finalAddress, addrIncremenet, selectorAddress will be stored.
	 * \param selectorAddress Selector address, default value is 0.
	 * \param readerAddress Reader access for selector, default value is 0 
	 * \param type StatusRegister type
	 * \param access StatusRegister access type
	 */
	virtual void setupCollection(
		char IDChar,
		uint fpgaNum,
		uint firstAddress, 
		uint finalAddress,
		uint addrIncrement,
		std::string collectionName, 
		std::string collectionShortName,
		std::vector<uint>* ISObjectVector,
		std::vector<uint>* ISObjectInfoVector=NULL,
                uint selectorAddress = 0,
                uint readerAddress = 0,
		srType type = srType::srOther,
                srAccess access = srAccess::dummy
	) = 0;

	/*! \brief Creates a collection Info objects containing meta-information.
	 *
	 * \param ISObjectInfoVector If IS interface object is provided, a vector containing firstAddress, finalAddress, addrIncremenet, fpgaNum, selectorAddress, readerAddress will be stored.
	 * \param firstAddress The address of the first register to be read
	 * \param finalAddress The address of the final register to be read
	 * \param addrIncremenet The increment between neighbouring addresses to be read
	 * \param fpgaNum The fpga number the registers are to be read from
	 * \param selectorAddress Selector address, default value is 0.
	 * \param readerAddress Reader adress for the selector, default value is 0.
	 */
	void setupCollectionInfo(
		std::vector<uint>* ISObjectInfoVector,
		uint firstAddress, 
		uint finalAddress,
		uint addrIncrement,
		uint fpgaNum,
                uint selectorAddress = 0,
                uint readerAddress = 0
	);

	/*! \brief Freeze/unfreeze the board before readout (if needed). Pure virtual!
	 *
	 * \param access StatuRegister access type
	 * \param fpgaNum The fpga number the registers are to be read from
	 * \param opt Control freeze (= true) or unfreeze (= false)
	 */
	virtual void freeze(srAccess access = srAccess::dummy, uint32_t fpgaNum=0, bool opt=true) = 0;
	/* virtual void freeze_IM(srAccess access = srAccess::dummy, uint32_t fpgaNum=0, bool opt=true, bool IM_use_block_transfer = true) = 0; */


private:
	/*! \brief Destroys all the SRVMECollection objects
	 */
	void deleteSRCollectionObjects();

	/*! \brief Publishes the register values to IS
	 */
	void publishToIS();

protected:
	std::string m_name;
	std::string m_isProvider;
	std::unique_ptr<ISNamedInfo> m_srxNamed;

	std::string m_inputString;
	std::vector<std::unique_ptr<StatusRegisterCollection>> m_collections;

	// IS Publishing Variables
	std::string m_isServerName; // IS server
	std::vector< std::vector<uint>* > m_isObjectVectors;
	std::vector< std::vector<uint>* > m_isObjectInfoVectors;

	// Counter of readout calls
	uint m_nread;
	// Default prescale value for non-express monitoring
	// NB: prescaling is not applied randomly, but sequentially!
	uint m_defPrescale;
	// String to search for in the collection name to define Prescaled collections
	std::string m_prescaleStr;

	std::mutex m_isReadingOut;

	/*! \brief defines usage of block transfer
	 * 0: single access
	 * 1: block transfer
	 */
	uint m_useBlockTransfer;


};

} // namespace ftk
} // namespace daq

#endif /* STATUSREGISTERFACTORY_H */
