#ifndef __STATUSREGISTERCOLLECTION_H__
#define __STATUSREGISTERCOLLECTION_H__
#include <vector>
#include <string>
#include <memory>
#include "ftkcommon/StatusRegister/StatusRegister.h"

namespace daq {
namespace ftk {

   /*! \brief A collection of registers for monitoring.
    * All registers should have the same type and access protocol 
    * (see srType and srAccess in StatusRegister.h)
    */
  class StatusRegisterCollection
  {
  public:
    /*! \brief Create a collection of registers of a certain type
     *
     * \param type   Type of the register defined by enum srType
     * \param access Type of the access defined by enum srAccess
     * \param blockTransfer Usage of block transfer in readout ( default is 0)
     *        0: single access
     *        1: block transfer 
     */
    StatusRegisterCollection(srType type, srAccess access, uint blockTransfer = 0);

    /*! \brief Destructor. Clean memory.
     */
    virtual ~StatusRegisterCollection();

    /*! \brief Add a register for monitoring. 
     *  only registers of the same type and access can be added
     */
    void addStatusRegister(std::unique_ptr<StatusRegister> sr);

    /*! \brief Get a register from the list. 
     */
    StatusRegister* getStatusRegister(uint32_t idx) const;


    /*! \brief Readout all registers one-by-one 
     */
    void readout();

    std::string	getName() const					{ return m_name; };
    void	setName(std::string name)			{ m_name = name; };
    std::string	getNameShort() const				{ return m_nameShort; };
    void	setNameShort(std::string nameShort)		{ m_nameShort = nameShort; };
    srType	getType() const					{ return m_type; };
    srAccess	getAccess() const				{ return m_access; };
    int         getBlockTransfer() const			{ return m_useBlockTransfer; };
    void        setBlockTransfer(uint blockTransfer)		{ m_useBlockTransfer = blockTransfer; };
    /*! \brief Get a vector of values of the registes. 
     */
    std::vector<uint32_t> getValues() const;
    /*! \brief Get a value of the given register. 
     */
    uint32_t	getValue(uint32_t idx) const;  

  private:
    std::vector< std::unique_ptr<StatusRegister> > m_registers;
    
    std::string m_name		= "Foo";
    std::string m_nameShort	= "foo";

    uint m_useBlockTransfer;

    srType m_type		= srType::dummy;
    srAccess m_access		= srAccess::dummy;
  };

} // namespace ftk
} // namespace daq

#endif // __STATUSREGISTERCOLLECTION_H__
