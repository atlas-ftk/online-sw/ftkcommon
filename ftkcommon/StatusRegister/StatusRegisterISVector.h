#ifndef __STATUSREGISTERISVECTOR_H__
#define __STATUSREGISTERISVECTOR_H__
#include <string>
#include <vector>
#include <memory>

#include "ftkcommon/StatusRegister/StatusRegister.h"

#include <ipc/core.h>
#include <ipc/partition.h>

#include <is/infodictionary.h>


namespace daq {
namespace ftk {

   /*! \brief Implementation of the DF direct register access 
    */

  class StatusRegisterISVector : public StatusRegister
  {
  public:
    /*! \brief Constructor
     *  \param partition The IPCPartition object linked to the partition to be monitored
     *  \param objName   Name of the object in the partition (full name as one sees it in is_ls or is_monitor)
     *  \param attrName  Name of the attribute (=variable) of the object. Corresponds to the name in IS schema file
     *  \param type      Any enum of srType type
     */

    StatusRegisterISVector(const IPCPartition& partition, const std::string& objName, const std::string& attrName, srType type);
    ~StatusRegisterISVector();

    /*! \brief Read values from IS as a vector of unsigned integers
     */
    void readout();

    std::vector<uint32_t> getValues() const { return m_values; };

    std::string getTime() const { return m_time; };

    std::string getObjName() const { return m_isObjName; }

    std::string getAttrName() const { return m_node; }

  private:
    std::unique_ptr<ISInfoDictionary> isd;

    std::string m_isObjName;

    std::vector<uint32_t> m_values;

    std::string m_time;

  };

} // namespace ftk
} // namespace daq

#endif // __STATUSREGISTERISVECTOR_H__
