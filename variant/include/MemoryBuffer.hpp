#ifndef __MEMORY_BUFFER_HPP__
#define  __MEMORY_BUFFER_HPP__

#include <vector>
#include <variant.hpp>
#include <stdint.h>


class MemoryBuffer {
public:
   template <int N> void _swap(const uint8_t *data,uint8_t *dest);
};
template<> inline void MemoryBuffer::_swap<1>(const uint8_t *data,uint8_t *dest) { *dest=*data;  }
template<> inline void MemoryBuffer::_swap<2>(const uint8_t *data,uint8_t *dest) { *((uint16_t*) dest) = __builtin_bswap16(*((uint16_t*)data));  }
template<> inline void MemoryBuffer::_swap<4>(const uint8_t *data,uint8_t *dest) { *((uint32_t*) dest) = __builtin_bswap32(*((uint32_t*)data)); }
template<> inline void MemoryBuffer::_swap<8>(const uint8_t *data,uint8_t *dest) { *((uint64_t*) dest) = __builtin_bswap64(*((uint64_t*)data)); } 
    

class MemoryReadBuffer : MemoryBuffer {
public:
  MemoryReadBuffer(const char *buffer,std::size_t len): _buf((uint8_t*)buffer),_len(len),_pos(0){}
  template <typename T> 
  void get(T &&val) {
    if(sizeof(T)>=_len) throw;
    _swap<sizeof(T)>(&_buf[_pos],(uint8_t *)&val);    
    _pos+=sizeof(T);
  }
  const char *getBytes(std::size_t n) {
    if((_pos+n)>=_len) throw;
    const char*p=(const char*)&_buf[_pos];
    _pos+=n;
    return p;
  }
  template <typename T> bool peek(T &val,std::size_t offset) { 
    if((_pos+offset+sizeof(T))>_len) {
      return false;
    }
    _swap<sizeof(T)>((uint8_t*)&_buf[_pos+offset],(uint8_t*)&val);
    return true;
  }
  void advance(std::size_t offset) {
    _pos+=offset;
  }

  std::size_t size() {return _len;}
  std::size_t pos() {return _pos;}

private :
  const uint8_t *_buf;
  std::size_t _len;
  std::size_t _pos;
 
};
class MemoryWriteBuffer : public MemoryBuffer {
public:
  MemoryWriteBuffer(std::vector<uint8_t> &buf):_buf(buf) {}
  
  template <typename T> 
  void put(const T &val) {
    uint8_t  tmp[sizeof(T)];
    _swap<sizeof(T)>((uint8_t*)&val,tmp);
    for(std::size_t i=0;i<sizeof(T);i++) _buf.push_back(tmp[i]);
  }
  void putBytes(const char *s,std::size_t n) {
    const uint8_t *p=(uint8_t*) s;
    for(std::size_t i=0;i<n;i++) _buf.push_back(p[i]);
  }
  private:
  std::vector<uint8_t> &_buf;

};

#endif
