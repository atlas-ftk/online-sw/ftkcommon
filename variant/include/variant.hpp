#ifndef __VARIANT_HPP__
#define  __VARIANT_HPP__

#if __cplusplus == 201402L 
#define USE_CPP14
#endif

#ifdef USE_CPP14
#include <mpark/variant.hpp>
#else
#include <variant>
#endif
#include <mpark/variant.hpp>
#include <memory>
#include <vector>
#include <unordered_map>
#include <map>
#include <cstddef>
#include <iostream>
#include <stdint.h>
#include <string>
#include <sstream>
#include <typeinfo>
#include <typeindex>
#include <iterator>
#include <initializer_list> 
#include <iostream>
#include <fstream>


#include <rapidjson/reader.h>
#include <rapidjson/writer.h>
#include <rapidjson/stringbuffer.h>

//#include <msgpack.hpp>

#include <stack>

namespace storage {
// deep pointer class from https://codereview.stackexchange.com/questions/103744/deepptra-deep-copying-unique-ptr-wrapper-in-c/103792
template <typename T>
class DeepPtr
{
public:
  DeepPtr() : myPtr(new T() )  { }; // change from original, always create stored object
  DeepPtr( const T& value ) :myPtr( new T( value ) ) {} ;
  DeepPtr( const DeepPtr& other ) :myPtr( nullptr ) {
    if ( other ) {	  
      myPtr = std::unique_ptr<T>( new T( *other ) );
    }
  }
  DeepPtr( DeepPtr&& other ) :myPtr( nullptr ) {
    if ( other )
      {
	myPtr = std::unique_ptr<T>( new T( *other ) );
      }
  }
  DeepPtr& operator=( const DeepPtr& other ) {
    DeepPtr temp{ other };
    swap( *this, temp );
    return *this;
  }
  DeepPtr& operator=( DeepPtr&& other ) {
    swap( *this, other );
    return *this;
  }
  static void swap( DeepPtr& left, DeepPtr& right ) { std::swap( left.myPtr, right.myPtr ); }
  T& operator*() { return *myPtr; }
  const T& operator*() const { return *myPtr; }
  // T* const operator->() { return myPtr.operator->(); }
  T* const operator->() {return myPtr.operator->();}
  T* operator->() const { return myPtr.operator->(); }
  T* get() const { return myPtr.get(); }
  operator bool() const { return (bool)myPtr; }
private:
  std::unique_ptr<T> myPtr;
};

template<typename Test, template<typename...> class Ref>
struct is_specialization : std::false_type {};

template<template<typename...> class Ref, typename... Args>
struct is_specialization<Ref<Args...>, Ref>: std::true_type {};

class variant;
using array_t=std::vector<variant>;
using string_t=std::string;
using size_type=std::size_t;
using object_t=std::map<std::string,variant>;
class ArrayPtr : public DeepPtr<array_t> { using DeepPtr<array_t>::DeepPtr ; } ;
class ObjPtr   : public DeepPtr<object_t> { using DeepPtr<object_t>::DeepPtr ; } ;
class StrPtr   : public DeepPtr<string_t> { using DeepPtr<string_t>::DeepPtr ; } ;
template <typename T,typename A=std::vector<T>> class ValPtr   : public DeepPtr<A> { using DeepPtr<A>::DeepPtr ; } ;


#ifdef USE_CPP14
template <typename...  T> using variant_t=mpark::variant<T...>;
#define get_v mpark::get
#define visit_v mpark::visit
#else
template <typename... T> using variant_t=std::variant<T...>;
#define get_v std::get
#define visit_v std::visit
#endif
using value_t =variant_t<std::nullptr_t,
                            uint64_t,
 	  		    uint32_t,
			    uint16_t,
			    uint8_t,
			    int64_t,
			    int32_t,
			    int16_t,
			    int8_t,
			    float,
			    double,
			    bool,
			    ArrayPtr,
			    ObjPtr,
			    StrPtr,
			    ValPtr<uint64_t>,
			    ValPtr<int64_t>,
			    ValPtr<uint32_t>,
			    ValPtr<int32_t>,
			    ValPtr<uint16_t>,
			    ValPtr<int16_t>,
			    ValPtr<uint8_t>,
			    ValPtr<int8_t>,
			    ValPtr<float>,
			    ValPtr<double>,
			    ValPtr<bool>
			    >;


class variant {
  using value_type = variant;
  using reference = value_type&;
  using pointer = const value_type*;
  using const_reference = const reference;
public:
  enum class Type {
    null,
      uint64,
      uint32,
      uint16,
      uint8,
      int64,
      int32,
      int16,
      int8,
      float32,
      float64,
      boolean,
      array,
      object,
      string,
      array_uint64,
      array_int64,
      array_uint32,
      array_int32,
      array_uint16,
      array_int16,
      array_uint8,
      array_int8,
      array_float,
      array_double,
      array_bool 
      };
  
  variant() : value(nullptr) {}
  
  variant(const variant &other)  { 
    value=other.value;
  }
  
  variant(Type t) {
    switch(t) {
    case Type::null: value=nullptr; break;
    case Type::uint64:  value=uint64_t(0); break;
    case Type::int64: value=int64_t(0); break;
    case Type::uint32: value=uint32_t(0); break;
    case Type::int32:  value=int32_t(0);break;
    case Type::uint16:  value=uint16_t(0);break;
    case Type::int16:  value=int16_t(0);break;
    case Type::uint8: value=uint8_t(0); break;
    case Type::int8:  value=int8_t(0);break;
    case Type::float32:  value=float(0.);break;
    case Type::float64: value=double(0.); break;
    case Type::boolean:  value=bool(false);break;
    case Type::array:  value=ArrayPtr();break;
    case Type::object:  value= ObjPtr();break;
    case Type::string:  value= StrPtr();break; 
    case Type::array_uint64: value=ValPtr<uint64_t>(); break;
    case Type::array_int64: value=ValPtr<int64_t>(); break;
    case Type::array_uint32: value=ValPtr<uint32_t>(); break;
    case Type::array_int32: value=ValPtr<int32_t>(); break;
    case Type::array_uint16: value=ValPtr<uint16_t>(); break;
    case Type::array_int16: value=ValPtr<int16_t>(); break;
    case Type::array_uint8: value=ValPtr<uint8_t>(); break;
    case Type::array_int8:  value=ValPtr<int8_t>();break;
    case Type::array_float:  value=ValPtr<float>();break;
    case Type::array_double:  value=ValPtr<double>();break;
    case Type::array_bool:  value=ValPtr<bool>(); break;
    }      
  }
  variant& operator=(variant other) {
    std::swap(value,other.value);
    return *this;
  }
  
  
  template <typename T> variant(T arg) :value(arg){}
  template <typename T> variant(std::vector<T> arg) {
    value=ValPtr<T>( std::vector<T> (arg)); 
  }
  variant(const char *arg) {
    value=StrPtr( std::string(arg));
  }

  variant(std::initializer_list<variant> init,
	  bool type_deduction = true,
	  Type manual_type = Type::array) {
    bool is_an_object = true;
    for (const auto& element : init) {
      if (not element.is_array() or element.size() != 2
	  or not element[0].is_string()) {
	is_an_object = false;
	break;
      }
    }
    if (not type_deduction) {
      if (manual_type == Type::array) {
	is_an_object = false;
      }
      if (manual_type == Type::object and not is_an_object) {
	throw;
      }
    }
     if (is_an_object) {
       value=ObjPtr();
       for (auto& element : init) {
	 get_v<ObjPtr>(value)->emplace(
						     std::move(*(get_v<StrPtr>(element[0].value))),
						     std::move(element[1]));
       }
     } else {
       value = ArrayPtr(std::move(init));
     }
  }

  uint64_t getUint() const {
    if(index()==Type::uint64) return get_v<uint64_t>(value);
    if(index()==Type::uint32) return get_v<uint32_t>(value);
    if(index()==Type::uint16) return get_v<uint16_t>(value);
    if(index()==Type::uint8) return get_v<uint8_t>(value);
    throw;
  }
  int64_t getInt() const {
    if(index()==Type::int64) return get_v<int64_t>(value);
    if(index()==Type::int32) return get_v<int32_t>(value);
    if(index()==Type::int16) return get_v<int16_t>(value);
    if(index()==Type::int8) return get_v<int8_t>(value);
    throw;
  }
  double getFloat() const {
    if(index()==Type::float64) return get_v<double>(value);
    if(index()==Type::float32) return get_v<float>(value);
    throw;
  }

  

  
  bool is_float() const { return index()==Type::float64 || index()==Type::float32; }
  bool is_integer()const { return index()>=Type::uint64 && index()<=Type::uint8;}
  bool is_signed()const { return index()>=Type::int64 && index()<=Type::int8;}
  bool is_unsigned()const { return index()>=Type::uint64 && index()<=Type::uint8;}
  bool is_bool() const { return index()==Type::boolean;}
  bool is_array() const { return index()==Type::array;}
  bool is_object() const { return index()==Type::object;}
  bool is_null() const { return index()==Type::null;}
  bool is_string() const { return index()==Type::string;}
  bool is_value() const { return index()<=Type::boolean;}
  bool is_value_array() const { return index()>=Type::array_uint64;}

  size_type size() const noexcept {
    if(is_null()) return 0;
    else if(is_array()) return get_v<ArrayPtr>(value)->size();
    else if(is_object()) return  get_v<ObjPtr>(value)->size();
    else return 1;
  }


#ifdef USE_CPP14
  size_type byte_count() const noexcept {
    size_t count=sizeof(*this);
    if(is_object()) {
      size_type s=0; 
      for(auto const &o: *get_v<ObjPtr>(value)) 
	count+=(o.second.byte_count()+o.first.size()); 
    } else if (is_array()) {
      for(auto const &a: *get_v<ArrayPtr>(value)) count+=a.byte_count();
    } else if(is_string()) {
    count+= get_v<StrPtr>(value)->size();
    }     
    return count;
  }  
#else  
  size_type byte_count() const noexcept {
    size_t count=sizeof(*this);
    count+=visit_v([](auto&& arg)->size_type {
	using T = std::decay_t<decltype(arg)>;
	if constexpr (is_specialization<T,ValPtr>::value ) {
		   size_type s=arg->size();
		   return s>0?s*sizeof(arg->at(0)):s;
		   }
	else if constexpr  (std::is_same_v<T,ArrayPtr> ) {
		 size_type s=0;
	  for(auto const &a: *arg) s+=a.byte_count();
	  	return s;
	}
	else if constexpr (std::is_same_v<T,ObjPtr>) {
	    size_type s=0;
	    for(auto const &o: *arg) s+=(o.second.byte_count()+o.first.size());
	    return s;
	} 
	else if constexpr (std::is_same_v<T,StrPtr>) return arg->size();
	else return 0;
      },value);
    return count;
  }
#endif
  void dump() const {
    rapidjson::StringBuffer buffer;
    rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
    dump(writer);
    std::cout << buffer.GetString() << std::endl;
  }



#ifdef USE_CPP14
  void dump(rapidjson::Writer<rapidjson::StringBuffer> &w) const noexcept {    
    if(is_object()) {
      w.StartObject();
      for(auto const &o: *get_v<ObjPtr>(value)) {
	w.Key(o.first.c_str()); 
	o.second.dump(w);
      }
      w.EndObject();
    } else if (is_array()) {
      w.StartArray();
      for(auto const &a: *get_v<ArrayPtr>(value))  a.dump(w);
      w.EndArray();
    } else if(is_string()) {
      w.String(get_v<StrPtr>(value)->c_str());
    } else if(is_bool()) {
      w.Bool(get_v<bool>(value));
    } else if(is_signed()) {
      w.Int(getInt());
    } else if(is_unsigned()) {
      w.Uint(getUint());
    } else if(is_float()) {
      w.Double(getFloat());
    } else if(is_null()) {
      w.Null();
    } else if(is_value_array()) {
      w.Null(); //fixme
    }
    
}  

 
#else
  void dump(rapidjson::Writer<rapidjson::StringBuffer> &w) const noexcept {
    visit_v([&w](auto&& arg) {
	using T = std::decay_t<decltype(arg)>;
	if constexpr (is_specialization<T,ValPtr>::value ) {
	    std::cout <<"ValPtr\n" << std::endl;
	  }
	else if constexpr  (std::is_same_v<T,ArrayPtr> ) {
	    w.StartArray();
	    for(auto const &a: *arg)  a.dump(w); 
	    w.EndArray();
	}
	else if constexpr (std::is_same_v<T,ObjPtr>) {
	    w.StartObject();
	    for(auto const &o: *arg) {
	      w.Key(o.first.c_str()); 
	      o.second.dump(w);
	    }
	    w.EndObject();
	  }
	else if constexpr (std::is_same_v<T,StrPtr>) w.String(arg->c_str());
	else if constexpr (std::is_same_v<T,std::nullptr_t>) w.Null();
	else if constexpr (std::is_same_v<T,bool>) w.Bool(arg);
	else if constexpr (std::is_floating_point_v<T>) w.Double(arg);
	else if constexpr (std::is_signed_v<T>) w.Int(arg);
	else if constexpr (std::is_unsigned_v<T>) w.Uint(arg);
	else std::cout << " " << arg << "\n";
      },value);
  }
#endif


  
  #if 0
  template<class T> operator const T&() {
    if constexpr(std::is_arithmetic<T>{}) {
	return get_v<T>(value);
      } //else return *get_v<T>(value);
  }
  template<class T> operator  T() {
    if constexpr(std::is_arithmetic<T>{}) {
	return get_v<T>(value);
      }// else return *get_v<T>(value);
  }
  #endif
      
  template<class T,typename std::enable_if<
		     std::is_arithmetic<T>{},int>::type = 0>
    operator const T&() {
    return get_v<T>(value);
  }

  template<class T,
	   typename std::enable_if<
             std::is_arithmetic<T>{},
	       int>::type = 0>
    operator T() {
      return get_v<T>(value);
  }

  template <typename T>  const std::vector<T> &get() const {
     return *get_v<ValPtr<T>>(value);
   }
  template <typename T>  std::vector<T> &get()  {
     return *get_v<ValPtr<T>>(value);
  }
  
   template <typename T> operator const std::vector<T>&() {
    return *get_v<ValPtr<T>>(value);
   }
  

  reference operator [] (const object_t::key_type &key) {
    if(is_null() ) {
      value=ObjPtr();
      get_v<ObjPtr>(value)->operator[](key);
    } 
    return get_v<ObjPtr>(value)->operator[](key);
  }
  const_reference operator [] (const object_t::key_type &key) const {
    if(is_object()) {
      return  get_v<ObjPtr>(value)->operator[](key);
    } else throw;
  }

  reference operator [] (array_t::size_type idx) {
    if(is_null()) {
      value=ArrayPtr();     
      get_v<ArrayPtr>(value)->operator[](idx);
    }
    return get_v<ArrayPtr>(value)->operator[](idx);
  }

  const_reference operator[](size_type idx) const {
    if(is_array())   return get_v<ArrayPtr>(value)->operator[](idx);
    else throw;
  }


  template <typename T>
  void push_back(const T& val) {
    if(is_null()) {
      value=ArrayPtr();
    }
    get_v<ArrayPtr>(value)->push_back(variant(val));
  }
  
  Type index() const {return (Type)value.index();}




  const std::string type_name() const {
     static const std::vector<std::string> typenames={
      "null",
      "uint64",
      "uint32",
      "uint16",
      "uint8",
      "int64",
      "int32",
      "int16",
      "int8",
      "float",
      "double",
      "bool",
      "array",
      "object",
      "string",
      "array_uint64",
      "array_int64",
      "array_uint32",
      "array_int32",
      "array_uint16",
      "array_int16",
      "array_uint8",
      "array_int8",
      "array_float",
      "array_double",
      "array_bool"
    };
    return typenames[value.index()];        
  }
  
  value_t value;
  friend std::ostream& operator<<(std::ostream &out, const variant &var) ;
  class primitive_iterator_t {
  public:
    using difference_type = std::ptrdiff_t;
    
    constexpr difference_type get_value() const noexcept {
      return m_it;
    }

    void set_begin() noexcept {
      m_it = begin_value;
    }

    void set_end() noexcept {
      m_it = end_value;
    }

    constexpr bool is_begin() const noexcept {
      return m_it == begin_value;
    }

    constexpr bool is_end() const noexcept {
      return m_it == end_value;
    }

    friend constexpr bool operator==(primitive_iterator_t lhs, primitive_iterator_t rhs) noexcept {
      return lhs.m_it == rhs.m_it;
    }

    friend constexpr bool operator<(primitive_iterator_t lhs, primitive_iterator_t rhs) noexcept {
      return lhs.m_it < rhs.m_it;
    }

    primitive_iterator_t operator+(difference_type i) {
      auto result = *this;
      result += i;
      return result;
    }

    friend constexpr difference_type operator-(primitive_iterator_t lhs, primitive_iterator_t rhs) noexcept {
      return lhs.m_it - rhs.m_it;
    }

    friend std::ostream& operator<<(std::ostream& os, primitive_iterator_t it) {
      return os << it.m_it;
    }
    
    primitive_iterator_t& operator++() {
      ++m_it;
      return *this;
    }

    primitive_iterator_t operator++(int) {
      auto result = *this;
      m_it++;
      return result;
    }

    primitive_iterator_t& operator--() {
      --m_it;
      return *this;
    }

    primitive_iterator_t operator--(int) {
      auto result = *this;
      m_it--;
      return result;
    }

    primitive_iterator_t& operator+=(difference_type n) {
      m_it += n;
      return *this;
    }

    primitive_iterator_t& operator-=(difference_type n) {
      m_it -= n;
      return *this;
    }

  private:
    static constexpr difference_type begin_value = 0;
    static constexpr difference_type end_value = begin_value + 1;
    
    difference_type m_it = (std::numeric_limits<std::ptrdiff_t>::min)();
  };

  using internal_iterator=variant_t< 
    object_t::iterator,
    array_t::iterator,
    primitive_iterator_t>;

  class iterator {
    friend variant;
    using value_type = variant;
    using pointer = const value_type*;
    using reference = const value_type&;
    using difference_type = std::ptrdiff_t;
    using iterator_category = std::bidirectional_iterator_tag;
    using array_it=array_t::iterator;
    using object_it=object_t::iterator;
    using internal_iterator=variant_t< 
      object_it,
      array_it,
      primitive_iterator_t>;


    iterator()=default;
  public:
    explicit iterator(pointer object) noexcept : m_object(object) {
      if(m_object->is_object()) m_it = object_it();
      else if(m_object->is_array())  m_it = array_it();
      else  m_it=primitive_iterator_t();
    }
    iterator(const iterator& other) noexcept : m_object(other.m_object), m_it(other.m_it) {}
    iterator& operator=(const iterator& other) noexcept {
      m_object = other.m_object;
      m_it = other.m_it;
      return *this;
    }
    reference operator*() const {
      if(m_object->is_array())  return *get_v<array_it>(m_it) ;
      else if (m_object->is_object()) return get_v<object_it>(m_it)->second;
      else if( get_v<primitive_iterator_t>(m_it).is_begin()) return *m_object;
      else throw;
    }
    pointer operator->() const {
      if(m_object->is_array())   return &*get_v<array_it>(m_it);
      else if (m_object->is_object()) return &get_v<object_it>(m_it)->second;
      else if( get_v<primitive_iterator_t>(m_it).is_begin()) return m_object;
      else throw;
    }
    iterator operator++(int) {
      auto result = *this;
      ++(*this);
      return result;
    }
    iterator& operator++() {
      if(m_object->is_array()) std::advance(get_v<array_it>(m_it), 1);
      else if (m_object->is_object()) std::advance(get_v<object_it>(m_it), 1);
      else get_v<primitive_iterator_t>(m_it)++;
      return *this;
    }
    iterator operator--(int) {
      auto result = *this;
      --(*this);
      return result;
    }
    iterator& operator--() {
      if(m_object->is_array()) std::advance(get_v<array_it>(m_it), -1);
      else if (m_object->is_object()) std::advance(get_v<object_it>(m_it), -1);
      else get_v<primitive_iterator_t>(m_it)--;
      return *this;
    }
    bool operator==(const iterator& other) const {
      if( m_object != other.m_object) throw;
      if(m_object->is_array()) return (get_v<array_it>(m_it)== get_v<array_it>(other.m_it));
      else if (m_object->is_object()) return (get_v<object_it>(m_it)== get_v<object_it>(other.m_it));
      else return (get_v<primitive_iterator_t>(m_it) == get_v<primitive_iterator_t>(other.m_it));

    }
    bool operator!=(const iterator& other) const {
        return not operator==(other);
    }
    bool operator<(const iterator& other) const {
      if (m_object->is_object()) throw;
      else if (m_object->is_array()) return (get_v<array_it>(m_it)< get_v<array_it>(other.m_it));
      else (get_v<primitive_iterator_t>(m_it) < get_v<primitive_iterator_t>(other.m_it));
    }
    bool operator<=(const iterator& other) const
    {
        return not other.operator < (*this);
    }
    bool operator>(const iterator& other) const
    {
        return not operator<=(other);
    }
    bool operator>=(const iterator& other) const
    {
        return not operator<(other);
    }
    iterator& operator+=(difference_type i) {
       if (m_object->is_object()) throw;
       else if (m_object->is_array()) std::advance(get_v<array_it>(m_it), i);
       else (get_v<primitive_iterator_t>(m_it))+=i;
       return *this;
    }
    iterator& operator-=(difference_type i) {
      return operator+=(-i);
    }
    iterator operator+(difference_type i) const {
      auto result = *this;
      result += i;
      return result;
    }
    friend iterator operator+(difference_type i, const iterator& it) {
      auto result = it;
      result += i;
      return result;
    }
    iterator operator-(difference_type i) const {
      auto result = *this;
      result -= i;
      return result;
    }
    difference_type operator-(const iterator& other) const {
      if (m_object->is_object()) throw;
      else if (m_object->is_array()) return get_v<array_it>(m_it) - get_v<array_it>(other.m_it);
      else  get_v<primitive_iterator_t>(m_it) - get_v<primitive_iterator_t>(other.m_it);
    }
    reference operator[](difference_type n) const {
      if (m_object->is_object()) throw;
      else if (m_object->is_array()) return *std::next(get_v<array_it>(m_it), n);
      else if (get_v<primitive_iterator_t>(m_it).get_value() == -n) return *m_object;
      else throw;
    }
    typename object_t::key_type key() const {
      if(m_object->is_object()) {
	return get_v<object_it>(m_it)->first;
      }
      throw;
    }
    reference value() const {
      return operator*();
    }


    
  private:

    void set_begin() noexcept  {
      if(m_object->is_array()) get_v<array_it>(m_it)= get_v<ArrayPtr>(m_object->value)->begin();   
      else  if(m_object->is_object()) get_v<object_it>(m_it)=get_v<ObjPtr>(m_object->value)->begin();
      else if(m_object->is_null()) get_v<primitive_iterator_t>(m_it).set_end();
      else get_v<primitive_iterator_t>(m_it).set_begin();	     	  
    }
     void set_end() noexcept  {
       if(m_object->is_array()) get_v<array_it>(m_it)=get_v<ArrayPtr>(m_object->value)->end();   
       else  if(m_object->is_object()) get_v<object_it>(m_it)=get_v<ObjPtr>(m_object->value)->end();
       else if(m_object->is_null()) get_v<primitive_iterator_t>(m_it).set_end();
       else get_v<primitive_iterator_t>(m_it).set_end();	     	  
     }
    pointer m_object = nullptr;
    internal_iterator m_it;
    
  };
public:
  iterator begin() noexcept {
    iterator result(this);
    result.set_begin();
    return result;
  }
  iterator end() noexcept {
    iterator result(this);
    result.set_end();
    return result;
  }


};

/*
#ifdef USE_CPP14


#else
#if 0
std::ostream& operator<<(std::ostream &out, const variant &var) 
{  
  out<< visit_v([](auto&& arg)->std::string {
      using T = std::decay_t<decltype(arg)>;
      if constexpr (std::is_arithmetic<T>{})
		     return std::to_string(arg);
      else if constexpr ( std::is_same_v<T, StrPtr >)
			  return *arg;
      else
	return std::string(typeid(arg).name());
      
    }, var.value);

  return out;
}
#endif
#endif
*/

struct MsgPackHandler {
  bool visit_nil() {
    return true;
  }
  



};

//using namespace rapidjson;
class JsonHandler {
public:
  JsonHandler(variant &var) : v(var) {}
private:
  void val_handler(variant var) {
    if(!object.empty()) {
      variant &top=object.top();
      if(top.is_array()) {
	top.push_back(var);
      } else if(top.is_object() && !keys.empty() ) {
	top[keys.top()]=var;
	keys.pop();
      }
    }
  }
    
  std::stack<variant> object;
  std::stack<std::string> keys;
  variant &v;
public:
  bool Null() { 
    val_handler(variant(variant::Type::null));
    return true; 
  }

  bool Bool(bool b) {   
    val_handler(variant(b));
    return true; 
  }
  
  bool Int(int i) { 
    val_handler(variant(i));
    return true; 
   }

   bool Uint(unsigned u) { 
    val_handler(variant(u));
    return true; 
  }

  bool Int64(int64_t i) { 
    val_handler(variant(i));
    return true; 
  }

  bool Uint64(uint64_t u) {
    val_handler(variant(u));
    return true; }

  bool Double(double d) { 
    val_handler(variant(d));
    return true; 
  }
  bool RawNumber(const char* str, rapidjson::SizeType length, bool copy) { 
    return true;
  }
  bool String(const char* str, rapidjson::SizeType length, bool copy) { 
    val_handler(variant(str));
    return true;
  }
  bool StartObject() { 
    object.push(variant(variant::Type::object));
    return true;
  }
  bool Key(const char* str, rapidjson::SizeType length, bool copy) {
    keys.push(str);
    return true;
  }
  bool EndObject(rapidjson::SizeType memberCount) {
    variant a=object.top();
    object.pop();
    val_handler(a);
    if(object.empty()) v=a;
    return true;
  }
  bool StartArray() { 
    object.push(variant(variant::Type::array));
    return true;
  }
  bool EndArray(rapidjson::SizeType elementCount) { 
    variant a=object.top();
    object.pop();
    val_handler(a);
    return true; 
  }
};

}
#endif
