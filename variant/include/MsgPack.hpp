#ifndef __MSG_PACK_HPP__
#define __MSG_PACK_HPP__

#include <variant.hpp>
#include <cstdlib>
#include <stdint.h>
#include <iostream>
#include <vector>
#include <typeinfo>
#include <unordered_map>

#include <MemoryBuffer.hpp>

namespace msgpack {
  using namespace storage;
  constexpr uint8_t FIXMAP=0x80;
  constexpr uint8_t FIXARRAY=0x90;
  constexpr uint8_t FIXSTR=0xa0;
  constexpr uint8_t NIL=0xc0;
  constexpr uint8_t NEVER_USED=0xc1;
  constexpr uint8_t FALSE=0xc2;
  constexpr uint8_t TRUE=0xc3;
  constexpr uint8_t BIN_8=0xc4;
  constexpr uint8_t BIN_16=0xc5;
  constexpr uint8_t BIN_32=0xc6;
  constexpr uint8_t EXT_8=0xc7;
  constexpr uint8_t EXT_16=0xc8;
  constexpr uint8_t EXT_32=0xc9;
  constexpr uint8_t FLOAT_32=0xca;
  constexpr uint8_t FLOAT_64=0xcb;
  constexpr uint8_t UINT_8=0xcc;
  constexpr uint8_t UINT_16=0xcd;
  constexpr uint8_t UINT_32=0xce;
  constexpr uint8_t UINT_64=0xcf;
  constexpr uint8_t INT_8=0xd0;
  constexpr uint8_t INT_16=0xd1;
  constexpr uint8_t INT_32=0xd2;
  constexpr uint8_t INT_64=0xd3;
  constexpr uint8_t FIXEXT_1=0xd4;
  constexpr uint8_t FIXEXT_2=0xd5;
  constexpr uint8_t FIXEXT_4=0xd6;
  constexpr uint8_t FIXEXT_8=0xd7;
  constexpr uint8_t FIXEXT_16=0xd8;
  constexpr uint8_t STR_8=0xd9;
  constexpr uint8_t STR_16=0xda;
  constexpr uint8_t STR_32=0xdb;
  constexpr uint8_t ARRAY_16=0xdc;
  constexpr uint8_t ARRAY_32=0xdd;
  constexpr uint8_t MAP_16=0xde;
  constexpr uint8_t MAP_32=0xdf;

  constexpr uint32_t CONV_NONE=0x00;
  constexpr uint32_t CONV_UINT_8_16=0x01;
  constexpr uint32_t CONV_UINT_8_32=0x02;
  constexpr uint32_t CONV_UINT_8_64=0x04;
  constexpr uint32_t CONV_UINT_16_32=0x08;
  constexpr uint32_t CONV_UINT_16_64=0x10;
  constexpr uint32_t CONV_UINT_32_64=0x20;
  constexpr uint32_t CONV_INT_8_16=0x100;
  constexpr uint32_t CONV_INT_8_32=0x200;
  constexpr uint32_t CONV_INT_8_64=0x400;
  constexpr uint32_t CONV_INT_16_32=0x800;
  constexpr uint32_t CONV_INT_16_64=0x1000;
  constexpr uint32_t CONV_INT_32_64=0x2000;
  constexpr uint32_t CONV_FLOAT32_64=0x10000;

  static std::unordered_map<uint8_t,std::size_t> typesize_map={
    { FLOAT_32,sizeof(float)   },
    { FLOAT_64,sizeof(double)  }, 
    { UINT_8  ,sizeof(uint8_t)   },
    { UINT_16 ,sizeof(uint16_t) },
    { UINT_32 ,sizeof(uint32_t) },
    { UINT_64 ,sizeof(uint64_t) },
    { INT_8   ,sizeof(int8_t)      },
    { INT_16  ,sizeof(int16_t)   },
    { INT_32  ,sizeof(int32_t)   },
    { INT_64  ,sizeof(int64_t)   }
  };

 static std::unordered_map<std::type_index, uint8_t> typeid_map={
    { typeid(float) , FLOAT_32 },
    { typeid(double) , FLOAT_64 }, 
    { typeid(uint8_t) , UINT_8 },
    { typeid(uint16_t) , UINT_16 },
    { typeid(uint32_t) , UINT_32 },
    { typeid(uint64_t) , UINT_64 },
    { typeid(int8_t),  INT_8 },
    { typeid(int16_t) , INT_16 },
    { typeid(int32_t) , INT_32 },
    { typeid(int64_t) , INT_64 }
  };

  

  class Reader {  
  public:
    Reader(MemoryReadBuffer &mem,variant &var,bool toDouble): _mem(mem),_var(var),_toDouble(toDouble) {
      _var=readToken();      
    }
  private:
    bool _toDouble;
    variant readToken() {
      variant v;
      uint8_t token;      
      _mem.get(token);
      switch(token) {
      case NIL: v=readNull(); break;
      case NEVER_USED: throw; break;// never used  
      case TRUE:  
      case FALSE: v=readBool(token); break; //false  
      case BIN_8: readBin<uint8_t>(); break;
      case BIN_16: readBin<uint16_t>(); break;
      case BIN_32: readBin<uint32_t>(); break;
      case EXT_8: readExt<uint8_t>(); break;
      case EXT_16: readExt<uint16_t>(); break;
      case EXT_32: readExt<uint32_t>(); break;
      case FLOAT_32: 
	if(_toDouble)
	  v=readValue<float,double>();
	else
	  v=readValue<float>(); 
	break;
      case FLOAT_64: v=readValue<double>(); break;
      case UINT_8: v=readValue<uint8_t>(); break;
      case UINT_16: v=readValue<uint16_t>(); break;
      case UINT_32: v=readValue<uint32_t>(); break;
      case UINT_64: v=readValue<uint64_t>(); break;
      case INT_8: v=readValue<int8_t>();break;
      case INT_16: v=readValue<int16_t>();break;
      case INT_32: v=readValue<int32_t>();break;
      case INT_64: v=readValue<int64_t>();break;
      case FIXEXT_1: readFixExt<1>();break;
      case FIXEXT_2: readFixExt<2>();break;  
      case FIXEXT_4: readFixExt<6>();break;   
      case FIXEXT_8: readFixExt<8>();break;  
      case FIXEXT_16: readFixExt<16>();break;    
      case STR_8: v=readStr<uint8_t>();break;
      case STR_16: v=readStr<uint16_t>();break;
      case STR_32: v=readStr<uint32_t>();break;  
      case ARRAY_16: v=readArray<uint16_t>(); break;
      case ARRAY_32: v=readArray<uint32_t>(); break;  
      case MAP_16: v=readMap<uint16_t>();break;
      case MAP_32: v=readMap<uint32_t>();break;  
	
      default: 
	uint8_t tmp=token&0x80;
	if(tmp==0x00) return readPosFixInt(token&0x7f);
	tmp=token&0xe0;
	if(tmp==0xe0) return readNegFixInt(token&0x1f);
	if(tmp==0xa0) return readStr<uint8_t>(token&0x1f);
	tmp=token&0xf0;
	if(tmp==0x80) v=readMap<uint8_t>(token&0xf);
	if(tmp==0x90) v=readArray<uint8_t>(token&0xf);    
	break;
      }
      return v;
    }
    
    template<typename T,typename N=T> variant readValue() {
      T val;
      _mem.get(val);
      return variant(N(val));
    }
    
    template<typename T> variant readStr(uint8_t token=0) {
      return variant(readString<T>(token));
    }

    std::string readKey() {
      uint8_t token;
      _mem.get(token);
      switch(token) {
      case 0xd9: return readString<uint8_t>();break;
      case 0xda: return readString<uint16_t>();break;
      case 0xdb: return readString<uint32_t>();break;  
      default:
	if((token&0xe0)==0xa0) return readString<uint8_t>(token&0x1f);	
	break;
      }
    }
    
    template<typename T> std::string readString(uint8_t token=0) {
      T len;
      if(token==0) _mem.get(len); else len=token;
      std::string result(_mem.getBytes(len),len);
      return result;
    }
    
    
    variant readBool(uint8_t val) {
      bool b=(val==0xc3); 
      return variant(b);
    }
    variant readPosFixInt(uint8_t val) {
      return variant(val);      
    }
    variant readNegFixInt(uint8_t val) {
      return variant(int8_t(val));
    }
    
    variant readNull() {
      return variant();
    }
    
    // byte array
    template<typename T> void readBin() {
      T len;
      _mem.get(len);
      std::vector<uint8_t> result(len);
      const uint8_t *p=(const uint8_t*) _mem.getBytes(len);
      for(std::size_t i=0;i<len;i++) result[i]=p[i];    
    }
    template <unsigned N> void readFixExt() {
      uint8_t type;
      std::size_t len=N;
      _mem.get(type);
      _mem.getBytes(len);  
    // handle types  
    }
    template <typename T> void readExt() {
      uint8_t type;
      T len;
      _mem.get(len);
    _mem.get(type);
    _mem.getBytes(len);  
    // handle types  
    }
    template <typename T> variant readMap(uint8_t token=0) {
      variant result;
      T len;
      if(token==0) _mem.get(len); else len=token;
      for(std::size_t i=0;i<len;i++) {
	std::string key=readKey();
	result[key]=readToken();
      }
      return result;
    }

    bool readTypedArray(variant &var,std::size_t len) {
      uint8_t last;
      uint8_t tok;     
      _mem.peek(tok,0);
      if(tok>=FLOAT_32 && tok<=INT_64) {
	std::size_t offset=(len-1)*(1+typesize_map[tok]);
	_mem.peek(last,offset);
	if(tok!=last) return false;
      }
      
      switch(tok) {
      case UINT_8: return readTypedArray<uint8_t>(var,len); break;
      case UINT_16: return readTypedArray<uint16_t>(var,len); break;
      case UINT_32: return readTypedArray<uint32_t>(var,len); break;
      case UINT_64: return readTypedArray<uint64_t>(var,len); break;	
      case INT_8: return readTypedArray<int8_t>(var,len); break;
      case INT_16: return readTypedArray<int16_t>(var,len); break;
      case INT_32: return readTypedArray<int32_t>(var,len); break;
      case INT_64: return readTypedArray<int64_t>(var,len); break;	
      case FLOAT_32: if(_toDouble) return  readTypedArray<float,double>(var,len);
	  else return readTypedArray<float>(var,len); break;
      case FLOAT_64: return readTypedArray<double>(var,len); break;
      default:
	break;
      }
      return false;
    }

    template <typename T,typename N=T> bool readTypedArray(variant &var,std::size_t len) {
      std::vector<N> result;      
      //      std::size_t pos=_mem.pos();
      uint8_t token=typeid_map[typeid(T)];
      std::size_t sz=typesize_map[token];
      std::size_t offset=0;
      for(std::size_t i=0;i<len;i++) {
	uint8_t tkn;
	T val;
     	bool ok=_mem.peek(tkn,offset) && _mem.peek(val,offset+1);	

	if(!ok) { 
	  std::cout << "len=" << len << std::endl;
	  
	  return false;	  
	}
	offset+=sz+1;
	result.push_back(val);
      }
      var=variant(result);
      _mem.advance(offset);
      return true;
    }

    template <typename T> variant readArray(uint8_t token=0) {
      variant result(variant::Type::array);
      T len;
      if(token==0) _mem.get(len); else len=token;
      bool is_typed_array=readTypedArray(result,len);
      if(!is_typed_array)  
	for(std::size_t i=0;i<len;i++) result.push_back(readToken());
      return result;
    }
    
    MemoryReadBuffer &_mem;
    variant &_var;
  };

  class Writer {
  public:
    Writer(MemoryWriteBuffer &mem,const variant &var):_mem(mem),_var(var) {
      writeValue(var);
    }

    void _writeArrayHeader(std::size_t length)  const  {
      uint64_t n=bit_width(length);
      if(n>32) throw;
      if(n>16) {_mem.put(uint8_t(0xdd));  _mem.put(uint32_t(length)); } 
      else if(n>4)  {_mem.put(uint8_t(0xdc));  _mem.put(uint16_t(length)); }
      else _mem.put(uint8_t(0x90|length));
    }

  
    template <typename T> void writeTypedArray(const variant &var) const {
      const std::vector<T> &ar=*get_v<ValPtr<T>>(var.value);
      _writeArrayHeader(ar.size());
      for(auto a:ar) {
	_mem.put(typeid_map[typeid(T)]);
	_mem.put(a);
      }
    }

    void writeArray(const array_t &ar) {
      _writeArrayHeader(ar.size());
      for(auto a:ar) writeValue(a);
    }

    void writeObject(const object_t &obj) {
      uint64_t len=obj.size();
      uint64_t n=bit_width(len);
      if(n>32) throw;
      if(n>16) {_mem.put(uint8_t(0xde));  _mem.put(uint32_t(len)); } 
      else if(n>4)  {_mem.put(uint8_t(0xdf));  _mem.put(uint16_t(len)); }
      else _mem.put(uint8_t(0x80|len));
      for(auto o:obj) {writeStr(o.first) ; writeValue(o.second);}
    }

    void writeStr(const std::string &str) {
      uint64_t len=str.size();
      uint64_t n=bit_width(len);
      if(n>32) throw;
      if(n>16) {_mem.put(uint8_t(0xdb));  _mem.put(uint32_t(len)); } 
      if(n>8) {_mem.put(uint8_t(0xda));  _mem.put(uint32_t(len)); } 
      if(n>5) {_mem.put(uint8_t(0xd9));  _mem.put(uint32_t(len)); } 
      else _mem.put(uint8_t(0xa0|len));
      _mem.putBytes((const char*) str.data(),str.size());
      
    }

    template <typename T> void writeScalar(const variant &v) {
      T val=get_v<T>(v.value);
      _mem.put(typeid_map[typeid(T)]);
      _mem.put(val);
    }
    void writeUint8(const variant &v) {      
      uint8_t val=get_v<uint8_t>(v.value);
      if(val>127)
	_mem.put(typeid_map[typeid(uint8_t)]);
      _mem.put(val);
    }
    void writeInt8(const variant &v) {      
      uint8_t val=get_v<int8_t>(v.value);
      if(val<-32)
	_mem.put(typeid_map[typeid(uint8_t)]);
      _mem.put(val);
    }
    
    void writeBool(const variant &v) {
      bool b=get_v<bool>(v.value);
      uint8_t op=FALSE;
      if(b) op=TRUE;
      _mem.put(op);
    }
    void writeNull() { _mem.put(NIL);}

    void writeValue(const variant &v) {
      if(v.is_object()) writeObject(*get_v<ObjPtr>(v.value));
      else if(v.is_array()) writeArray(*get_v<ArrayPtr>(v.value));
      else if(v.is_string()) writeStr(*get_v<StrPtr>(v.value));
      else if(v.is_bool()) writeBool(v);
      else if(v.is_null()) writeNull();
      else switch(v.index()) {
	case variant::Type::uint64: writeScalar<uint64_t>(v); break;
	case variant::Type::uint32: writeScalar<uint32_t>(v); break;  
	case variant::Type::uint16: writeScalar<uint16_t>(v); break;    
	case variant::Type::uint8:  writeUint8(v); break; 
	case variant::Type::int64: writeScalar<int64_t>(v); break;
	case variant::Type::int32: writeScalar<int32_t>(v); break;  
	case variant::Type::int16: writeScalar<int16_t>(v); break;    
	case variant::Type::int8: writeInt8(v); break; 
    	case variant::Type::float32: writeScalar<float>(v); break;    
	case variant::Type::float64: writeScalar<double>(v); break;
	case variant::Type::array_uint64: writeTypedArray<uint64_t>(v); break;
	case variant::Type::array_uint32: writeTypedArray<uint32_t>(v); break;  
	case variant::Type::array_uint16: writeTypedArray<uint16_t>(v); break;    
	case variant::Type::array_uint8:  writeTypedArray<uint8_t>(v); break; 
	case variant::Type::array_int64: writeTypedArray<int64_t>(v); break;
	case variant::Type::array_int32: writeTypedArray<int32_t>(v); break;  
	case variant::Type::array_int16: writeTypedArray<int16_t>(v); break;    
	case variant::Type::array_int8: writeTypedArray<int8_t>(v); break; 
    	case variant::Type::array_float: writeTypedArray<float>(v); break;    
	case variant::Type::array_double: writeTypedArray<double>(v); break; 
	}    
    }
    
  private:
    MemoryWriteBuffer &_mem;
    const variant &_var;  
    inline uint32_t bit_width(uint64_t arg) const {      
      return sizeof(arg)*8 - __builtin_clzl(arg);
    }
  };
  


}









#endif





